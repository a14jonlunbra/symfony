<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Clase;
use AppBundle\Entity\Monitor;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Usuari;

class ClaseController extends Controller
{

	/**
     *  @Route("/insertClase", name="insertClase")
     */
    public function insertClaseAction(Request $request)
    {
        $clase = new Clase();

        $form = $this->createFormBuilder($clase)
            ->add('monitor', EntityType::class, array('class' => 'AppBundle:Monitor', 
            											  'choice_label' => 'cognom1'))
            ->add('nom', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('descripcio', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Create','attr'=>array('class'=>'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clase);
            $em->flush();
            return $this->redirectToRoute('selectAllClases');
        }
        return $this->render('clase/form.html.twig', array(
            'title' => 'Insertar clase',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectClase", name="selectClase")
     */
    public function selectClaseAction(Request $request)
    {
        $clase = new Clase();

        $form = $this->createFormBuilder($clase)
            ->add('nom', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Select', 'attr'=>array('class'=>'btn btn-primary')))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clase = $this->getDoctrine()
                ->getRepository('AppBundle:Clase')
                ->findBy(
                    array('nom'=>$clase->getNom()) 
                    );
            if (count($clase)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'La clase no existeix o no esta disponible actualment'));
            }
            return $this->render('clase/content.html.twig', array(
                'clase' => $clase));
        }
        return $this->render('clase/form.html.twig', array(
            'title' => 'Seleccionar Clase',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllClases", name="selectAllClases")
     */
    public function selectAllClasesAction()
    {
        $clases = $this->getDoctrine()
            ->getRepository('AppBundle:Clase')
            ->findAll();

        if (count($clases)==0) {
            return  $this->redirectToRoute('insertClase');
        }
        return $this->render('clase/content.html.twig', array(
            'clase' => $clases));
    }

    /**
     * @Route("/updateClase", name="updateClase")
     */
    public function updateClaseAction(Request $request)
    {
        $clase = new Clase();

        $form = $this->createFormBuilder($clase)
            ->add('nom', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $clase = $em->getRepository('AppBundle:Clase')
            ->findOneBy(
                array('nom'=>$clase->getNom())
              );
                
            if (!$clase) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No em trobat aquesta clase'));
            }
            return $this->redirectToRoute('editarClase',array('nom' => $clase->getNom()));

        }
        return $this->render('clase/form.html.twig', array(
            'title' => 'Modificar la clase',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editarClase/{nom}", name="editarClase")
     */
    public function editarUsuariAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $clase = $em->getRepository('AppBundle:Clase')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($clase)
            ->add('monitor', EntityType::class, array('class' => 'AppBundle:Monitor', 
                                                          'choice_label' => 'cognom1'))
            ->add('nom', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('descripcio', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Modificar clase', 'attr'=>array('class'=>'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Clase actualitzada: '. $clase->getMonitor()->getNom() .' / '
                                               . $clase->getNom() .' / '
                                               . $clase->getDescripcio() 
            ));
        }
        return $this->render('clase/form.html.twig', array(
            'title' => 'Editar la clase',
            'form' => $form->createView(),
        ));
    }

    /**
     *
     * @Route("matricula/{id}", name="matricular")
     *
     */
    // $id  de la clase
    public function matricularAction($id,Request $request){
        $formClase =new Clase();
        $form = $this->createFormBuilder($formClase )
            ->add('usuaris', EntityType::class, array('class' => 'AppBundle:Usuari',
                'choice_label' => 'cognom1','choice_value' => 'id','attr'=>array('class'=>'form-group'), 'multiple'=>'true'))
            ->add('save', SubmitType::class, array('label' => 'Matricular','attr'=> array('class'=>'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);
        $em=$this->getDoctrine()->getManager();
        $clase = $em->getRepository('AppBundle:Clase')
            ->find($id);
        if ($form->isSubmitted() && $form->isValid()) {

            if ($id!=null){

                $clase = $em->getRepository('AppBundle:Clase')
                    ->find($id);
                foreach ($formClase->getUsuaris() as $usuari) {
                    $clase->addUsuari($usuari);
                }
                $em->persist($clase);
                $em->flush();
                return $this->redirectToRoute('showMatricules', array('id' => $id));
            }
            return $this->render('default/message.html.twig',array("message"=>"Error, No s'ha trobat la clase"));
		}
		return $this->render('clase/form.html.twig', array(
			'title' => "Nova inscripció per la clase de ".$clase->getNom(),
			'form' => $form->createView(),
		));
    }

    /**
     *
     * @Route("showMatricules/{id}", name="showMatricules")
     */
    public function showMatriculaAction($id,Request $request){

                $em=$this->getDoctrine()->getManager();
                $clase = $em->getRepository('AppBundle:Clase')
                    ->find($id);
        if($clase instanceof Clase){
            $usuaris=$clase->getUsuaris();
            return $this->render('clase/showMatricula.html.twig', array('clase'=>$clase,
            'usuaris'=>$usuaris));
        }
        return $this->render('default/message.html.twig',array("message"=>"Error, No s'ha trobat la clase"));
    }
    /**
     *
     * @Route("removeUsuariMatricula/{class_id}/{user_id}", name="removeUsuariMatricula")
     *
     */
    public function removeMatriculaAction($class_id,$user_id, Request $request){
        $em=$this->getDoctrine()->getManager();
        $clase = $em->getRepository('AppBundle:Clase')
            ->find($class_id);

        $usuari = $em->getRepository('AppBundle:Usuari')
            ->find($user_id);

        if ($clase instanceof Clase && $usuari instanceof Usuari ){
                $clase->removeUsuari($usuari);
                //$em->persist($clase);
                $em->flush();
                return $this->redirectToRoute("showMatricules", array('id' => $class_id));

        }
        return $this->render('default/message.html.twig',array("message"=>"Error la clase o l'usuari no existeix
        "));
    }
    /*
     * $clase=$em->getRepository('AppBundle:Clase')
                    ->find($id);
                if (count($clase)==0) {
                    return $this->render('default/message.html.twig', array(
                        'message' => 'La clase no existeix o no esta disponible actualment'));
                }
                $clase->addUsuari($usuari);
                $em->persist($clase);

                if (count($usuari)==0) {
                    return $this->render('default/message.html.twig', array(
                        'message' => 'Aquest usuari n existeix'));
                }
                $em->flush();
     *
     * */
    /**
     * @Route("/removeClase/{id}", name="removeClase")
     */
    public function removeClaseAction(Request $request, $id)
    {
    
        $em = $this->getDoctrine()->getManager();
        $clase = $em->getRepository('AppBundle:Clase')
        ->find($id); 

            $nomMonitor = $clase->getMonitor()->getNom();
            $nomClase = $clase->getNom();
            $descripcio = $clase->getDescripcio();
            $em->remove($clase);
            $em->flush();

        return $this->redirectToRoute('selectAllClases');
    }

}
