<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Usuari;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Clase;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Date;
class UsuariController extends Controller
{

    /**
         *  @Route("/insertUsuari", name="insertUsuari")
     */
     public function insertUsuariAction(Request $request)
    {
        $usuari = new Usuari();

        $form = $this->createFormBuilder($usuari) // hay que poner mas campos como telefono, email, dni
            ->add('nom', TextType::class,array('attr'=>array('class'=>'form-control')))
            ->add('cognom1', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('cognom2', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('edat',NumberType::class, array('attr'=>array('class'=>'form-control')))
            ->add('dni', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('email', TextType::class, array('attr'=>array('class'=>'form-control', 'type'=>'email')))
            ->add('telefon', NumberType::class, array('attr'=>array('class'=>'form-control')))
            ->add('quota',NumberType::class, array('attr'=>array('class'=>'form-control' )))
            ->add('save', SubmitType::class, array('label' => 'Create','attr'=>array('class'=>'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $usuari->setInscripcio(new \DateTime());
            $em->persist($usuari);
            $em->flush();
           return $this->redirectToRoute('selectAllUsers');
        }
        return $this->render('usuari/form.html.twig', array(
            'title' => 'Insertar usuari',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectUsuari", name="selectUsuari")
     */
    public function selectUsuariAction(Request $request)
    {
        $usuari = new Usuari();

        $form = $this->createFormBuilder($usuari)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usuari = $this->getDoctrine()
                ->getRepository('AppBundle:Usuari')
                ->findBy(
                    array('cognom1'=>$usuari->getCognom1(), 'cognom2'=>$usuari->getCognom2()) 
                    );
            if (count($usuari)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'Usuari no trobat'));
            }
            return $this->render('usuari/content.html.twig', array(
                'usuari' => $usuari));
        }
        return $this->render('usuari/form.html.twig', array(
            'title' => 'Seleccionar Usuari',
            'form' => $form->createView(),
        ));
    }
    /**
     *
     * @Route("showUsuariClases/{id}", name="showUsuariClsaes")
     *
     */
    // $id  de la clase
    public function showUsuariClsaesAction($id,Request $request){
        $em=$this->getDoctrine()->getManager();
        $usuari = $em->getRepository('AppBundle:Usuari')
            ->find($id);
        if ($usuari instanceof Usuari ) {
            $clases = $usuari->getClases();
            return $this->render('usuari/showClases.html.twig', array('clases' => $clases,
                'usuari' => $usuari));
        }
        return $this->render('default/message.html.twig',array("message"=>"No s'ha trobat l'usuari"));
    }
    /**
     *
     * @Route("removeUsuariclase/{user_id}/{class_id}", name="removeUsuariclases")
     *
     */
    public function removeUsuariclasesAction($class_id,$user_id, Request $request){
        $em=$this->getDoctrine()->getManager();
        $clase = $em->getRepository('AppBundle:Clase')
            ->find($class_id);
        $usuari = $em->getRepository('AppBundle:Usuari')
            ->find($user_id);
        if ($clase instanceof Clase && $usuari instanceof Usuari ){
            $clase->removeUsuari($usuari);
            $em->persist($clase);
            $em->flush();
            return $this->redirectToRoute("showUsuariClsaes", array('id' => $user_id));
        }
        return $this->render('default/message.html.twig',array("message"=>"Error la clase o l'usuari no existeix
        "));
    }

    /**
     * @Route("/selectAllUsers/{id}", name="selectAllUsers",  defaults={"id" = 0})
     */
    public function selectAllUsersAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id==0){
        $usuaris = $this->getDoctrine()
            ->getRepository('AppBundle:Usuari')
            ->findAll();
        }
        if ($id==2){
            $usuaris = $em->getRepository('AppBundle:Usuari')
            ->findBy(array(), array('nom'=>'ASC'));
        }
        if ($id==3){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Usuari u
                ORDER BY u.cognom1 DESC'
            );
            $usuaris = $query->getResult();
        }
        if ($id==4){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Usuari u
                ORDER BY u.cognom2 DESC'
            );
            $usuaris = $query->getResult();
        }
        if ($id==6){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Usuari u
                ORDER BY u.dni DESC'
            );
            $usuaris = $query->getResult();
        }
        if($id==9){

            $query = $em->createQuery(
                'SELECT u 
                FROM AppBundle:Usuari u
                ORDER BY u.inscripcio DESC'
            );
            $usuaris = $query->getResult(); 
         }

        if (count($usuaris)==0) {
             return  $this->redirectToRoute('insertUsuari');
        }
        return $this->render('usuari/content.html.twig', array(
            'usuari' => $usuaris));
    }

    /**
     * @Route("/updateUsuari", name="updateUsuari")
     */
    public function updateUsuariAction(Request $request)
    {
        $usuari = new Usuari();

        $form = $this->createFormBuilder($usuari)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $usuari = $em->getRepository('AppBundle:Usuari')
            ->findOneBy(
                array('cognom1'=>$usuari->getCognom1(), 'cognom2'=>$usuari->getCognom2())
              );
                
            if (!$usuari) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No em trobat aquest usuari'));
            }
            return $this->redirectToRoute('editarUsuari',array('nom' => $usuari->getNom()));

        }
        return $this->render('usuari/form.html.twig', array(
            'title' => 'Modificar usuari',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editarUsuari/{nom}", name="editarUsuari")
     */
    public function editarUsuariAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $usuari = $em->getRepository('AppBundle:Usuari')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($usuari)
            ->add('nom', TextType::class)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('edat',NumberType::class)
            ->add('inscripcio',DateType::class)
            ->add('quota',NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Modificar'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Usuari actualitzat: '. $usuari->getId() .' / '
                                               . $usuari->getNom() .' / '
                                               . $usuari->getCognom1() .' / '
                                               . $usuari->getCognom2() .' / '
                                               . $usuari->getEdat() .' / '
                                               . $usuari->getInscripcio()->format('d/m/y') .' / '
                                               . $usuari->getQuota() 
            ));
        }
        return $this->render('usuari/form.html.twig', array(
            'title' => 'Editar l`usuari',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeUsuari/{id}", name="removeUsuari")
     */
    public function removeProductAction($id,Request $request)
    {
       
            $em = $this->getDoctrine()->getManager();
            $usuari = $em->getRepository('AppBundle:Usuari')
            ->find($id); 
            $nom = $usuari->getNom();
            $cognom1 = $usuari->getCognom1();
            $cognom2 = $usuari->getCognom2();
            $em->remove($usuari);
            $em->flush();
            
            return $this->redirectToRoute('selectAllUsers');

        
    }
}
