<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Squash;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SquashController extends Controller
{

	/**
     *  @Route("/insertPista", name="insertPista")
     */
     public function insertPistaAction(Request $request)
    {
        $pista = new Squash();

        $form = $this->createFormBuilder($pista)
            ->add('numero_pista',NumberType::class, array('attr'=>array('class'=>'form-control')))
            ->add('nom',TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $pista->setEstat(0);
            $em->persist($pista);
            $em->flush();
           return  $this->redirectToRoute('selectAllPistes');
        }
        return $this->render('pista/form.html.twig', array(
            'title' => 'Insertar pista',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectPista", name="selectPista")
     */
    public function selectPistaAction(Request $request)
    {
        $pista = new Squash();

        $form = $this->createFormBuilder($pista)
            ->add('numeropista',NumberType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pista = $this->getDoctrine()
                ->getRepository('AppBundle:Squash')
                ->findBy(
                    array('numeroPista'=>$pista->getNumeroPista()) 
                    );
            if (count($pista)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'Pista no trobada'));
            }
            return $this->render('pista/content.html.twig', array(
                'pista' => $pista));
        }
        return $this->render('pista/form.html.twig', array(
            'title' => 'Seleccionar Pista',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllPistes", name="selectAllPistes")
     */
    public function selectAllPistesAction()
    {
        $pistes = $this->getDoctrine()
            ->getRepository('AppBundle:Squash')
            ->findAll();

        if (count($pistes)==0) {
             return  $this->redirectToRoute('insertPista');
        }
        return $this->render('pista/content.html.twig', array(
            'pista' => $pistes));
    }

}
