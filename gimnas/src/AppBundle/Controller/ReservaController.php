<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reserva;
use AppBundle\Entity\Squash;
use AppBundle\Entity\Usuari;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
class ReservaController extends Controller
{

    /**
     *  @Route("/insertReserva", name="insertReserva")
     */
    public function insertReservaAction(Request $request)
    {
        $reserva = new Reserva();

        $form = $this->createFormBuilder($reserva)
            ->add('pista', EntityType::class, array('class' => 'AppBundle:Squash', 'choice_label' => 'numero_pista'))
            ->add('usuari', EntityType::class, array('class' => 'AppBundle:Usuari', 
                                                          'choice_label' => 'cognom1','attr'=>array('class'=>'form-group')))
            ->add('data', DateType::class)
            ->add('hora', TimeType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid() ){

            if($reserva->getPista()->getEstat()==0) {
                $em = $this->getDoctrine()->getManager();
                if( $reserva->getData()>=new \DateTime()) {
                    $em->persist($reserva);
                    $reserva->getPista()->setEstat(1);
                    $em->flush();
                    return  $this->redirectToRoute('selectAllReserves');
                }
                else{
                    return $this->render('default/message.html.twig', array('message'=>'Data incorrecte'));
                }
            }
            else {
                 return $this->render('default/message.html.twig', array(
                    'message' => 'La pista ja esta alquilada'
                ));
            }
        }
        $pistes = $this->getDoctrine()
            ->getRepository('AppBundle:Squash')
            ->findAll();

        return $this->render('reserva/form.html.twig', array(
            'title' => 'Insertar reserva',
            'form' => $form->createView(),
            'pistes' => $pistes,
        ));
    }

    /**
     * @Route("/selectReserva", name="selectReserva")
     */
    public function selectReservaAction(Request $request)
    {
        $reserva = new Reserva();

        $form = $this->createFormBuilder($reserva)
            ->add('pista', EntityType::class, array('class' => 'AppBundle:Squash', 'choice_label' => 'numero_pista'))
            ->add('usuari', EntityType::class, array('class' => 'AppBundle:Usuari', 
                                                          'choice_label' => 'cognom1'))
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reserva = $this->getDoctrine()
                ->getRepository('AppBundle:Reserva')
                ->findBy(
                    array('pista'=>$reserva->getPista(),'usuari'=>$reserva->getUsuari() ) 
                    );
            if (count($reserva)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No s`ha trobat la reserva'));
            }
            return $this->render('reserva/content.html.twig', array(
                'reserva' => $reserva));
        }
        $pistes = $this->getDoctrine()
                ->getRepository('AppBundle:Squash')
                ->findAll();

        return $this->render('reserva/form.html.twig', array(
            'title' => 'Seleccionar Reserva',
            'form' => $form->createView(),
            'pistes' => $pistes,
        ));
    }

    /**
     * @Route("/selectAllReserves", name="selectAllReserves")
     */
    public function selectAllReservesAction()
    {
        $reserves = $this->getDoctrine()
            ->getRepository('AppBundle:Reserva')
            ->findAll();

        if (count($reserves)==0) {
             return  $this->redirectToRoute('insertReserva');
        }
        return $this->render('reserva/content.html.twig', array(
            'reserva' => $reserves));
    }
    
    /**
     * @Route("/removeReserva/{id}", name="removeReserva")
     */
    public function removeReservaAction($id,Request $request)
    {
       
        $em = $this->getDoctrine()->getManager();
        $reserva = $em->getRepository('AppBundle:Reserva')
        ->find($id); 

            $reserva->getPista()->setEstat(0);
            $em->remove($reserva);
            $em->flush();

            return $this->redirectToRoute('selectAllReserves');
    }

}
