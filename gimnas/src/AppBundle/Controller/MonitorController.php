<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Monitor;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class MonitorController extends Controller
{

    /**
     *  @Route("/insertMonitor", name="insertMonitor")
     */
     public function insertMonitorAction(Request $request)
    {
        $monitor = new Monitor();

        $form = $this->createFormBuilder($monitor)
            ->add('nom', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('cognom1', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('cognom2', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('sou',NumberType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Create','attr'=>array('class'=>'btn btn-primary')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($monitor);
            $em->flush();
            return $this->redirectToRoute('selectAllMonitors');
        }
        return $this->render('monitor/form.html.twig', array(
            'title' => 'Insertar monitor',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllCognom1", name="selectAllCognom1")
     */
    public function selectAllCognom1Action()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT m 
            FROM AppBundle:Monitor m
            ORDER BY m.cognom1 ASC'
        );

        $monitors = $query->getResult(); 

        if (count($monitors)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No s`han trobat monitors'));
        }
        return $this->render('monitor/content.html.twig', array(
            'monitor' => $monitors));
    }

    /**
     * @Route("/selectMonitor", name="selectMonitor")
     */
    public function selectMonitorAction(Request $request)
    {
        $monitor = new Monitor();

        $form = $this->createFormBuilder($monitor)
            ->add('cognom1', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('cognom2', TextType::class, array('attr'=>array('class'=>'form-control')))
            ->add('save', SubmitType::class, array('label' => 'Select','attr'=>array('class'=>'btn btn-primary')))
            ->getForm();
            
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $monitor = $this->getDoctrine()
                ->getRepository('AppBundle:Monitor')
                ->findBy(
                    array('cognom1'=>$monitor->getCognom1(), 'cognom2'=>$monitor->getCognom2()) 
                    );
            if (count($monitor)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'monitor no trobat'));
            }
            return $this->render('monitor/content.html.twig', array(
                'monitor' => $monitor));
        }
        return $this->render('monitor/form.html.twig', array(
            'title' => 'Seleccionar Monitor',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllMonitors/{id}", name="selectAllMonitors",defaults={"id" = 0})
     */
    public function selectAllMonitorsAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id == 0){
        $monitors = $this->getDoctrine()
            ->getRepository('AppBundle:Monitor')
            ->findAll();
        }
        if ($id == 1 ){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Monitor u
                ORDER BY u.id DESC'
            );
            $monitors = $query->getResult();
        }
        if ($id == 2 ){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Monitor u
                ORDER BY u.nom DESC'
            );
            $monitors = $query->getResult();
        }
        if ($id == 3 ){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Monitor u
                ORDER BY u.cognom1 DESC'
            );
            $monitors = $query->getResult();
        }
        if ($id == 4 ){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Monitor u
                ORDER BY u.cognom2 DESC'
            );
            $monitors = $query->getResult();
        }
        if ($id == 5 ){
            $query = $em->createQuery(
                'SELECT u
                FROM AppBundle:Monitor u
                ORDER BY u.sou DESC'
            );
            $monitors = $query->getResult();
        }

        if (count($monitors)==0) {
            return  $this->redirectToRoute('insertMonitor');
        }
        return $this->render('monitor/content.html.twig', array(
            'monitor' => $monitors));
    }

    /**
     * @Route("/updateMonitor", name="updateMonitor")
     */
    public function updateMonitorAction(Request $request)
    {
        $monitor = new Monitor();

        $form = $this->createFormBuilder($monitor)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $monitor = $em->getRepository('AppBundle:Monitor')
            ->findOneBy(
                array('cognom1'=>$monitor->getCognom1(), 'cognom2'=>$monitor->getCognom2())
              );
                
            if (!$monitor) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No em trobat aquest monitor'));
            }
            return $this->redirectToRoute('editarMonitor',array('nom' => $monitor->getNom()));

        }
        return $this->render('monitor/form.html.twig', array(
            'title' => 'Modificar monitor',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editarMonitor/{nom}", name="editarMonitor")
     */
    public function editarMonitorAction($nom, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $monitor = $em->getRepository('AppBundle:Monitor')
           ->findOneByNom($nom);

        $form = $this->createFormBuilder($monitor)
            ->add('nom', TextType::class)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('sou',NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Modificar'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->redirectToRoute('selectAllMonitors');
        }
        return $this->render('monitor/form.html.twig', array(
            'title' => 'Editar el monitor',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeMonitor/{id}", name="removeMonitor")
     */
    public function removeProductAction(Request $request, $id)
    {

            $em = $this->getDoctrine()->getManager();
            $monitor = $em->getRepository('AppBundle:Monitor')
            ->find($id); 

            $nom = $monitor->getNom();
            $cognom1 = $monitor->getCognom1();
            $cognom2 = $monitor->getCognom2();
            $em->remove($monitor);
            $em->flush();
            return $this->redirectToRoute('selectAllMonitors');
    }
}
