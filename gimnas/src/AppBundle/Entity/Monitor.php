<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Monitor
 *
 * @ORM\Table(name="monitor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MonitorRepository")
 */
class Monitor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\OneToMany(targetEntity="Clase", mappedBy="monitor")
    */
    protected $clases;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=35)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="cognom1", type="string", length=35)
     */
    private $cognom1;

    /**
     * @var string
     *
     * @ORM\Column(name="cognom2", type="string", length=35)
     */
    private $cognom2;

    /**
     * @var float
     *
     * @ORM\Column(name="sou", type="float")
     */
    private $sou;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Monitor
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cognom1
     *
     * @param string $cognom1
     *
     * @return Monitor
     */
    public function setCognom1($cognom1)
    {
        $this->cognom1 = $cognom1;

        return $this;
    }

    /**
     * Get cognom1
     *
     * @return string
     */
    public function getCognom1()
    {
        return $this->cognom1;
    }

    /**
     * Set cognom2
     *
     * @param string $cognom2
     *
     * @return Monitor
     */
    public function setCognom2($cognom2)
    {
        $this->cognom2 = $cognom2;

        return $this;
    }

    /**
     * Get cognom2
     *
     * @return string
     */
    public function getCognom2()
    {
        return $this->cognom2;
    }

    /**
     * Set sou
     *
     * @param float $sou
     *
     * @return Monitor
     */
    public function setSou($sou)
    {
        $this->sou = $sou;

        return $this;
    }

    /**
     * Get sou
     *
     * @return float
     */
    public function getSou()
    {
        return $this->sou;
    }

    /**
     * Add clase
     *
     * @param \AppBundle\Entity\Clase $clase
     *
     * @return Monitor
     */
    public function addClase(\AppBundle\Entity\Clase $clase)
    {
        $this->clases[] = $clase;

        return $this;
    }

    /**
     * Remove clase
     *
     * @param \AppBundle\Entity\Clase $clase
     */
    public function removeClase(\AppBundle\Entity\Clase $clase)
    {
        $this->clases->removeElement($clase);
    }

    /**
     * Get clases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClases()
    {
        return $this->clases;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clases = new \Doctrine\Common\Collections\ArrayCollection();
    }

}
