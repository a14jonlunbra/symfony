<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuari
 *
 * @ORM\Table(name="usuari")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuariRepository")
 */
class Usuari
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Reserva", mappedBy="usuari")
     */
    protected $reserves;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=35)
     */
    private $nom;

/**
     * @ORM\ManyToMany(targetEntity="Clase", mappedBy="usuaris")
     */
    protected $clases;
    /**
     * @var string
     *
     * @ORM\Column(name="cognom1", type="string", length=35)
     */
    private $cognom1;

    /**
     * @var string
     *
     * @ORM\Column(name="cognom2", type="string", length=35)
     */
    private $cognom2;

    /**
     * @var int
     *
     * @ORM\Column(name="edat", type="integer")
     */
    private $edat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscripcio", type="date")
     */
    private $inscripcio;

    /**
     * @var float
     *
     * @ORM\Column(name="quota", type="float")
     */
    private $quota;
    /**
     * @var int
     *
     * @ORM\Column(name="telefon", type="integer")
     */
    private $telefon;
    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=9)
     */
    private $dni;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=35)
     */
    private $email;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Usuari
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cognom1
     *
     * @param string $cognom1
     *
     * @return Usuari
     */
    public function setCognom1($cognom1)
    {
        $this->cognom1 = $cognom1;

        return $this;
    }

    /**
     * Get cognom1
     *
     * @return string
     */
    public function getCognom1()
    {
        return $this->cognom1;
    }

    /**
     * Set cognom2
     *
     * @param string $cognom2
     *
     * @return Usuari
     */
    public function setCognom2($cognom2)
    {
        $this->cognom2 = $cognom2;

        return $this;
    }

    /**
     * Get cognom2
     *
     * @return string
     */
    public function getCognom2()
    {
        return $this->cognom2;
    }

    /**
     * Set edat
     *
     * @param integer $edat
     *
     * @return Usuari
     */
    public function setEdat($edat)
    {
        $this->edat = $edat;

        return $this;
    }

    /**
     * Get edat
     *
     * @return int
     */
    public function getEdat()
    {
        return $this->edat;
    }

    /**
     * Set inscripcio
     *
     * @param \DateTime $inscripcio
     *
     * @return Usuari
     */
    public function setInscripcio($inscripcio)
    {
        $this->inscripcio = $inscripcio;

        return $this;
    }

    /**
     * Get inscripcio
     *
     * @return \DateTime
     */
    public function getInscripcio()
    {
        return $this->inscripcio;
    }

    /**
     * Set quota
     *
     * @param float $quota
     *
     * @return Usuari
     */
    public function setQuota($quota)
    {
        $this->quota = $quota;

        return $this;
    }

    /**
     * Get quota
     *
     * @return float
     */
    public function getQuota()
    {
        return $this->quota;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reserves = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clases = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reserf
     *
     * @param \AppBundle\Entity\Reserva $reserf
     *
     * @return Usuari
     */
    public function addReserf(\AppBundle\Entity\Reserva $reserf)
    {
        $this->reserves[] = $reserf;

        return $this;
    }

    /**
     * Remove reserf
     *
     * @param \AppBundle\Entity\Reserva $reserf
     */
    public function removeReserf(\AppBundle\Entity\Reserva $reserf)
    {
        $this->reserves->removeElement($reserf);
    }

    /**
     * Get reserves
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReserves()
    {
        return $this->reserves;
    }

    /**
     * Add clase
     *
     * @param \AppBundle\Entity\Clase $clase
     *
     * @return Usuari
     */
    public function addClase(\AppBundle\Entity\Clase $clase)
    {
        $this->clases[] = $clase;

        return $this;
    }

    /**
     * Remove clase
     *
     * @param \AppBundle\Entity\Clase $clase
     */
    public function removeClase(\AppBundle\Entity\Clase $clase)
    {
        $this->clases->removeElement($clase);
    }

    /**
     * Get clases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClases()
    {
        return $this->clases;
    }

    /**
     * Set telefon
     *
     * @param integer $telefon
     *
     * @return Usuari
     */
    public function setTelefon($telefon)
    {
        $this->telefon = $telefon;

        return $this;
    }

    /**
     * Get telefon
     *
     * @return integer
     */
    public function getTelefon()
    {
        return $this->telefon;
    }

    /**
     * Set dni
     *
     * @param string $dni
     *
     * @return Usuari
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Usuari
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
