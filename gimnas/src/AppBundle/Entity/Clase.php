<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clase
 *
 * @ORM\Table(name="clase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClaseRepository")
 */
class Clase
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Monitor", inversedBy="clases")
     * @ORM\JoinColumn(name="monitor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $monitor;
    /**
     * @ORM\ManyToMany(targetEntity="Usuari", inversedBy="clases")
     * @ORM\JoinTable(name="usuaris_clases")
     */
    
    protected $usuaris;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=35, unique=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcio", type="string", length=150, nullable=true)
     */
    private $descripcio;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Clase
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set descripcio
     *
     * @param string $descripcio
     *
     * @return Clase
     */
    public function setDescripcio($descripcio)
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    /**
     * Get descripcio
     *
     * @return string
     */
    public function getDescripcio()
    {
        return $this->descripcio;
    }

    /**
     * Set monitor
     *
     * @param \AppBundle\Entity\Monitor $monitor
     *
     * @return Clase
     */
    public function setMonitor(\AppBundle\Entity\Monitor $monitor = null)
    {
        $this->monitor = $monitor;

        return $this;
    }

    /**
     * Get monitor
     *
     * @return \AppBundle\Entity\Monitor
     */
    public function getMonitor()
    {
        return $this->monitor;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->usuaris = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Clase
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Add usuari
     *
     * @param \AppBundle\Entity\Usuari $usuari
     *
     * @return Clase
     */
    public function addUsuari(\AppBundle\Entity\Usuari $usuari)
    {
        $this->usuaris[] = $usuari;

        return $this;
    }

    /**
     * Remove usuari
     *
     * @param \AppBundle\Entity\Usuari $usuari
     */
    public function removeUsuari(\AppBundle\Entity\Usuari $usuari)
    {
        $this->usuaris->removeElement($usuari);
    }

    /**
     * Get usuaris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsuaris()
    {
        return $this->usuaris;
    }
}
