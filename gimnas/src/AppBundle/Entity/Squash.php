<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Squash
 *
 * @ORM\Table(name="squash")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SquashRepository")
 */
class Squash
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Reserva", mappedBy="pista")
     */
    protected $reserves;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_pista", type="integer", unique=true)
     */
    private $numeroPista;

    /**
     * @var String
     *
     * @ORM\Column(name="nom", type="string" )
     */
    private $nom;

    /**
     * @var bool
     *
     * @ORM\Column(name="estat", type="boolean")
     */
    private $estat;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroPista
     *
     * @param integer $numeroPista
     *
     * @return Squash
     */
    public function setNumeroPista($numeroPista)
    {
        $this->numeroPista = $numeroPista;

        return $this;
    }

    /**
     * Get numeroPista
     *
     * @return int
     */
    public function getNumeroPista()
    {
        return $this->numeroPista;
    }

    /**
     * Set estat
     *
     * @param boolean $estat
     *
     * @return Squash
     */
    public function setEstat($estat)
    {
        $this->estat = $estat;

        return $this;
    }

    /**
     * Get estat
     *
     * @return bool
     */
    public function getEstat()
    {
        return $this->estat;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reserves = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reserf
     *
     * @param \AppBundle\Entity\Reserva $reserf
     *
     * @return Squash
     */
    public function addReserf(\AppBundle\Entity\Reserva $reserf)
    {
        $this->reserves[] = $reserf;

        return $this;
    }

    /**
     * Remove reserf
     *
     * @param \AppBundle\Entity\Reserva $reserf
     */
    public function removeReserf(\AppBundle\Entity\Reserva $reserf)
    {
        $this->reserves->removeElement($reserf);
    }

    /**
     * Get reserves
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReserves()
    {
        return $this->reserves;
    }
    

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Squash
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
}
