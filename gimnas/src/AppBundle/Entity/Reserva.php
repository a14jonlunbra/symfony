<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reserva
 *
 * @ORM\Table(name="reserva")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReservaRepository")
 */
class Reserva
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Squash", inversedBy="reserves")
     * @ORM\JoinColumn(name="pista_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $pista;

    /**
     * @ORM\ManyToOne(targetEntity="Usuari", inversedBy="reserves")
     * @ORM\JoinColumn(name="usuari_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $usuari;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="date" )
     */
    private $data;

    /**
     * @var string
     *
     * @ORM\Column(name="hora", type="time")
     */
    private $hora;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Reserva
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set hora
     *
     * @param string $hora
     *
     * @return Reserva
     */
    public function setHora($hora)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return string
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set pista
     *
     * @param \AppBundle\Entity\Squash $pista
     *
     * @return Reserva
     */
    public function setPista(\AppBundle\Entity\Squash $pista = null)
    {
        $this->pista = $pista;

        return $this;
    }

    /**
     * Get pista
     *
     * @return \AppBundle\Entity\Squash
     */
    public function getPista()
    {
        return $this->pista;
    }

    /**
     * Set usuari
     *
     * @param \AppBundle\Entity\Usuari $usuari
     *
     * @return Reserva
     */
    public function setUsuari(\AppBundle\Entity\Usuari $usuari = null)
    {
        $this->usuari = $usuari;

        return $this;
    }

    /**
     * Get usuari
     *
     * @return \AppBundle\Entity\Usuari
     */
    public function getUsuari()
    {
        return $this->usuari;
    }
}
