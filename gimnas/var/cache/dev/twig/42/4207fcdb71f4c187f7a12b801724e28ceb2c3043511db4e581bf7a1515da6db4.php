<?php

/* pista/content.html.twig */
class __TwigTemplate_1927237e4fa2c76201abbf61dc2c8d3fc528872b4e7d4a2aaf70025be6c0e9c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "pista/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_97f99078a1ec3b014eacbfe1593a07a06f5832a8d5afca8e4553d0cc1ebdb6da = $this->env->getExtension("native_profiler");
        $__internal_97f99078a1ec3b014eacbfe1593a07a06f5832a8d5afca8e4553d0cc1ebdb6da->enter($__internal_97f99078a1ec3b014eacbfe1593a07a06f5832a8d5afca8e4553d0cc1ebdb6da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "pista/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_97f99078a1ec3b014eacbfe1593a07a06f5832a8d5afca8e4553d0cc1ebdb6da->leave($__internal_97f99078a1ec3b014eacbfe1593a07a06f5832a8d5afca8e4553d0cc1ebdb6da_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_7aefa9a79d4e34b2d6436909119d89d12cbaf0f213f1fc22390975398a7165b1 = $this->env->getExtension("native_profiler");
        $__internal_7aefa9a79d4e34b2d6436909119d89d12cbaf0f213f1fc22390975398a7165b1->enter($__internal_7aefa9a79d4e34b2d6436909119d89d12cbaf0f213f1fc22390975398a7165b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_7aefa9a79d4e34b2d6436909119d89d12cbaf0f213f1fc22390975398a7165b1->leave($__internal_7aefa9a79d4e34b2d6436909119d89d12cbaf0f213f1fc22390975398a7165b1_prof);

    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_873b799c5317552c09aa99a529edfc7b4e914d9bc0fbffb2c94e812d685b8b40 = $this->env->getExtension("native_profiler");
        $__internal_873b799c5317552c09aa99a529edfc7b4e914d9bc0fbffb2c94e812d685b8b40->enter($__internal_873b799c5317552c09aa99a529edfc7b4e914d9bc0fbffb2c94e812d685b8b40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Pistes</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllPistes\">Llistar totes les pistes</a>
                </li>
                <li>
                    <a href=\"/insertPista\">Insertar una nova pista</a></li>
                <li>
                    <a href=\"/selectPista\">Seleccionar una pista</a>
                </li>
";
        
        $__internal_873b799c5317552c09aa99a529edfc7b4e914d9bc0fbffb2c94e812d685b8b40->leave($__internal_873b799c5317552c09aa99a529edfc7b4e914d9bc0fbffb2c94e812d685b8b40_prof);

    }

    // line 67
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_66fb30c2975998819b95f9225c6b5f29ea639f8adbe8bbc4273759472be80c9e = $this->env->getExtension("native_profiler");
        $__internal_66fb30c2975998819b95f9225c6b5f29ea639f8adbe8bbc4273759472be80c9e->enter($__internal_66fb30c2975998819b95f9225c6b5f29ea639f8adbe8bbc4273759472be80c9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 68
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>numero de pista</th>
                <th>estat</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 81
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pista"]) ? $context["pista"] : $this->getContext($context, "pista")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 82
            echo "                <tr>
                    <td>";
            // line 83
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "numeroPista", array()), "html", null, true);
            echo "</td>
                    ";
            // line 85
            if (($this->getAttribute($context["p"], "estat", array()) == 0)) {
                // line 86
                echo "                        <td> Pista ocupada </td>
                    ";
            } else {
                // line 88
                echo "                        <td> Pista buida </td>
                    ";
            }
            // line 90
            echo "                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_66fb30c2975998819b95f9225c6b5f29ea639f8adbe8bbc4273759472be80c9e->leave($__internal_66fb30c2975998819b95f9225c6b5f29ea639f8adbe8bbc4273759472be80c9e_prof);

    }

    public function getTemplateName()
    {
        return "pista/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 92,  166 => 90,  162 => 88,  158 => 86,  156 => 85,  152 => 84,  148 => 83,  145 => 82,  141 => 81,  126 => 68,  120 => 67,  92 => 44,  86 => 43,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/pista/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Pistes</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllPistes">Llistar totes les pistes</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertPista">Insertar una nova pista</a></li>*/
/*                 <li>*/
/*                     <a href="/selectPista">Seleccionar una pista</a>*/
/*                 </li>*/
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>numero de pista</th>*/
/*                 <th>estat</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for p in pista %}*/
/*                 <tr>*/
/*                     <td>{{ p.id }}</td>*/
/*                     <td>{{ p.numeroPista }}</td>*/
/*                     {% if(p.estat==0) %}*/
/*                         <td> Pista ocupada </td>*/
/*                     {% else %}*/
/*                         <td> Pista buida </td>*/
/*                     {% endif %}*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
