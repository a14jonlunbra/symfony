<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_42641ce622ec74937b9291ef04a56ad0db7b9c1e51bd84dc0904b4b2cca724b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ead206e85a4ffc4561af8c50a80a95f45cb9d1e0b3f56dd02d17753532e256b = $this->env->getExtension("native_profiler");
        $__internal_5ead206e85a4ffc4561af8c50a80a95f45cb9d1e0b3f56dd02d17753532e256b->enter($__internal_5ead206e85a4ffc4561af8c50a80a95f45cb9d1e0b3f56dd02d17753532e256b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ead206e85a4ffc4561af8c50a80a95f45cb9d1e0b3f56dd02d17753532e256b->leave($__internal_5ead206e85a4ffc4561af8c50a80a95f45cb9d1e0b3f56dd02d17753532e256b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c105edde58247dd4b604920b2a06b95dcb6d964ad7d54af5294c14d6cba6bbb9 = $this->env->getExtension("native_profiler");
        $__internal_c105edde58247dd4b604920b2a06b95dcb6d964ad7d54af5294c14d6cba6bbb9->enter($__internal_c105edde58247dd4b604920b2a06b95dcb6d964ad7d54af5294c14d6cba6bbb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c105edde58247dd4b604920b2a06b95dcb6d964ad7d54af5294c14d6cba6bbb9->leave($__internal_c105edde58247dd4b604920b2a06b95dcb6d964ad7d54af5294c14d6cba6bbb9_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_0d4fc6714590b514f728618d9aca6dfca5d08d8a37fe82ee527967b88e02f1e7 = $this->env->getExtension("native_profiler");
        $__internal_0d4fc6714590b514f728618d9aca6dfca5d08d8a37fe82ee527967b88e02f1e7->enter($__internal_0d4fc6714590b514f728618d9aca6dfca5d08d8a37fe82ee527967b88e02f1e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0d4fc6714590b514f728618d9aca6dfca5d08d8a37fe82ee527967b88e02f1e7->leave($__internal_0d4fc6714590b514f728618d9aca6dfca5d08d8a37fe82ee527967b88e02f1e7_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_b1e5dd0d8088b9e0698a116281ebd409d241d05622c5ece4bb256bf13e30a73f = $this->env->getExtension("native_profiler");
        $__internal_b1e5dd0d8088b9e0698a116281ebd409d241d05622c5ece4bb256bf13e30a73f->enter($__internal_b1e5dd0d8088b9e0698a116281ebd409d241d05622c5ece4bb256bf13e30a73f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_b1e5dd0d8088b9e0698a116281ebd409d241d05622c5ece4bb256bf13e30a73f->leave($__internal_b1e5dd0d8088b9e0698a116281ebd409d241d05622c5ece4bb256bf13e30a73f_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
