<?php

/* principal/pista.html.twig */
class __TwigTemplate_0ed38f3e358ddb488d623662d8338286985e7bb0bedcd68bf535ae85dc9b5981 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/pista.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f5976669c07c55b67a2d5840bd84af6d4f6238863b85fdced9e2dda8ce3bef5 = $this->env->getExtension("native_profiler");
        $__internal_2f5976669c07c55b67a2d5840bd84af6d4f6238863b85fdced9e2dda8ce3bef5->enter($__internal_2f5976669c07c55b67a2d5840bd84af6d4f6238863b85fdced9e2dda8ce3bef5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/pista.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2f5976669c07c55b67a2d5840bd84af6d4f6238863b85fdced9e2dda8ce3bef5->leave($__internal_2f5976669c07c55b67a2d5840bd84af6d4f6238863b85fdced9e2dda8ce3bef5_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_7d22f0114ea63c8cbd141b1f170026e8da3fe8d91adfaaf4499f4733f6f396de = $this->env->getExtension("native_profiler");
        $__internal_7d22f0114ea63c8cbd141b1f170026e8da3fe8d91adfaaf4499f4733f6f396de->enter($__internal_7d22f0114ea63c8cbd141b1f170026e8da3fe8d91adfaaf4499f4733f6f396de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Pista</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 12
        echo "        </div>
";
        
        $__internal_7d22f0114ea63c8cbd141b1f170026e8da3fe8d91adfaaf4499f4733f6f396de->leave($__internal_7d22f0114ea63c8cbd141b1f170026e8da3fe8d91adfaaf4499f4733f6f396de_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_28adba33c55196fd466765a34f6ed960749387c42bcec3ca876dd99d228c3fbf = $this->env->getExtension("native_profiler");
        $__internal_28adba33c55196fd466765a34f6ed960749387c42bcec3ca876dd99d228c3fbf->enter($__internal_28adba33c55196fd466765a34f6ed960749387c42bcec3ca876dd99d228c3fbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllPistes\">Llistar totes les pistes</a><br>
                <a href=\"/insertPista\">Insertar una nova pista</a><br>
                <a href=\"/selectPista\">Seleccionar una pista</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_28adba33c55196fd466765a34f6ed960749387c42bcec3ca876dd99d228c3fbf->leave($__internal_28adba33c55196fd466765a34f6ed960749387c42bcec3ca876dd99d228c3fbf_prof);

    }

    public function getTemplateName()
    {
        return "principal/pista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 12,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/pista.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Pista</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllPistes">Llistar totes les pistes</a><br>*/
/*                 <a href="/insertPista">Insertar una nova pista</a><br>*/
/*                 <a href="/selectPista">Seleccionar una pista</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
