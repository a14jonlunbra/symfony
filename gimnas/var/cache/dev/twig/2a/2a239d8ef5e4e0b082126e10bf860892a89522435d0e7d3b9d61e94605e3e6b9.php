<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_c323853229c483e92c07ce0acf7f82ab4b20563cee9cc719e778b7e3a2170009 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_da8f82beb07a5f90c681868347e8de0fc714762bc29617641b14953658edb86c = $this->env->getExtension("native_profiler");
        $__internal_da8f82beb07a5f90c681868347e8de0fc714762bc29617641b14953658edb86c->enter($__internal_da8f82beb07a5f90c681868347e8de0fc714762bc29617641b14953658edb86c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_da8f82beb07a5f90c681868347e8de0fc714762bc29617641b14953658edb86c->leave($__internal_da8f82beb07a5f90c681868347e8de0fc714762bc29617641b14953658edb86c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4340fb038ff26d04ff27e2a01d7361ebc4a398fdde0d70f456e18f327852dece = $this->env->getExtension("native_profiler");
        $__internal_4340fb038ff26d04ff27e2a01d7361ebc4a398fdde0d70f456e18f327852dece->enter($__internal_4340fb038ff26d04ff27e2a01d7361ebc4a398fdde0d70f456e18f327852dece_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_4340fb038ff26d04ff27e2a01d7361ebc4a398fdde0d70f456e18f327852dece->leave($__internal_4340fb038ff26d04ff27e2a01d7361ebc4a398fdde0d70f456e18f327852dece_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_91e63cdd5fcc4fd921e25a84f8e7c086f90b3fa669ea9a63f766787d98330b6e = $this->env->getExtension("native_profiler");
        $__internal_91e63cdd5fcc4fd921e25a84f8e7c086f90b3fa669ea9a63f766787d98330b6e->enter($__internal_91e63cdd5fcc4fd921e25a84f8e7c086f90b3fa669ea9a63f766787d98330b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_91e63cdd5fcc4fd921e25a84f8e7c086f90b3fa669ea9a63f766787d98330b6e->leave($__internal_91e63cdd5fcc4fd921e25a84f8e7c086f90b3fa669ea9a63f766787d98330b6e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f4ffcb0c58794ca9fe473dd13fe19da5a0706ca59e8f2886dfe945a318b386a = $this->env->getExtension("native_profiler");
        $__internal_6f4ffcb0c58794ca9fe473dd13fe19da5a0706ca59e8f2886dfe945a318b386a->enter($__internal_6f4ffcb0c58794ca9fe473dd13fe19da5a0706ca59e8f2886dfe945a318b386a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_6f4ffcb0c58794ca9fe473dd13fe19da5a0706ca59e8f2886dfe945a318b386a->leave($__internal_6f4ffcb0c58794ca9fe473dd13fe19da5a0706ca59e8f2886dfe945a318b386a_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
