<?php

/* reserva/form.html.twig */
class __TwigTemplate_ad15ed7cc0c42b536022fd063b37262851927fb4fd99f3ab59ff56c16894a91c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "reserva/form.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7dc9e446185e3e3c977ce1b0a371343f225c91c87a2678a3ccbf43500ea1b74 = $this->env->getExtension("native_profiler");
        $__internal_f7dc9e446185e3e3c977ce1b0a371343f225c91c87a2678a3ccbf43500ea1b74->enter($__internal_f7dc9e446185e3e3c977ce1b0a371343f225c91c87a2678a3ccbf43500ea1b74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reserva/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7dc9e446185e3e3c977ce1b0a371343f225c91c87a2678a3ccbf43500ea1b74->leave($__internal_f7dc9e446185e3e3c977ce1b0a371343f225c91c87a2678a3ccbf43500ea1b74_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_97119b05c3f69384104a83d123404272749026cd53953505eb717a3570073cee = $this->env->getExtension("native_profiler");
        $__internal_97119b05c3f69384104a83d123404272749026cd53953505eb717a3570073cee->enter($__internal_97119b05c3f69384104a83d123404272749026cd53953505eb717a3570073cee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 7
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

\t<div class=\"container\">
\t  <h2>Estat de les pistes</h2>
\t  <button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#demo\">Veure estat</button>
\t  <div id=\"demo\" class=\"collapse\">
\t    
\t\t  <div class=\"container\">
\t\t  \t<div class=\"row\">
\t\t  \t \t";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reserva"]) ? $context["reserva"] : $this->getContext($context, "reserva")));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 17
            echo "\t\t    \t\t<div class=\"col-sm-4\" style=\"background-color:lavender;\">
\t\t    \t\t\t<h5>Numero de Pista: ";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "pista", array()), "getId", array(), "method"), "html", null, true);
            echo "</h5>
\t\t    \t\t\t";
            // line 19
            if (($this->getAttribute($this->getAttribute($context["res"], "pista", array()), "estat", array()) == 0)) {
                // line 20
                echo "\t\t    \t\t\t\t<p>Estat: Lliure</p>
\t\t    \t\t\t";
            } else {
                // line 22
                echo "              \t\t\t\t<p>Estat: Reservada</p>
        \t\t\t\t";
            }
            // line 24
            echo "\t\t    \t\t</div>
\t    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "\t  \t\t</div>
\t  \t  </div>

\t  </div>
\t</div>

";
        
        $__internal_97119b05c3f69384104a83d123404272749026cd53953505eb717a3570073cee->leave($__internal_97119b05c3f69384104a83d123404272749026cd53953505eb717a3570073cee_prof);

    }

    public function getTemplateName()
    {
        return "reserva/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 26,  86 => 24,  82 => 22,  78 => 20,  76 => 19,  72 => 18,  69 => 17,  65 => 16,  53 => 7,  49 => 6,  45 => 5,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/reserva/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     <h3> {{title}} </h3>*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* */
/* 	<div class="container">*/
/* 	  <h2>Estat de les pistes</h2>*/
/* 	  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Veure estat</button>*/
/* 	  <div id="demo" class="collapse">*/
/* 	    */
/* 		  <div class="container">*/
/* 		  	<div class="row">*/
/* 		  	 	{% for res in reserva %}*/
/* 		    		<div class="col-sm-4" style="background-color:lavender;">*/
/* 		    			<h5>Numero de Pista: {{res.pista.getId()}}</h5>*/
/* 		    			{% if res.pista.estat==0 %}*/
/* 		    				<p>Estat: Lliure</p>*/
/* 		    			{% else %}*/
/*               				<p>Estat: Reservada</p>*/
/*         				{% endif %}*/
/* 		    		</div>*/
/* 	    		{% endfor %}*/
/* 	  		</div>*/
/* 	  	  </div>*/
/* */
/* 	  </div>*/
/* 	</div>*/
/* */
/* {% endblock %}*/
