<?php

/* principal/clase.html.twig */
class __TwigTemplate_77c3ae4b772f98cc8887996cc26e7776017c900896e496ad3c111b87092829c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/clase.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_511c0257538a9e65dd5ca8d07d4601ff0d72d3ee0d4e3607d34dc18405e280c4 = $this->env->getExtension("native_profiler");
        $__internal_511c0257538a9e65dd5ca8d07d4601ff0d72d3ee0d4e3607d34dc18405e280c4->enter($__internal_511c0257538a9e65dd5ca8d07d4601ff0d72d3ee0d4e3607d34dc18405e280c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/clase.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_511c0257538a9e65dd5ca8d07d4601ff0d72d3ee0d4e3607d34dc18405e280c4->leave($__internal_511c0257538a9e65dd5ca8d07d4601ff0d72d3ee0d4e3607d34dc18405e280c4_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2e86663dd4c587e6e0fc66a02b28467e7c5afd9b647bc8073d98e5075a3d9f4e = $this->env->getExtension("native_profiler");
        $__internal_2e86663dd4c587e6e0fc66a02b28467e7c5afd9b647bc8073d98e5075a3d9f4e->enter($__internal_2e86663dd4c587e6e0fc66a02b28467e7c5afd9b647bc8073d98e5075a3d9f4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Clase</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 14
        echo "        </div>
";
        
        $__internal_2e86663dd4c587e6e0fc66a02b28467e7c5afd9b647bc8073d98e5075a3d9f4e->leave($__internal_2e86663dd4c587e6e0fc66a02b28467e7c5afd9b647bc8073d98e5075a3d9f4e_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_0ba04ba7f9a8f0bc3c67692bf18ec1c9c27bbc9e856aa7bdd7bc7a6887615722 = $this->env->getExtension("native_profiler");
        $__internal_0ba04ba7f9a8f0bc3c67692bf18ec1c9c27bbc9e856aa7bdd7bc7a6887615722->enter($__internal_0ba04ba7f9a8f0bc3c67692bf18ec1c9c27bbc9e856aa7bdd7bc7a6887615722_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllClases\">Llistar totes les clases</a><br>
                <a href=\"/insertClase\">Insertar una nova clase</a><br>
                <a href=\"/selectClase\">Seleccionar una clase</a><br>
                <a href=\"/updateClase\">Modificar una clase</a><br>
                <a href=\"/removeClase\">Eliminar una clase</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_0ba04ba7f9a8f0bc3c67692bf18ec1c9c27bbc9e856aa7bdd7bc7a6887615722->leave($__internal_0ba04ba7f9a8f0bc3c67692bf18ec1c9c27bbc9e856aa7bdd7bc7a6887615722_prof);

    }

    public function getTemplateName()
    {
        return "principal/clase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 14,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/clase.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Clase</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllClases">Llistar totes les clases</a><br>*/
/*                 <a href="/insertClase">Insertar una nova clase</a><br>*/
/*                 <a href="/selectClase">Seleccionar una clase</a><br>*/
/*                 <a href="/updateClase">Modificar una clase</a><br>*/
/*                 <a href="/removeClase">Eliminar una clase</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
