<?php

/* principal/monitor.html.twig */
class __TwigTemplate_b40caae1c67734db94c09158eda7e11c514b64e6eeda2320dbe4790c918f3c0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/monitor.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec2d0cdf38ddd3d5f9dc6e6c10bb4a3ab16c9c6649ba14c9536b6bc58374cae6 = $this->env->getExtension("native_profiler");
        $__internal_ec2d0cdf38ddd3d5f9dc6e6c10bb4a3ab16c9c6649ba14c9536b6bc58374cae6->enter($__internal_ec2d0cdf38ddd3d5f9dc6e6c10bb4a3ab16c9c6649ba14c9536b6bc58374cae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/monitor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ec2d0cdf38ddd3d5f9dc6e6c10bb4a3ab16c9c6649ba14c9536b6bc58374cae6->leave($__internal_ec2d0cdf38ddd3d5f9dc6e6c10bb4a3ab16c9c6649ba14c9536b6bc58374cae6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1531c98532755b22b511eb3ec018f9ec7a24ca019d0f87be25b8dac860d6fec6 = $this->env->getExtension("native_profiler");
        $__internal_1531c98532755b22b511eb3ec018f9ec7a24ca019d0f87be25b8dac860d6fec6->enter($__internal_1531c98532755b22b511eb3ec018f9ec7a24ca019d0f87be25b8dac860d6fec6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Monitor</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>

";
        
        $__internal_1531c98532755b22b511eb3ec018f9ec7a24ca019d0f87be25b8dac860d6fec6->leave($__internal_1531c98532755b22b511eb3ec018f9ec7a24ca019d0f87be25b8dac860d6fec6_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_5580c4b8eeaa9d4d209ea41f57689f16a86ff80fecf9878feef9ad5a39292913 = $this->env->getExtension("native_profiler");
        $__internal_5580c4b8eeaa9d4d209ea41f57689f16a86ff80fecf9878feef9ad5a39292913->enter($__internal_5580c4b8eeaa9d4d209ea41f57689f16a86ff80fecf9878feef9ad5a39292913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllMonitors\">Llistar tots els monitors</a><br>
                <a href=\"/selectAllCognom1\">Llistar tots els monitors per el primer cognom</a><br>
                <a href=\"/insertMonitor\">Insertar nou monitor</a><br>
                <a href=\"/selectMonitor\">Seleccionar un monitor</a><br>
                <a href=\"/updateMonitor\">Modificar un monitor</a><br>
                <a href=\"/removeMonitor\">Eliminar un monitor</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_5580c4b8eeaa9d4d209ea41f57689f16a86ff80fecf9878feef9ad5a39292913->leave($__internal_5580c4b8eeaa9d4d209ea41f57689f16a86ff80fecf9878feef9ad5a39292913_prof);

    }

    public function getTemplateName()
    {
        return "principal/monitor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 7,  56 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/monitor.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Monitor</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllMonitors">Llistar tots els monitors</a><br>*/
/*                 <a href="/selectAllCognom1">Llistar tots els monitors per el primer cognom</a><br>*/
/*                 <a href="/insertMonitor">Insertar nou monitor</a><br>*/
/*                 <a href="/selectMonitor">Seleccionar un monitor</a><br>*/
/*                 <a href="/updateMonitor">Modificar un monitor</a><br>*/
/*                 <a href="/removeMonitor">Eliminar un monitor</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* */
/* {% endblock %}*/
