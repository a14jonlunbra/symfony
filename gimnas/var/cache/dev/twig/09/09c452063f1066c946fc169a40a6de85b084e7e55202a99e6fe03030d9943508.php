<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_2a5ccbc98782a4ff37039a789f2ffcbcee687d4e72c5ccc3061780eb35b72567 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d8f6539aee9ce6cda6c2e6c70c4f4c97a32a72340b66f8814b9e316bdcf4daa = $this->env->getExtension("native_profiler");
        $__internal_0d8f6539aee9ce6cda6c2e6c70c4f4c97a32a72340b66f8814b9e316bdcf4daa->enter($__internal_0d8f6539aee9ce6cda6c2e6c70c4f4c97a32a72340b66f8814b9e316bdcf4daa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d8f6539aee9ce6cda6c2e6c70c4f4c97a32a72340b66f8814b9e316bdcf4daa->leave($__internal_0d8f6539aee9ce6cda6c2e6c70c4f4c97a32a72340b66f8814b9e316bdcf4daa_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e82863dd1db0bf8fe72c74e54407df0173ddbc71ce57fce9679b28001d8ebdae = $this->env->getExtension("native_profiler");
        $__internal_e82863dd1db0bf8fe72c74e54407df0173ddbc71ce57fce9679b28001d8ebdae->enter($__internal_e82863dd1db0bf8fe72c74e54407df0173ddbc71ce57fce9679b28001d8ebdae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e82863dd1db0bf8fe72c74e54407df0173ddbc71ce57fce9679b28001d8ebdae->leave($__internal_e82863dd1db0bf8fe72c74e54407df0173ddbc71ce57fce9679b28001d8ebdae_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_2055d48b9daddf608e59b74d3a1ba1d5e2ac6625d4a2ad30d3d259d917f2f8bd = $this->env->getExtension("native_profiler");
        $__internal_2055d48b9daddf608e59b74d3a1ba1d5e2ac6625d4a2ad30d3d259d917f2f8bd->enter($__internal_2055d48b9daddf608e59b74d3a1ba1d5e2ac6625d4a2ad30d3d259d917f2f8bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_2055d48b9daddf608e59b74d3a1ba1d5e2ac6625d4a2ad30d3d259d917f2f8bd->leave($__internal_2055d48b9daddf608e59b74d3a1ba1d5e2ac6625d4a2ad30d3d259d917f2f8bd_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_60c42dbf19578bb389eacb5377a55ae9bef9743ca077c331e2bc1622d98370e1 = $this->env->getExtension("native_profiler");
        $__internal_60c42dbf19578bb389eacb5377a55ae9bef9743ca077c331e2bc1622d98370e1->enter($__internal_60c42dbf19578bb389eacb5377a55ae9bef9743ca077c331e2bc1622d98370e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_60c42dbf19578bb389eacb5377a55ae9bef9743ca077c331e2bc1622d98370e1->leave($__internal_60c42dbf19578bb389eacb5377a55ae9bef9743ca077c331e2bc1622d98370e1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     {% if collector.hasexception %}*/
/*         <style>*/
/*             {{ render(path('_profiler_exception_css', { token: token })) }}*/
/*         </style>*/
/*     {% endif %}*/
/*     {{ parent() }}*/
/* {% endblock %}*/
/* */
/* {% block menu %}*/
/*     <span class="label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}">*/
/*         <span class="icon">{{ include('@WebProfiler/Icon/exception.svg') }}</span>*/
/*         <strong>Exception</strong>*/
/*         {% if collector.hasexception %}*/
/*             <span class="count">*/
/*                 <span>1</span>*/
/*             </span>*/
/*         {% endif %}*/
/*     </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     <h2>Exceptions</h2>*/
/* */
/*     {% if not collector.hasexception %}*/
/*         <div class="empty">*/
/*             <p>No exception was thrown and caught during the request.</p>*/
/*         </div>*/
/*     {% else %}*/
/*         <div class="sf-reset">*/
/*             {{ render(path('_profiler_exception', { token: token })) }}*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
