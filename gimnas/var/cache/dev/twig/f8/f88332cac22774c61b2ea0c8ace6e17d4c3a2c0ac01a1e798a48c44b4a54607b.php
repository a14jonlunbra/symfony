<?php

/* usuari/content.html.twig */
class __TwigTemplate_a5ee8c9b76b1b85ed0def38f824a828b05c9e122957c4c077c007dd084923db2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "usuari/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fed23d01f211d7fc53d5c554051b22e6a7d5862a96c40991e87d517abc90da48 = $this->env->getExtension("native_profiler");
        $__internal_fed23d01f211d7fc53d5c554051b22e6a7d5862a96c40991e87d517abc90da48->enter($__internal_fed23d01f211d7fc53d5c554051b22e6a7d5862a96c40991e87d517abc90da48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "usuari/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fed23d01f211d7fc53d5c554051b22e6a7d5862a96c40991e87d517abc90da48->leave($__internal_fed23d01f211d7fc53d5c554051b22e6a7d5862a96c40991e87d517abc90da48_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_63c187e94d700080363a1a13082e702a1185bb6ad7e1099694454a77b1dded0a = $this->env->getExtension("native_profiler");
        $__internal_63c187e94d700080363a1a13082e702a1185bb6ad7e1099694454a77b1dded0a->enter($__internal_63c187e94d700080363a1a13082e702a1185bb6ad7e1099694454a77b1dded0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_63c187e94d700080363a1a13082e702a1185bb6ad7e1099694454a77b1dded0a->leave($__internal_63c187e94d700080363a1a13082e702a1185bb6ad7e1099694454a77b1dded0a_prof);

    }

    // line 44
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_e0ef7056f1c0e4f4746f06650b3390e9ba41fe7fd956de506f8bf24ffbb55567 = $this->env->getExtension("native_profiler");
        $__internal_e0ef7056f1c0e4f4746f06650b3390e9ba41fe7fd956de506f8bf24ffbb55567->enter($__internal_e0ef7056f1c0e4f4746f06650b3390e9ba41fe7fd956de506f8bf24ffbb55567_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 45
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Usuaris</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllUsers\">Llistar tots els usuaris</a>
                </li>
                <li>
                    <a href=\"/selectUsuari\">Seleccionar un usuari</a></li>
                <li>
                    <a href=\"/insertUsuari\">Insertar nou usuari</a></li>
                <li>
                    <a href=\"/updateUsuari\">Modificar un usuari</a>
                </li>

                <li>
                    <a href=\"/removeUsuari\">Eliminar un usuari</a>
                </li>

";
        
        $__internal_e0ef7056f1c0e4f4746f06650b3390e9ba41fe7fd956de506f8bf24ffbb55567->leave($__internal_e0ef7056f1c0e4f4746f06650b3390e9ba41fe7fd956de506f8bf24ffbb55567_prof);

    }

    // line 75
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_7031e3411ea43e4894b81788d41a6cf5eabcb39106cd60d8ae4f969d4771fc4a = $this->env->getExtension("native_profiler");
        $__internal_7031e3411ea43e4894b81788d41a6cf5eabcb39106cd60d8ae4f969d4771fc4a->enter($__internal_7031e3411ea43e4894b81788d41a6cf5eabcb39106cd60d8ae4f969d4771fc4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 76
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th><a href=\"/selectAllUsers/1\">id</a></th>
                <th><a href=\"/selectAllUsers/2\">nom</a></th>
                <th><a href=\"/selectAllUsers/3\">cognom1</a></th>
                <th><a href=\"/selectAllUsers/4\">cognom2</a></th>
                <th><a href=\"/selectAllUsers/5\">edat</a></th>
                <th><a href=\"/selectAllUsers/6\">dni</a></th>
                <th><a href=\"/selectAllUsers/7\">email</a></th>
                <th><a href=\"/selectAllUsers/8\">telefon</a></th>
                <th><a href=\"/selectAllUsers/9\">inscripcio</a></th>
                <th><a href=\"/selectAllUsers/10\">quota</a></th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuari"]) ? $context["usuari"] : $this->getContext($context, "usuari")));
        foreach ($context['_seq'] as $context["_key"] => $context["usu"]) {
            // line 98
            echo "                <tr>
                    <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "edat", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "dni", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "email", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "telefon", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 107
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usu"], "inscripcio", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "quota", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_7031e3411ea43e4894b81788d41a6cf5eabcb39106cd60d8ae4f969d4771fc4a->leave($__internal_7031e3411ea43e4894b81788d41a6cf5eabcb39106cd60d8ae4f969d4771fc4a_prof);

    }

    public function getTemplateName()
    {
        return "usuari/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 111,  199 => 108,  195 => 107,  191 => 106,  187 => 105,  183 => 104,  179 => 103,  175 => 102,  171 => 101,  167 => 100,  163 => 99,  160 => 98,  156 => 97,  133 => 76,  127 => 75,  92 => 45,  86 => 44,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* */
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Usuaris</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllUsers">Llistar tots els usuaris</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectUsuari">Seleccionar un usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/insertUsuari">Insertar nou usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/updateUsuari">Modificar un usuari</a>*/
/*                 </li>*/
/* */
/*                 <li>*/
/*                     <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th><a href="/selectAllUsers/1">id</a></th>*/
/*                 <th><a href="/selectAllUsers/2">nom</a></th>*/
/*                 <th><a href="/selectAllUsers/3">cognom1</a></th>*/
/*                 <th><a href="/selectAllUsers/4">cognom2</a></th>*/
/*                 <th><a href="/selectAllUsers/5">edat</a></th>*/
/*                 <th><a href="/selectAllUsers/6">dni</a></th>*/
/*                 <th><a href="/selectAllUsers/7">email</a></th>*/
/*                 <th><a href="/selectAllUsers/8">telefon</a></th>*/
/*                 <th><a href="/selectAllUsers/9">inscripcio</a></th>*/
/*                 <th><a href="/selectAllUsers/10">quota</a></th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for usu in usuari %}*/
/*                 <tr>*/
/*                     <td>{{ usu.id }}</td>*/
/*                     <td>{{ usu.nom }}</td>*/
/*                     <td>{{ usu.cognom1 }}</td>*/
/*                     <td>{{ usu.cognom2 }}</td>*/
/*                     <td>{{ usu.edat }}</td>*/
/*                     <td>{{ usu.dni }}</td>*/
/*                     <td>{{ usu.email }}</td>*/
/*                     <td>{{ usu.telefon }}</td>*/
/*                     <td>{{ usu.inscripcio|date("m/d/Y")}}</td>*/
/*                     <td>{{ usu.quota }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
