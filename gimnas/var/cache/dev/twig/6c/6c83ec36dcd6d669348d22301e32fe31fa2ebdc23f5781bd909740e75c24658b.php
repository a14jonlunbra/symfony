<?php

/* reserva/content.html.twig */
class __TwigTemplate_f6fcf6e0dd6df0a2ce2eba34cd30f36c872843d25af2c02e2dfc6c8c34057fb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "reserva/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d17e6a60b2350524fea2b0a9f01b8c856d972b72c48802615a0929b223887a4 = $this->env->getExtension("native_profiler");
        $__internal_7d17e6a60b2350524fea2b0a9f01b8c856d972b72c48802615a0929b223887a4->enter($__internal_7d17e6a60b2350524fea2b0a9f01b8c856d972b72c48802615a0929b223887a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reserva/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7d17e6a60b2350524fea2b0a9f01b8c856d972b72c48802615a0929b223887a4->leave($__internal_7d17e6a60b2350524fea2b0a9f01b8c856d972b72c48802615a0929b223887a4_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_8773a15ed8bfdee0ab59acfaf7dcbbc8aebec535f100582aff4961fd3261d3a3 = $this->env->getExtension("native_profiler");
        $__internal_8773a15ed8bfdee0ab59acfaf7dcbbc8aebec535f100582aff4961fd3261d3a3->enter($__internal_8773a15ed8bfdee0ab59acfaf7dcbbc8aebec535f100582aff4961fd3261d3a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>                              

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>pista_id</th>
                <th>usuari_id</th>
                <th>data</th>
                <th>hora</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reserva"]) ? $context["reserva"] : $this->getContext($context, "reserva")));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 22
            echo "                 <tr>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "pista", array()), "getNumeroPista", array(), "method"), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "usuari", array()), "getCognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "data", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "hora", array()), "html", null, true);
            echo "</td>
                 </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_8773a15ed8bfdee0ab59acfaf7dcbbc8aebec535f100582aff4961fd3261d3a3->leave($__internal_8773a15ed8bfdee0ab59acfaf7dcbbc8aebec535f100582aff4961fd3261d3a3_prof);

    }

    public function getTemplateName()
    {
        return "reserva/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 30,  82 => 27,  78 => 26,  74 => 25,  70 => 24,  66 => 23,  63 => 22,  59 => 21,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>                              */
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>pista_id</th>*/
/*                 <th>usuari_id</th>*/
/*                 <th>data</th>*/
/*                 <th>hora</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for res in reserva %}*/
/*                  <tr>*/
/*                     <td>{{ res.id }}</td>*/
/*                     <td>{{ res.pista.getNumeroPista() }}</td>*/
/*                     <td>{{ res.usuari.getCognom1 }}</td>*/
/*                     <td>{{ res.data }}</td>*/
/*                     <td>{{ res.hora }}</td>*/
/*                  </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
