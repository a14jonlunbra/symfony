<?php

/* monitor/content.html.twig */
class __TwigTemplate_c23938fe0af72271d7e839146e677a001399d1f59c57e97e398422912336f7f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "monitor/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70a7fe2055ea573346a6487455218bb38efad977e9518ae3cadb9e913f6a249d = $this->env->getExtension("native_profiler");
        $__internal_70a7fe2055ea573346a6487455218bb38efad977e9518ae3cadb9e913f6a249d->enter($__internal_70a7fe2055ea573346a6487455218bb38efad977e9518ae3cadb9e913f6a249d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "monitor/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_70a7fe2055ea573346a6487455218bb38efad977e9518ae3cadb9e913f6a249d->leave($__internal_70a7fe2055ea573346a6487455218bb38efad977e9518ae3cadb9e913f6a249d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3b676636f0f48d7ea0d32916a13a358ca764e883e22233657b5dcc30ff08aaa7 = $this->env->getExtension("native_profiler");
        $__internal_3b676636f0f48d7ea0d32916a13a358ca764e883e22233657b5dcc30ff08aaa7->enter($__internal_3b676636f0f48d7ea0d32916a13a358ca764e883e22233657b5dcc30ff08aaa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>                              

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>nom</th>
                <th>cognom1</th>
                <th>cognom2</th>
                <th>sou</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["monitor"]) ? $context["monitor"] : $this->getContext($context, "monitor")));
        foreach ($context['_seq'] as $context["_key"] => $context["moni"]) {
            // line 22
            echo "                <tr>
                    <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "sou", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['moni'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_3b676636f0f48d7ea0d32916a13a358ca764e883e22233657b5dcc30ff08aaa7->leave($__internal_3b676636f0f48d7ea0d32916a13a358ca764e883e22233657b5dcc30ff08aaa7_prof);

    }

    public function getTemplateName()
    {
        return "monitor/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 30,  82 => 27,  78 => 26,  74 => 25,  70 => 24,  66 => 23,  63 => 22,  59 => 21,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/monitor/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>                              */
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>nom</th>*/
/*                 <th>cognom1</th>*/
/*                 <th>cognom2</th>*/
/*                 <th>sou</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for moni in monitor %}*/
/*                 <tr>*/
/*                     <td>{{ moni.id }}</td>*/
/*                     <td>{{ moni.nom }}</td>*/
/*                     <td>{{ moni.cognom1 }}</td>*/
/*                     <td>{{ moni.cognom2 }}</td>*/
/*                     <td>{{ moni.sou }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
