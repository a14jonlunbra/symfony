<?php

/* principal/reserva.html.twig */
class __TwigTemplate_09e14c967b986dc82436c5972cffd1dff66e60a0ab50e46c69aa170f1722bb50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/reserva.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0bbf5c2615773b7e86524fb6e3e828179d22f6370c5fba004c4c1545fa463bec = $this->env->getExtension("native_profiler");
        $__internal_0bbf5c2615773b7e86524fb6e3e828179d22f6370c5fba004c4c1545fa463bec->enter($__internal_0bbf5c2615773b7e86524fb6e3e828179d22f6370c5fba004c4c1545fa463bec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/reserva.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0bbf5c2615773b7e86524fb6e3e828179d22f6370c5fba004c4c1545fa463bec->leave($__internal_0bbf5c2615773b7e86524fb6e3e828179d22f6370c5fba004c4c1545fa463bec_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c2b17d21ec05646864a4560ea90330fb6927826098805a914568071c04720542 = $this->env->getExtension("native_profiler");
        $__internal_c2b17d21ec05646864a4560ea90330fb6927826098805a914568071c04720542->enter($__internal_c2b17d21ec05646864a4560ea90330fb6927826098805a914568071c04720542_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Reserva</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 13
        echo "        </div>
";
        
        $__internal_c2b17d21ec05646864a4560ea90330fb6927826098805a914568071c04720542->leave($__internal_c2b17d21ec05646864a4560ea90330fb6927826098805a914568071c04720542_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_57aaaee82b3236234e676ac94ec9f5a6578196186620997ab77518f570152ef6 = $this->env->getExtension("native_profiler");
        $__internal_57aaaee82b3236234e676ac94ec9f5a6578196186620997ab77518f570152ef6->enter($__internal_57aaaee82b3236234e676ac94ec9f5a6578196186620997ab77518f570152ef6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllReserves\">Llistar totes les reserves</a><br>
                <a href=\"/insertReserva\">Insertar una nova reserva</a><br>
                <a href=\"/selectReserva\">Seleccionar una reserva</a><br>
                <a href=\"/removeReserva\">Eliminar una reserva</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_57aaaee82b3236234e676ac94ec9f5a6578196186620997ab77518f570152ef6->leave($__internal_57aaaee82b3236234e676ac94ec9f5a6578196186620997ab77518f570152ef6_prof);

    }

    public function getTemplateName()
    {
        return "principal/reserva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 13,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/reserva.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Reserva</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllReserves">Llistar totes les reserves</a><br>*/
/*                 <a href="/insertReserva">Insertar una nova reserva</a><br>*/
/*                 <a href="/selectReserva">Seleccionar una reserva</a><br>*/
/*                 <a href="/removeReserva">Eliminar una reserva</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
