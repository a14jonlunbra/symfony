<?php

/* base.html.twig */
class __TwigTemplate_52c8270e44f47dd582d67237d5e1f6acd34ac91e253827681173605296e98bfa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_539b734354fb2063d1f2beb02fc3c0fa9e1b055c8098b2e40d61e2fe6657e531 = $this->env->getExtension("native_profiler");
        $__internal_539b734354fb2063d1f2beb02fc3c0fa9e1b055c8098b2e40d61e2fe6657e531->enter($__internal_539b734354fb2063d1f2beb02fc3c0fa9e1b055c8098b2e40d61e2fe6657e531_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "        <style>
            .mainContent{
                margin-left: 20%;
            }
            .table-responsive{
                margin-right: 19%;
            }
          ";
        // line 24
        $this->displayBlock('style', $context, $blocks);
        // line 25
        echo "        </style>
    </head>
    
    <body>

         <nav class=\"navbar navbar-inverse navbar-static-top custom-navbar\" role=\"navigation\">
            <div class=\"container\">
             <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
             </button>
             <div class=\"navbar-header\">
                 <a class=\"navbar-brand\" rel=\"home\" href=\"/\" title=\"Help\"> Projecte Symfony</a>
             </div>
             <!-- Non-collapsing right-side icons -->
             <ul class=\"nav navbar-nav navbar-right\">
              <li>
                   <a href=\"#\" class=\"fa fa-cog\"></a>
              </li>
              <li>
                   <a href=\"#\" class=\"fa fa-home\"></a>
              </li>
             </ul>
             <!-- the collapsing menu -->
             <div class=\"collapse navbar-collapse navbar-left\" id=\"navbar-collapse-1\">
               <ul class=\"nav navbar-nav\">
                   <li><a href=\"/selectAllUsers\">Usuaris</a></li>
                   <li><a href=\"/selectAllMonitors\">Monitors</a></li>
                   <li><a href=\"/selectAllClases\">Clases</a></li>
                   <li><a href=\"/selectAllPistes\">Pistes</a></li>
                   <li><a href=\"/selectAllReserves\">Reserves</a></li>
               </ul>
             </div>
            <!--/.nav-collapse -->
            </div>
            <!--/.container -->
         </nav>

        <div id=\"content\">
            <nav>
                ";
        // line 67
        $this->displayBlock('menu_aside', $context, $blocks);
        // line 68
        echo "            </nav>
            <main>
                <div class=\"mainContent\">
                    ";
        // line 71
        $this->displayBlock('mainContent', $context, $blocks);
        // line 72
        echo "                </div>
            </main>

        </div>

    </body>

</html>";
        
        $__internal_539b734354fb2063d1f2beb02fc3c0fa9e1b055c8098b2e40d61e2fe6657e531->leave($__internal_539b734354fb2063d1f2beb02fc3c0fa9e1b055c8098b2e40d61e2fe6657e531_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_d50f8cff1e97f1108302e4cf5223664fd9cb9de9654b6ed482fe4e3944705d2b = $this->env->getExtension("native_profiler");
        $__internal_d50f8cff1e97f1108302e4cf5223664fd9cb9de9654b6ed482fe4e3944705d2b->enter($__internal_d50f8cff1e97f1108302e4cf5223664fd9cb9de9654b6ed482fe4e3944705d2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Product Application";
        
        $__internal_d50f8cff1e97f1108302e4cf5223664fd9cb9de9654b6ed482fe4e3944705d2b->leave($__internal_d50f8cff1e97f1108302e4cf5223664fd9cb9de9654b6ed482fe4e3944705d2b_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b3c5f2184907fa756e4f82d315c938e4aabebc0a4aec0c15796941b76e369519 = $this->env->getExtension("native_profiler");
        $__internal_b3c5f2184907fa756e4f82d315c938e4aabebc0a4aec0c15796941b76e369519->enter($__internal_b3c5f2184907fa756e4f82d315c938e4aabebc0a4aec0c15796941b76e369519_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "         <!-- Latest compiled and minified CSS -->
         <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">

         <!-- jQuery library -->
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>

         <!-- Latest compiled JavaScript -->
         <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        ";
        
        $__internal_b3c5f2184907fa756e4f82d315c938e4aabebc0a4aec0c15796941b76e369519->leave($__internal_b3c5f2184907fa756e4f82d315c938e4aabebc0a4aec0c15796941b76e369519_prof);

    }

    // line 24
    public function block_style($context, array $blocks = array())
    {
        $__internal_06c1f3dc3f5d8b34c1595324d3cb57c54a1a200b42687d21c8b5215ff68f54da = $this->env->getExtension("native_profiler");
        $__internal_06c1f3dc3f5d8b34c1595324d3cb57c54a1a200b42687d21c8b5215ff68f54da->enter($__internal_06c1f3dc3f5d8b34c1595324d3cb57c54a1a200b42687d21c8b5215ff68f54da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        
        $__internal_06c1f3dc3f5d8b34c1595324d3cb57c54a1a200b42687d21c8b5215ff68f54da->leave($__internal_06c1f3dc3f5d8b34c1595324d3cb57c54a1a200b42687d21c8b5215ff68f54da_prof);

    }

    // line 67
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_029884ad6cb5f7adf6b4bb314c0c3ae83da498505efc17fff71e060ec2eeb9d3 = $this->env->getExtension("native_profiler");
        $__internal_029884ad6cb5f7adf6b4bb314c0c3ae83da498505efc17fff71e060ec2eeb9d3->enter($__internal_029884ad6cb5f7adf6b4bb314c0c3ae83da498505efc17fff71e060ec2eeb9d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        echo " ";
        
        $__internal_029884ad6cb5f7adf6b4bb314c0c3ae83da498505efc17fff71e060ec2eeb9d3->leave($__internal_029884ad6cb5f7adf6b4bb314c0c3ae83da498505efc17fff71e060ec2eeb9d3_prof);

    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_db4eb1d2525166b308804030dfce1b90c6f9c493f603b575d5719c701186237e = $this->env->getExtension("native_profiler");
        $__internal_db4eb1d2525166b308804030dfce1b90c6f9c493f603b575d5719c701186237e->enter($__internal_db4eb1d2525166b308804030dfce1b90c6f9c493f603b575d5719c701186237e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        
        $__internal_db4eb1d2525166b308804030dfce1b90c6f9c493f603b575d5719c701186237e->leave($__internal_db4eb1d2525166b308804030dfce1b90c6f9c493f603b575d5719c701186237e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  173 => 71,  161 => 67,  150 => 24,  135 => 8,  129 => 7,  117 => 6,  103 => 72,  101 => 71,  96 => 68,  94 => 67,  50 => 25,  48 => 24,  39 => 17,  37 => 7,  33 => 6,  27 => 2,);
    }
}
/* {# app/Resources/views/base.html.twig #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{% block title %}Product Application{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*          <!-- Latest compiled and minified CSS -->*/
/*          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/* */
/*          <!-- jQuery library -->*/
/*          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/* */
/*          <!-- Latest compiled JavaScript -->*/
/*          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>*/
/*         {% endblock %}*/
/*         <style>*/
/*             .mainContent{*/
/*                 margin-left: 20%;*/
/*             }*/
/*             .table-responsive{*/
/*                 margin-right: 19%;*/
/*             }*/
/*           {% block style%}{% endblock %}*/
/*         </style>*/
/*     </head>*/
/*     */
/*     <body>*/
/* */
/*          <nav class="navbar navbar-inverse navbar-static-top custom-navbar" role="navigation">*/
/*             <div class="container">*/
/*              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*              </button>*/
/*              <div class="navbar-header">*/
/*                  <a class="navbar-brand" rel="home" href="/" title="Help"> Projecte Symfony</a>*/
/*              </div>*/
/*              <!-- Non-collapsing right-side icons -->*/
/*              <ul class="nav navbar-nav navbar-right">*/
/*               <li>*/
/*                    <a href="#" class="fa fa-cog"></a>*/
/*               </li>*/
/*               <li>*/
/*                    <a href="#" class="fa fa-home"></a>*/
/*               </li>*/
/*              </ul>*/
/*              <!-- the collapsing menu -->*/
/*              <div class="collapse navbar-collapse navbar-left" id="navbar-collapse-1">*/
/*                <ul class="nav navbar-nav">*/
/*                    <li><a href="/selectAllUsers">Usuaris</a></li>*/
/*                    <li><a href="/selectAllMonitors">Monitors</a></li>*/
/*                    <li><a href="/selectAllClases">Clases</a></li>*/
/*                    <li><a href="/selectAllPistes">Pistes</a></li>*/
/*                    <li><a href="/selectAllReserves">Reserves</a></li>*/
/*                </ul>*/
/*              </div>*/
/*             <!--/.nav-collapse -->*/
/*             </div>*/
/*             <!--/.container -->*/
/*          </nav>*/
/* */
/*         <div id="content">*/
/*             <nav>*/
/*                 {% block menu_aside %} {% endblock %}*/
/*             </nav>*/
/*             <main>*/
/*                 <div class="mainContent">*/
/*                     {% block mainContent %}{% endblock %}*/
/*                 </div>*/
/*             </main>*/
/* */
/*         </div>*/
/* */
/*     </body>*/
/* */
/* </html>*/
