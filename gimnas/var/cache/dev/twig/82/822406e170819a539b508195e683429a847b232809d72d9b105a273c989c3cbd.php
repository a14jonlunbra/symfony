<?php

/* principal/usuari.html.twig */
class __TwigTemplate_c72a1d8f80978a3db377ee083d6104d2607d5d31190e8f5f878c55ca2b4b74ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/usuari.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c60b26e2d06412d13f57f9ada9e81ee9c7d7131fedd17b6876d0054d284cddf7 = $this->env->getExtension("native_profiler");
        $__internal_c60b26e2d06412d13f57f9ada9e81ee9c7d7131fedd17b6876d0054d284cddf7->enter($__internal_c60b26e2d06412d13f57f9ada9e81ee9c7d7131fedd17b6876d0054d284cddf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/usuari.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c60b26e2d06412d13f57f9ada9e81ee9c7d7131fedd17b6876d0054d284cddf7->leave($__internal_c60b26e2d06412d13f57f9ada9e81ee9c7d7131fedd17b6876d0054d284cddf7_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_96436de56c618d87aa870db37d992b5b00f60387c10fb19e4241599b5e624e55 = $this->env->getExtension("native_profiler");
        $__internal_96436de56c618d87aa870db37d992b5b00f60387c10fb19e4241599b5e624e55->enter($__internal_96436de56c618d87aa870db37d992b5b00f60387c10fb19e4241599b5e624e55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Usuari</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>
";
        
        $__internal_96436de56c618d87aa870db37d992b5b00f60387c10fb19e4241599b5e624e55->leave($__internal_96436de56c618d87aa870db37d992b5b00f60387c10fb19e4241599b5e624e55_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_14997f6b8df8912f37446dbfa2062118e75efaa55f9435916a36c3f4857d0040 = $this->env->getExtension("native_profiler");
        $__internal_14997f6b8df8912f37446dbfa2062118e75efaa55f9435916a36c3f4857d0040->enter($__internal_14997f6b8df8912f37446dbfa2062118e75efaa55f9435916a36c3f4857d0040_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllUsers\">Llistar tots els usuaris</a><br>
                <a href=\"/selectAllDate\">Llistar tots els usuaris per data d'inscripcio</a><br>
                <a href=\"/insertUsuari\">Insertar nou usuari</a><br>
                <a href=\"/selectUsuari\">Seleccionar un usuari</a><br>
                <a href=\"/updateUsuari\">Modificar un usuari</a><br>
                <a href=\"/removeUsuari\">Eliminar un usuari</a>
                <br>
            ";
        
        $__internal_14997f6b8df8912f37446dbfa2062118e75efaa55f9435916a36c3f4857d0040->leave($__internal_14997f6b8df8912f37446dbfa2062118e75efaa55f9435916a36c3f4857d0040_prof);

    }

    public function getTemplateName()
    {
        return "principal/usuari.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/usuari.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Usuari</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllUsers">Llistar tots els usuaris</a><br>*/
/*                 <a href="/selectAllDate">Llistar tots els usuaris per data d'inscripcio</a><br>*/
/*                 <a href="/insertUsuari">Insertar nou usuari</a><br>*/
/*                 <a href="/selectUsuari">Seleccionar un usuari</a><br>*/
/*                 <a href="/updateUsuari">Modificar un usuari</a><br>*/
/*                 <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
