<?php

/* clase/form.html.twig */
class __TwigTemplate_556bb481dd5b66ba70b2396e29c6afd6d347c4df358a62009105420857da817e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "clase/form.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09cf058640d5c7c2681c598ac911cc584509382679e9c7ea76e062e5f0d87a3d = $this->env->getExtension("native_profiler");
        $__internal_09cf058640d5c7c2681c598ac911cc584509382679e9c7ea76e062e5f0d87a3d->enter($__internal_09cf058640d5c7c2681c598ac911cc584509382679e9c7ea76e062e5f0d87a3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clase/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09cf058640d5c7c2681c598ac911cc584509382679e9c7ea76e062e5f0d87a3d->leave($__internal_09cf058640d5c7c2681c598ac911cc584509382679e9c7ea76e062e5f0d87a3d_prof);

    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        $__internal_31841af840c19496192bdd17bbeab79a7e1d69c287ac188784dae95841263131 = $this->env->getExtension("native_profiler");
        $__internal_31841af840c19496192bdd17bbeab79a7e1d69c287ac188784dae95841263131->enter($__internal_31841af840c19496192bdd17bbeab79a7e1d69c287ac188784dae95841263131_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 4
        echo "    .form{
        margin-left:10%;
    }
    .form-control{
    margin-bottom: 1%;
     width:70%;
     }
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_31841af840c19496192bdd17bbeab79a7e1d69c287ac188784dae95841263131->leave($__internal_31841af840c19496192bdd17bbeab79a7e1d69c287ac188784dae95841263131_prof);

    }

    // line 48
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_290aceaa7e527c9f6e8ec82c0d4b58bea9f885157349734eda9c552d9133f85e = $this->env->getExtension("native_profiler");
        $__internal_290aceaa7e527c9f6e8ec82c0d4b58bea9f885157349734eda9c552d9133f85e->enter($__internal_290aceaa7e527c9f6e8ec82c0d4b58bea9f885157349734eda9c552d9133f85e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 49
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Clase</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllClases\">Llistar totes les classes</a>
                </li>
                <li>
                    <a href=\"/insertClase\">Insertar nova classe</a></li>
                <li>
                    <a href=\"/selectClase\">Seleccionar una classe</a>
                </li>
                <li><a href=\"/updateClase\">Modificar una classe</a></li>
                <li>
                    <a href=\"/removeClase\">Eliminar una classe</a>
                </li>

";
        
        $__internal_290aceaa7e527c9f6e8ec82c0d4b58bea9f885157349734eda9c552d9133f85e->leave($__internal_290aceaa7e527c9f6e8ec82c0d4b58bea9f885157349734eda9c552d9133f85e_prof);

    }

    // line 77
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_9e010073f38f5fdbde543a89e3002d9020997002237a96702380e6c70c491834 = $this->env->getExtension("native_profiler");
        $__internal_9e010073f38f5fdbde543a89e3002d9020997002237a96702380e6c70c491834->enter($__internal_9e010073f38f5fdbde543a89e3002d9020997002237a96702380e6c70c491834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 78
        echo "    <div class=\"form\">
        <h3> ";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
        ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        ";
        // line 82
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
     </div>
";
        
        $__internal_9e010073f38f5fdbde543a89e3002d9020997002237a96702380e6c70c491834->leave($__internal_9e010073f38f5fdbde543a89e3002d9020997002237a96702380e6c70c491834_prof);

    }

    public function getTemplateName()
    {
        return "clase/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 82,  148 => 81,  144 => 80,  140 => 79,  137 => 78,  131 => 77,  98 => 49,  92 => 48,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block style%}*/
/*     .form{*/
/*         margin-left:10%;*/
/*     }*/
/*     .form-control{*/
/*     margin-bottom: 1%;*/
/*      width:70%;*/
/*      }*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Clase</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllClases">Llistar totes les classes</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertClase">Insertar nova classe</a></li>*/
/*                 <li>*/
/*                     <a href="/selectClase">Seleccionar una classe</a>*/
/*                 </li>*/
/*                 <li><a href="/updateClase">Modificar una classe</a></li>*/
/*                 <li>*/
/*                     <a href="/removeClase">Eliminar una classe</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/*     <div class="form">*/
/*         <h3> {{title}} </h3>*/
/*         {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         {{ form_end(form) }}*/
/*      </div>*/
/* {% endblock %}*/
