<?php

/* principal/clase.html.twig */
class __TwigTemplate_f6b8923b43de134650765b038762a8785f71583b9c641375ade1bc1f6457e993 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/clase.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc468197b9536d3d6c9cf4b2f2fa86764104843ebbc9d777766b1d4cef8143d9 = $this->env->getExtension("native_profiler");
        $__internal_cc468197b9536d3d6c9cf4b2f2fa86764104843ebbc9d777766b1d4cef8143d9->enter($__internal_cc468197b9536d3d6c9cf4b2f2fa86764104843ebbc9d777766b1d4cef8143d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/clase.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cc468197b9536d3d6c9cf4b2f2fa86764104843ebbc9d777766b1d4cef8143d9->leave($__internal_cc468197b9536d3d6c9cf4b2f2fa86764104843ebbc9d777766b1d4cef8143d9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ce70fa365cdf06d63240c0c3b1754a14c1c72ecd48aea7e8262e2d11c95cd77d = $this->env->getExtension("native_profiler");
        $__internal_ce70fa365cdf06d63240c0c3b1754a14c1c72ecd48aea7e8262e2d11c95cd77d->enter($__internal_ce70fa365cdf06d63240c0c3b1754a14c1c72ecd48aea7e8262e2d11c95cd77d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Clase</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 14
        echo "        </div>
";
        
        $__internal_ce70fa365cdf06d63240c0c3b1754a14c1c72ecd48aea7e8262e2d11c95cd77d->leave($__internal_ce70fa365cdf06d63240c0c3b1754a14c1c72ecd48aea7e8262e2d11c95cd77d_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_974fd858d2bd6c0f4ed5f916fe666d6c80be0a113a046e4a8d82008e47187aa6 = $this->env->getExtension("native_profiler");
        $__internal_974fd858d2bd6c0f4ed5f916fe666d6c80be0a113a046e4a8d82008e47187aa6->enter($__internal_974fd858d2bd6c0f4ed5f916fe666d6c80be0a113a046e4a8d82008e47187aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllClases\">Llistar totes les clases</a><br>
                <a href=\"/insertClase\">Insertar una nova clase</a><br>
                <a href=\"/selectClase\">Seleccionar una clase</a><br>
                <a href=\"/updateClase\">Modificar una clase</a><br>
                <a href=\"/removeClase\">Eliminar una clase</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_974fd858d2bd6c0f4ed5f916fe666d6c80be0a113a046e4a8d82008e47187aa6->leave($__internal_974fd858d2bd6c0f4ed5f916fe666d6c80be0a113a046e4a8d82008e47187aa6_prof);

    }

    public function getTemplateName()
    {
        return "principal/clase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 14,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/clase.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Clase</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllClases">Llistar totes les clases</a><br>*/
/*                 <a href="/insertClase">Insertar una nova clase</a><br>*/
/*                 <a href="/selectClase">Seleccionar una clase</a><br>*/
/*                 <a href="/updateClase">Modificar una clase</a><br>*/
/*                 <a href="/removeClase">Eliminar una clase</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
