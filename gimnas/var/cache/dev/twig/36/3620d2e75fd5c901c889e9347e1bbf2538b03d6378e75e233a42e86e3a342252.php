<?php

/* principal/pista.html.twig */
class __TwigTemplate_045afebd56165278f41aecf21b956e42304596e3539710bb0dc82db0f002bb5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/pista.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bcae36d1e5ed19c94e4c02326631835408c135a4231ec6f127252943ce153249 = $this->env->getExtension("native_profiler");
        $__internal_bcae36d1e5ed19c94e4c02326631835408c135a4231ec6f127252943ce153249->enter($__internal_bcae36d1e5ed19c94e4c02326631835408c135a4231ec6f127252943ce153249_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/pista.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bcae36d1e5ed19c94e4c02326631835408c135a4231ec6f127252943ce153249->leave($__internal_bcae36d1e5ed19c94e4c02326631835408c135a4231ec6f127252943ce153249_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9f8092d7bf2ba4726a13607508cf048fe21df5b6d8ab201e827d0d9d4f92917d = $this->env->getExtension("native_profiler");
        $__internal_9f8092d7bf2ba4726a13607508cf048fe21df5b6d8ab201e827d0d9d4f92917d->enter($__internal_9f8092d7bf2ba4726a13607508cf048fe21df5b6d8ab201e827d0d9d4f92917d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Pista</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 12
        echo "        </div>
";
        
        $__internal_9f8092d7bf2ba4726a13607508cf048fe21df5b6d8ab201e827d0d9d4f92917d->leave($__internal_9f8092d7bf2ba4726a13607508cf048fe21df5b6d8ab201e827d0d9d4f92917d_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_b78cae6016366c586a7dd714d5f76607c302426020bdf365be0964912b8a749a = $this->env->getExtension("native_profiler");
        $__internal_b78cae6016366c586a7dd714d5f76607c302426020bdf365be0964912b8a749a->enter($__internal_b78cae6016366c586a7dd714d5f76607c302426020bdf365be0964912b8a749a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllPistes\">Llistar totes les pistes</a><br>
                <a href=\"/insertPista\">Insertar una nova pista</a><br>
                <a href=\"/selectPista\">Seleccionar una pista</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_b78cae6016366c586a7dd714d5f76607c302426020bdf365be0964912b8a749a->leave($__internal_b78cae6016366c586a7dd714d5f76607c302426020bdf365be0964912b8a749a_prof);

    }

    public function getTemplateName()
    {
        return "principal/pista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 12,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/pista.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Pista</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllPistes">Llistar totes les pistes</a><br>*/
/*                 <a href="/insertPista">Insertar una nova pista</a><br>*/
/*                 <a href="/selectPista">Seleccionar una pista</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
