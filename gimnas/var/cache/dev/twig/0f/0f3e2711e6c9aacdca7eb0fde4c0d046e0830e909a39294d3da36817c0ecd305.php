<?php

/* principal/pista.html.twig */
class __TwigTemplate_3947f38a0b19e2706e87dc50beb65e40af4007281f6f54b106e6160de12b2cbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/pista.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_262460c5e472bbc1d981bbf350fc89a3e5ca4f06390aebfab1115d34a320b639 = $this->env->getExtension("native_profiler");
        $__internal_262460c5e472bbc1d981bbf350fc89a3e5ca4f06390aebfab1115d34a320b639->enter($__internal_262460c5e472bbc1d981bbf350fc89a3e5ca4f06390aebfab1115d34a320b639_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/pista.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_262460c5e472bbc1d981bbf350fc89a3e5ca4f06390aebfab1115d34a320b639->leave($__internal_262460c5e472bbc1d981bbf350fc89a3e5ca4f06390aebfab1115d34a320b639_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_bd33358aad5d53b7e2d34c7d223503c84bfa00856f6b8e15a31ae1a9c5543fb5 = $this->env->getExtension("native_profiler");
        $__internal_bd33358aad5d53b7e2d34c7d223503c84bfa00856f6b8e15a31ae1a9c5543fb5->enter($__internal_bd33358aad5d53b7e2d34c7d223503c84bfa00856f6b8e15a31ae1a9c5543fb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Pista</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 12
        echo "        </div>
";
        
        $__internal_bd33358aad5d53b7e2d34c7d223503c84bfa00856f6b8e15a31ae1a9c5543fb5->leave($__internal_bd33358aad5d53b7e2d34c7d223503c84bfa00856f6b8e15a31ae1a9c5543fb5_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_2af2f4616892999a8bc04b5a5c9dbdf0e099ed9da4ce7f9fcff8862701781cda = $this->env->getExtension("native_profiler");
        $__internal_2af2f4616892999a8bc04b5a5c9dbdf0e099ed9da4ce7f9fcff8862701781cda->enter($__internal_2af2f4616892999a8bc04b5a5c9dbdf0e099ed9da4ce7f9fcff8862701781cda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllPistes\">Llistar totes les pistes</a><br>
                <a href=\"/insertPista\">Insertar una nova pista</a><br>
                <a href=\"/selectPista\">Seleccionar una pista</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_2af2f4616892999a8bc04b5a5c9dbdf0e099ed9da4ce7f9fcff8862701781cda->leave($__internal_2af2f4616892999a8bc04b5a5c9dbdf0e099ed9da4ce7f9fcff8862701781cda_prof);

    }

    public function getTemplateName()
    {
        return "principal/pista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 12,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/pista.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Pista</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllPistes">Llistar totes les pistes</a><br>*/
/*                 <a href="/insertPista">Insertar una nova pista</a><br>*/
/*                 <a href="/selectPista">Seleccionar una pista</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
