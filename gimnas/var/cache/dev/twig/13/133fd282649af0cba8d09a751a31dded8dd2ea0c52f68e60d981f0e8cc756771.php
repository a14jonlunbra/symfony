<?php

/* default/message.html.twig */
class __TwigTemplate_0c8ebd30d3e22a309cc2f2fd8b72c99b77dc7f2ac5fcf9c536a3f1f6012622e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/message.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ab7998aef9aa7f44137c056746efd44df34383ca8dbd97969851480a5da1b39 = $this->env->getExtension("native_profiler");
        $__internal_0ab7998aef9aa7f44137c056746efd44df34383ca8dbd97969851480a5da1b39->enter($__internal_0ab7998aef9aa7f44137c056746efd44df34383ca8dbd97969851480a5da1b39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ab7998aef9aa7f44137c056746efd44df34383ca8dbd97969851480a5da1b39->leave($__internal_0ab7998aef9aa7f44137c056746efd44df34383ca8dbd97969851480a5da1b39_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_57914d69b8a61cc8245414b26575b4b042e814ef095e8324ff3389626047c494 = $this->env->getExtension("native_profiler");
        $__internal_57914d69b8a61cc8245414b26575b4b042e814ef095e8324ff3389626047c494->enter($__internal_57914d69b8a61cc8245414b26575b4b042e814ef095e8324ff3389626047c494_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
";
        
        $__internal_57914d69b8a61cc8245414b26575b4b042e814ef095e8324ff3389626047c494->leave($__internal_57914d69b8a61cc8245414b26575b4b042e814ef095e8324ff3389626047c494_prof);

    }

    public function getTemplateName()
    {
        return "default/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/message.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     {{ message }}*/
/* {% endblock %}*/
