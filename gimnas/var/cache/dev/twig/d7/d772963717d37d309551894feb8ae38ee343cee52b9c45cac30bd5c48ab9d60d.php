<?php

/* monitor/form.html.twig */
class __TwigTemplate_8c504eac0f6af9e7d3a45be7be382e10558dd934d4e4b16986288878422b5f02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "monitor/form.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a5e1ecdd20712a1332727558fb3f8bbf0323b5b8f5b64ea3013f3abeedbcbfa = $this->env->getExtension("native_profiler");
        $__internal_1a5e1ecdd20712a1332727558fb3f8bbf0323b5b8f5b64ea3013f3abeedbcbfa->enter($__internal_1a5e1ecdd20712a1332727558fb3f8bbf0323b5b8f5b64ea3013f3abeedbcbfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "monitor/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a5e1ecdd20712a1332727558fb3f8bbf0323b5b8f5b64ea3013f3abeedbcbfa->leave($__internal_1a5e1ecdd20712a1332727558fb3f8bbf0323b5b8f5b64ea3013f3abeedbcbfa_prof);

    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        $__internal_3e75a950094b6cf974474d6bbccb158a2199650663f3dfd0418765e2e1a73ffb = $this->env->getExtension("native_profiler");
        $__internal_3e75a950094b6cf974474d6bbccb158a2199650663f3dfd0418765e2e1a73ffb->enter($__internal_3e75a950094b6cf974474d6bbccb158a2199650663f3dfd0418765e2e1a73ffb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 4
        echo "    .form{
        margin-left:10%;
    }
    .form-control{
    margin-bottom: 1%;
     width:70%;
     }
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_3e75a950094b6cf974474d6bbccb158a2199650663f3dfd0418765e2e1a73ffb->leave($__internal_3e75a950094b6cf974474d6bbccb158a2199650663f3dfd0418765e2e1a73ffb_prof);

    }

    // line 48
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_50536d931b9b731ba5ec17c6565fe7711ee582617f8b078d57029c353803ac15 = $this->env->getExtension("native_profiler");
        $__internal_50536d931b9b731ba5ec17c6565fe7711ee582617f8b078d57029c353803ac15->enter($__internal_50536d931b9b731ba5ec17c6565fe7711ee582617f8b078d57029c353803ac15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 49
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Monitor</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllMonitors\">Llistar tots els monitors</a>
                </li>
                <li>
                    <a href=\"/insertMonitor\">Insertar nou monitor</a></li>
                <li>
                    <a href=\"/selectMonitor\">Seleccionar un monitor</a>
                </li>
                <li><a href=\"/updateMonitor\">Modificar un monitor</a></li>
                <li>
                    <a href=\"/removeMonitor\">Eliminar un monitor</a>
                </li>

";
        
        $__internal_50536d931b9b731ba5ec17c6565fe7711ee582617f8b078d57029c353803ac15->leave($__internal_50536d931b9b731ba5ec17c6565fe7711ee582617f8b078d57029c353803ac15_prof);

    }

    // line 77
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_3368724233f0fe7a5ccf36b2085b7b24c2e64f9303bff9922d53117cdc4e7f27 = $this->env->getExtension("native_profiler");
        $__internal_3368724233f0fe7a5ccf36b2085b7b24c2e64f9303bff9922d53117cdc4e7f27->enter($__internal_3368724233f0fe7a5ccf36b2085b7b24c2e64f9303bff9922d53117cdc4e7f27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 78
        echo "    <div class=\"form\">
        <h3> ";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
        ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 81
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        ";
        // line 82
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
     </div>
";
        
        $__internal_3368724233f0fe7a5ccf36b2085b7b24c2e64f9303bff9922d53117cdc4e7f27->leave($__internal_3368724233f0fe7a5ccf36b2085b7b24c2e64f9303bff9922d53117cdc4e7f27_prof);

    }

    public function getTemplateName()
    {
        return "monitor/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 82,  148 => 81,  144 => 80,  140 => 79,  137 => 78,  131 => 77,  98 => 49,  92 => 48,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block style%}*/
/*     .form{*/
/*         margin-left:10%;*/
/*     }*/
/*     .form-control{*/
/*     margin-bottom: 1%;*/
/*      width:70%;*/
/*      }*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Monitor</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllMonitors">Llistar tots els monitors</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertMonitor">Insertar nou monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/selectMonitor">Seleccionar un monitor</a>*/
/*                 </li>*/
/*                 <li><a href="/updateMonitor">Modificar un monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/removeMonitor">Eliminar un monitor</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/*     <div class="form">*/
/*         <h3> {{title}} </h3>*/
/*         {{ form_start(form) }}*/
/*         {{ form_widget(form) }}*/
/*         {{ form_end(form) }}*/
/*      </div>*/
/* {% endblock %}*/
