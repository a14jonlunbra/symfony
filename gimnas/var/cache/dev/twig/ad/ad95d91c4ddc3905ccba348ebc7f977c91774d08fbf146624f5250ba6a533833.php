<?php

/* usuari/content.html.twig */
class __TwigTemplate_baf01cee1aa7d5dd8b1088cade01649b574626d47d3d63318d1fefd3815370cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "usuari/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_681c349c6fe7248eecc801f73103b1d0f252c8994940ba8d2aefd8da08cfcdf4 = $this->env->getExtension("native_profiler");
        $__internal_681c349c6fe7248eecc801f73103b1d0f252c8994940ba8d2aefd8da08cfcdf4->enter($__internal_681c349c6fe7248eecc801f73103b1d0f252c8994940ba8d2aefd8da08cfcdf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "usuari/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_681c349c6fe7248eecc801f73103b1d0f252c8994940ba8d2aefd8da08cfcdf4->leave($__internal_681c349c6fe7248eecc801f73103b1d0f252c8994940ba8d2aefd8da08cfcdf4_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_c9c8c57673daaadd07ddb08da11f1657cc1a102f7b80971f8f0ac435e83bfecb = $this->env->getExtension("native_profiler");
        $__internal_c9c8c57673daaadd07ddb08da11f1657cc1a102f7b80971f8f0ac435e83bfecb->enter($__internal_c9c8c57673daaadd07ddb08da11f1657cc1a102f7b80971f8f0ac435e83bfecb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_c9c8c57673daaadd07ddb08da11f1657cc1a102f7b80971f8f0ac435e83bfecb->leave($__internal_c9c8c57673daaadd07ddb08da11f1657cc1a102f7b80971f8f0ac435e83bfecb_prof);

    }

    // line 44
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_e220007894a91d7b5d3ae0d600b56072463b8940652bf88aca9e77ad2a22cdeb = $this->env->getExtension("native_profiler");
        $__internal_e220007894a91d7b5d3ae0d600b56072463b8940652bf88aca9e77ad2a22cdeb->enter($__internal_e220007894a91d7b5d3ae0d600b56072463b8940652bf88aca9e77ad2a22cdeb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 45
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Usuaris</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllUsers\">Llistar tots els usuaris</a>
                </li>
                <li>
                    <a href=\"/selectUsuari\">Seleccionar un usuari</a></li>
                <li>
                    <a href=\"/insertUsuari\">Insertar nou usuari</a></li>
                <li>
                    <a href=\"/updateUsuari\">Modificar un usuari</a>
                </li>

                <li>
                    <a href=\"/removeUsuari\">Eliminar un usuari</a>
                </li>

";
        
        $__internal_e220007894a91d7b5d3ae0d600b56072463b8940652bf88aca9e77ad2a22cdeb->leave($__internal_e220007894a91d7b5d3ae0d600b56072463b8940652bf88aca9e77ad2a22cdeb_prof);

    }

    // line 75
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_b9558e7eb2dbdc143083835332d910f0c416b3ab9d5bee024fb17352531a312e = $this->env->getExtension("native_profiler");
        $__internal_b9558e7eb2dbdc143083835332d910f0c416b3ab9d5bee024fb17352531a312e->enter($__internal_b9558e7eb2dbdc143083835332d910f0c416b3ab9d5bee024fb17352531a312e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 76
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th><a href=\"/selectAllUsers/1\">id</a></th>
                <th><a href=\"/selectAllUsers/2\">nom</a></th>
                <th><a href=\"/selectAllUsers/3\">cognom1</a></th>
                <th><a href=\"/selectAllUsers/4\">cognom2</a></th>
                <th><a href=\"/selectAllUsers/5\">edat</a></th>
                <th><a href=\"/selectAllUsers/6\">dni</a></th>
                <th><a href=\"/selectAllUsers/7\">email</a></th>
                <th><a href=\"/selectAllUsers/8\">telefon</a></th>
                <th><a href=\"/selectAllUsers/9\">inscripcio</a></th>
                <th><a href=\"/selectAllUsers/10\">quota</a></th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 97
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuari"]) ? $context["usuari"] : $this->getContext($context, "usuari")));
        foreach ($context['_seq'] as $context["_key"] => $context["usu"]) {
            // line 98
            echo "                <tr>
                    <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "edat", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "dni", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 105
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "email", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "telefon", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 107
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usu"], "inscripcio", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 108
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "quota", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_b9558e7eb2dbdc143083835332d910f0c416b3ab9d5bee024fb17352531a312e->leave($__internal_b9558e7eb2dbdc143083835332d910f0c416b3ab9d5bee024fb17352531a312e_prof);

    }

    public function getTemplateName()
    {
        return "usuari/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 111,  199 => 108,  195 => 107,  191 => 106,  187 => 105,  183 => 104,  179 => 103,  175 => 102,  171 => 101,  167 => 100,  163 => 99,  160 => 98,  156 => 97,  133 => 76,  127 => 75,  92 => 45,  86 => 44,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* */
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Usuaris</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllUsers">Llistar tots els usuaris</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectUsuari">Seleccionar un usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/insertUsuari">Insertar nou usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/updateUsuari">Modificar un usuari</a>*/
/*                 </li>*/
/* */
/*                 <li>*/
/*                     <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th><a href="/selectAllUsers/1">id</a></th>*/
/*                 <th><a href="/selectAllUsers/2">nom</a></th>*/
/*                 <th><a href="/selectAllUsers/3">cognom1</a></th>*/
/*                 <th><a href="/selectAllUsers/4">cognom2</a></th>*/
/*                 <th><a href="/selectAllUsers/5">edat</a></th>*/
/*                 <th><a href="/selectAllUsers/6">dni</a></th>*/
/*                 <th><a href="/selectAllUsers/7">email</a></th>*/
/*                 <th><a href="/selectAllUsers/8">telefon</a></th>*/
/*                 <th><a href="/selectAllUsers/9">inscripcio</a></th>*/
/*                 <th><a href="/selectAllUsers/10">quota</a></th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for usu in usuari %}*/
/*                 <tr>*/
/*                     <td>{{ usu.id }}</td>*/
/*                     <td>{{ usu.nom }}</td>*/
/*                     <td>{{ usu.cognom1 }}</td>*/
/*                     <td>{{ usu.cognom2 }}</td>*/
/*                     <td>{{ usu.edat }}</td>*/
/*                     <td>{{ usu.dni }}</td>*/
/*                     <td>{{ usu.email }}</td>*/
/*                     <td>{{ usu.telefon }}</td>*/
/*                     <td>{{ usu.inscripcio|date("m/d/Y")}}</td>*/
/*                     <td>{{ usu.quota }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
