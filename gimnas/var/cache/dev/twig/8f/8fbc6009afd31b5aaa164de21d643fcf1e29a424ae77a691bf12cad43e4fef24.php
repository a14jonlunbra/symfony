<?php

/* monitor/content.html.twig */
class __TwigTemplate_bf03781b59b075ccb45da298ff0fd12d088a971e0282638cdfa633c8251ca519 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "monitor/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9988e491d2c2ddc43ffe8a93c50c75ab8b7ca5d6c7026af12bf0752f31424d6e = $this->env->getExtension("native_profiler");
        $__internal_9988e491d2c2ddc43ffe8a93c50c75ab8b7ca5d6c7026af12bf0752f31424d6e->enter($__internal_9988e491d2c2ddc43ffe8a93c50c75ab8b7ca5d6c7026af12bf0752f31424d6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "monitor/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9988e491d2c2ddc43ffe8a93c50c75ab8b7ca5d6c7026af12bf0752f31424d6e->leave($__internal_9988e491d2c2ddc43ffe8a93c50c75ab8b7ca5d6c7026af12bf0752f31424d6e_prof);

    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        $__internal_72b5a829158699ac6cce3cb79e3cf28274c0f096dfb2a4e7d51112860d1e5bf1 = $this->env->getExtension("native_profiler");
        $__internal_72b5a829158699ac6cce3cb79e3cf28274c0f096dfb2a4e7d51112860d1e5bf1->enter($__internal_72b5a829158699ac6cce3cb79e3cf28274c0f096dfb2a4e7d51112860d1e5bf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 4
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_72b5a829158699ac6cce3cb79e3cf28274c0f096dfb2a4e7d51112860d1e5bf1->leave($__internal_72b5a829158699ac6cce3cb79e3cf28274c0f096dfb2a4e7d51112860d1e5bf1_prof);

    }

    // line 42
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_e6d18c9e2238a9ea982072e5fa5244722b402620b1e8a152c541d8d33760bdd1 = $this->env->getExtension("native_profiler");
        $__internal_e6d18c9e2238a9ea982072e5fa5244722b402620b1e8a152c541d8d33760bdd1->enter($__internal_e6d18c9e2238a9ea982072e5fa5244722b402620b1e8a152c541d8d33760bdd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 43
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Monitors</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllMonitors\">Llistar tots els monitors</a>
                </li>
                <li>
                    <a href=\"/selectMonitor\">Seleccionar un monitor</a>
                </li>
                <li>
                    <a href=\"/insertMonitor\">Insertar nou monitor</a></li>

                <li><a href=\"/updateMonitor\">Modificar un monitor</a></li>
                <li>
                    <a href=\"/removeMonitor\">Eliminar un monitor</a>
                </li>

";
        
        $__internal_e6d18c9e2238a9ea982072e5fa5244722b402620b1e8a152c541d8d33760bdd1->leave($__internal_e6d18c9e2238a9ea982072e5fa5244722b402620b1e8a152c541d8d33760bdd1_prof);

    }

    // line 72
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_0313e4258db886afbefb7face98e5cc60a006fdc29385f9b7071fee8f0b92f65 = $this->env->getExtension("native_profiler");
        $__internal_0313e4258db886afbefb7face98e5cc60a006fdc29385f9b7071fee8f0b92f65->enter($__internal_0313e4258db886afbefb7face98e5cc60a006fdc29385f9b7071fee8f0b92f65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 73
        echo "
  <h2>Llista d'usuaris inscrits</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
               <th><a href=\"/selectAllMonitors/1\">id</a></th>
                <th><a href=\"/selectAllMonitors/2\">nom</a></th>
                <th><a href=\"/selectAllMonitors/3\">cognom1</a></th>
                <th><a href=\"/selectAllMonitors/4\">cognom2</a></th>
                <th><a href=\"/selectAllMonitors/5\">sou</a></th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["monitor"]) ? $context["monitor"] : $this->getContext($context, "monitor")));
        foreach ($context['_seq'] as $context["_key"] => $context["moni"]) {
            // line 88
            echo "                <tr>
                    <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "sou", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['moni'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "            </tbody>
        </table>
</div>

";
        
        $__internal_0313e4258db886afbefb7face98e5cc60a006fdc29385f9b7071fee8f0b92f65->leave($__internal_0313e4258db886afbefb7face98e5cc60a006fdc29385f9b7071fee8f0b92f65_prof);

    }

    public function getTemplateName()
    {
        return "monitor/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 96,  171 => 93,  167 => 92,  163 => 91,  159 => 90,  155 => 89,  152 => 88,  148 => 87,  132 => 73,  126 => 72,  92 => 43,  86 => 42,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/monitor/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Monitors</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllMonitors">Llistar tots els monitors</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectMonitor">Seleccionar un monitor</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertMonitor">Insertar nou monitor</a></li>*/
/* */
/*                 <li><a href="/updateMonitor">Modificar un monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/removeMonitor">Eliminar un monitor</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                <th><a href="/selectAllMonitors/1">id</a></th>*/
/*                 <th><a href="/selectAllMonitors/2">nom</a></th>*/
/*                 <th><a href="/selectAllMonitors/3">cognom1</a></th>*/
/*                 <th><a href="/selectAllMonitors/4">cognom2</a></th>*/
/*                 <th><a href="/selectAllMonitors/5">sou</a></th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for moni in monitor %}*/
/*                 <tr>*/
/*                     <td>{{ moni.id }}</td>*/
/*                     <td>{{ moni.nom }}</td>*/
/*                     <td>{{ moni.cognom1 }}</td>*/
/*                     <td>{{ moni.cognom2 }}</td>*/
/*                     <td>{{ moni.sou }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/* </div>*/
/* */
/* {% endblock %}*/
