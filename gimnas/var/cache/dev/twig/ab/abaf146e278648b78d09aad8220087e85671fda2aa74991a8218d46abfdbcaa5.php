<?php

/* principal/pista.html.twig */
class __TwigTemplate_d1ead2a1ebbaf6ac2c82e49f02fad0854ce451ee45e96d98830332f41bed9b58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/pista.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5107f3a51c73f5ba4a5f43f2a73aef8a00e3ffd842abb09af0a0e7fe1efb39e9 = $this->env->getExtension("native_profiler");
        $__internal_5107f3a51c73f5ba4a5f43f2a73aef8a00e3ffd842abb09af0a0e7fe1efb39e9->enter($__internal_5107f3a51c73f5ba4a5f43f2a73aef8a00e3ffd842abb09af0a0e7fe1efb39e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/pista.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5107f3a51c73f5ba4a5f43f2a73aef8a00e3ffd842abb09af0a0e7fe1efb39e9->leave($__internal_5107f3a51c73f5ba4a5f43f2a73aef8a00e3ffd842abb09af0a0e7fe1efb39e9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_946d3a41dcee798bf2300091e1ade437edae5e80c562317ce6fe2ca19d295840 = $this->env->getExtension("native_profiler");
        $__internal_946d3a41dcee798bf2300091e1ade437edae5e80c562317ce6fe2ca19d295840->enter($__internal_946d3a41dcee798bf2300091e1ade437edae5e80c562317ce6fe2ca19d295840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Pista</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 12
        echo "        </div>
";
        
        $__internal_946d3a41dcee798bf2300091e1ade437edae5e80c562317ce6fe2ca19d295840->leave($__internal_946d3a41dcee798bf2300091e1ade437edae5e80c562317ce6fe2ca19d295840_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_e96ab6c8b1ebefaa700efee858737069558ce1c83bde524972d75bfe6cb8d84a = $this->env->getExtension("native_profiler");
        $__internal_e96ab6c8b1ebefaa700efee858737069558ce1c83bde524972d75bfe6cb8d84a->enter($__internal_e96ab6c8b1ebefaa700efee858737069558ce1c83bde524972d75bfe6cb8d84a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllPistes\">Llistar totes les pistes</a><br>
                <a href=\"/insertPista\">Insertar una nova pista</a><br>
                <a href=\"/selectPista\">Seleccionar una pista</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_e96ab6c8b1ebefaa700efee858737069558ce1c83bde524972d75bfe6cb8d84a->leave($__internal_e96ab6c8b1ebefaa700efee858737069558ce1c83bde524972d75bfe6cb8d84a_prof);

    }

    public function getTemplateName()
    {
        return "principal/pista.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 12,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/pista.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Pista</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllPistes">Llistar totes les pistes</a><br>*/
/*                 <a href="/insertPista">Insertar una nova pista</a><br>*/
/*                 <a href="/selectPista">Seleccionar una pista</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
