<?php

/* reserva/form.html.twig */
class __TwigTemplate_41d99cc7977f22fc0989d6b62f9ee4dbefc9555b12e761d14f6c868cd24bbd74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "reserva/form.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_12f21e70b8743db2fe6bb658563226cf643e8a784541cc6f4e3bdb27e8c26aa6 = $this->env->getExtension("native_profiler");
        $__internal_12f21e70b8743db2fe6bb658563226cf643e8a784541cc6f4e3bdb27e8c26aa6->enter($__internal_12f21e70b8743db2fe6bb658563226cf643e8a784541cc6f4e3bdb27e8c26aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reserva/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_12f21e70b8743db2fe6bb658563226cf643e8a784541cc6f4e3bdb27e8c26aa6->leave($__internal_12f21e70b8743db2fe6bb658563226cf643e8a784541cc6f4e3bdb27e8c26aa6_prof);

    }

    // line 5
    public function block_style($context, array $blocks = array())
    {
        $__internal_75b015d01655e4fa02f7eba33f72c08218ff909245cf259e433a940d0abed76e = $this->env->getExtension("native_profiler");
        $__internal_75b015d01655e4fa02f7eba33f72c08218ff909245cf259e433a940d0abed76e->enter($__internal_75b015d01655e4fa02f7eba33f72c08218ff909245cf259e433a940d0abed76e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 6
        echo "\t.col-sm-4{
\t\t\t\tmargin-right: 1%;
\t\t\t}
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_75b015d01655e4fa02f7eba33f72c08218ff909245cf259e433a940d0abed76e->leave($__internal_75b015d01655e4fa02f7eba33f72c08218ff909245cf259e433a940d0abed76e_prof);

    }

    // line 46
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_57988f6c4bb9a47f756854bc690f21c3d0de20f26675fab3e01692f689b64edf = $this->env->getExtension("native_profiler");
        $__internal_57988f6c4bb9a47f756854bc690f21c3d0de20f26675fab3e01692f689b64edf->enter($__internal_57988f6c4bb9a47f756854bc690f21c3d0de20f26675fab3e01692f689b64edf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 47
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Monitor</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllMonitors\">Llistar tots els monitors</a>
                </li>
                <li>
                    <a href=\"/insertMonitor\">Insertar nou monitor</a></li>
                <li>
                    <a href=\"/selectMonitor\">Seleccionar un monitor</a>
                </li>
                <li><a href=\"/updateMonitor\">Modificar un monitor</a></li>
                <li>
                    <a href=\"/removeMonitor\">Eliminar un monitor</a>
                </li>

";
        
        $__internal_57988f6c4bb9a47f756854bc690f21c3d0de20f26675fab3e01692f689b64edf->leave($__internal_57988f6c4bb9a47f756854bc690f21c3d0de20f26675fab3e01692f689b64edf_prof);

    }

    // line 75
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_737dcfec3d4acb7337317e49287a294349d38b95787d30e4291f65da3280e039 = $this->env->getExtension("native_profiler");
        $__internal_737dcfec3d4acb7337317e49287a294349d38b95787d30e4291f65da3280e039->enter($__internal_737dcfec3d4acb7337317e49287a294349d38b95787d30e4291f65da3280e039_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 76
        echo "    <h3> ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 77
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

\t
\t<div class=\"container\">
\t  <h2>Estat de les pistes</h2>
\t  <button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#demo\">Veure estat</button>
\t  <div id=\"demo\" class=\"collapse\">
\t    
\t\t  <div class=\"container\">
\t\t  \t<div class=\"row\">
\t\t  \t \t";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pistes"]) ? $context["pistes"] : $this->getContext($context, "pistes")));
        foreach ($context['_seq'] as $context["_key"] => $context["pista"]) {
            // line 90
            echo "\t\t    \t\t<div class=\"col-sm-4\" style=\"background-color:lavender;\">
\t\t    \t\t\t<h5>Numero de Pista: ";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["pista"], "getId", array(), "method"), "html", null, true);
            echo "</h5>
\t\t    \t\t\t";
            // line 92
            if (($this->getAttribute($context["pista"], "estat", array()) == 0)) {
                // line 93
                echo "\t\t    \t\t\t\t<p>Estat: Lliure</p>
\t\t    \t\t\t";
            } else {
                // line 95
                echo "              \t\t\t\t<p>Estat: Ocupat</p>
        \t\t\t\t";
            }
            // line 97
            echo "\t\t    \t\t</div>
\t    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pista'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "\t  \t\t</div>
\t  \t  </div>

\t  </div>
\t</div>

";
        
        $__internal_737dcfec3d4acb7337317e49287a294349d38b95787d30e4291f65da3280e039->leave($__internal_737dcfec3d4acb7337317e49287a294349d38b95787d30e4291f65da3280e039_prof);

    }

    public function getTemplateName()
    {
        return "reserva/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 99,  180 => 97,  176 => 95,  172 => 93,  170 => 92,  166 => 91,  163 => 90,  159 => 89,  146 => 79,  142 => 78,  138 => 77,  133 => 76,  127 => 75,  94 => 47,  88 => 46,  42 => 6,  36 => 5,  11 => 2,);
    }
}
/* {# app/Resources/views/reserva/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block style%}*/
/* 	.col-sm-4{*/
/* 				margin-right: 1%;*/
/* 			}*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Monitor</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllMonitors">Llistar tots els monitors</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertMonitor">Insertar nou monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/selectMonitor">Seleccionar un monitor</a>*/
/*                 </li>*/
/*                 <li><a href="/updateMonitor">Modificar un monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/removeMonitor">Eliminar un monitor</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/*     <h3> {{title}} </h3>*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* */
/* 	*/
/* 	<div class="container">*/
/* 	  <h2>Estat de les pistes</h2>*/
/* 	  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Veure estat</button>*/
/* 	  <div id="demo" class="collapse">*/
/* 	    */
/* 		  <div class="container">*/
/* 		  	<div class="row">*/
/* 		  	 	{% for pista in pistes %}*/
/* 		    		<div class="col-sm-4" style="background-color:lavender;">*/
/* 		    			<h5>Numero de Pista: {{pista.getId()}}</h5>*/
/* 		    			{% if pista.estat==0 %}*/
/* 		    				<p>Estat: Lliure</p>*/
/* 		    			{% else %}*/
/*               				<p>Estat: Ocupat</p>*/
/*         				{% endif %}*/
/* 		    		</div>*/
/* 	    		{% endfor %}*/
/* 	  		</div>*/
/* 	  	  </div>*/
/* */
/* 	  </div>*/
/* 	</div>*/
/* */
/* {% endblock %}*/
