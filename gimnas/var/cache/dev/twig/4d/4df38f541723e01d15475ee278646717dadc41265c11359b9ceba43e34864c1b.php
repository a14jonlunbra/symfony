<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_90f011f1ab5e3f37e78047aa18035a877dea2112b62670c06a06a3fd998835bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9ffcaed9fd6b5a632dc71b72485a8b59c513aacd9ab86a1e199e0a8a67bf93cd = $this->env->getExtension("native_profiler");
        $__internal_9ffcaed9fd6b5a632dc71b72485a8b59c513aacd9ab86a1e199e0a8a67bf93cd->enter($__internal_9ffcaed9fd6b5a632dc71b72485a8b59c513aacd9ab86a1e199e0a8a67bf93cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9ffcaed9fd6b5a632dc71b72485a8b59c513aacd9ab86a1e199e0a8a67bf93cd->leave($__internal_9ffcaed9fd6b5a632dc71b72485a8b59c513aacd9ab86a1e199e0a8a67bf93cd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_8a46108e20044ffda5846f9342bf06fc3256ffbbb075d3a8c42dcda29994b08e = $this->env->getExtension("native_profiler");
        $__internal_8a46108e20044ffda5846f9342bf06fc3256ffbbb075d3a8c42dcda29994b08e->enter($__internal_8a46108e20044ffda5846f9342bf06fc3256ffbbb075d3a8c42dcda29994b08e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_8a46108e20044ffda5846f9342bf06fc3256ffbbb075d3a8c42dcda29994b08e->leave($__internal_8a46108e20044ffda5846f9342bf06fc3256ffbbb075d3a8c42dcda29994b08e_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_8a3df95d53be90d8bc1968ca2ee78062662c711ff95ac403ab06d6e655f24373 = $this->env->getExtension("native_profiler");
        $__internal_8a3df95d53be90d8bc1968ca2ee78062662c711ff95ac403ab06d6e655f24373->enter($__internal_8a3df95d53be90d8bc1968ca2ee78062662c711ff95ac403ab06d6e655f24373_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_8a3df95d53be90d8bc1968ca2ee78062662c711ff95ac403ab06d6e655f24373->leave($__internal_8a3df95d53be90d8bc1968ca2ee78062662c711ff95ac403ab06d6e655f24373_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_2402ea8261d8d9f8688269074ff3f62be64afd4297df46a1271296c10e281229 = $this->env->getExtension("native_profiler");
        $__internal_2402ea8261d8d9f8688269074ff3f62be64afd4297df46a1271296c10e281229->enter($__internal_2402ea8261d8d9f8688269074ff3f62be64afd4297df46a1271296c10e281229_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_2402ea8261d8d9f8688269074ff3f62be64afd4297df46a1271296c10e281229->leave($__internal_2402ea8261d8d9f8688269074ff3f62be64afd4297df46a1271296c10e281229_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
