<?php

/* principal/monitor.html.twig */
class __TwigTemplate_d6222dd7926e54e0fa929548ad44eaeb47d8488e8ba8f3a79de6d1ee1fc4ba53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/monitor.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'body' => array($this, 'block_body'),
            'bodychild' => array($this, 'block_bodychild'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5333c5e129c637848f710d5378a924251d0ae5719166a682b0c7f6ae04f17222 = $this->env->getExtension("native_profiler");
        $__internal_5333c5e129c637848f710d5378a924251d0ae5719166a682b0c7f6ae04f17222->enter($__internal_5333c5e129c637848f710d5378a924251d0ae5719166a682b0c7f6ae04f17222_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/monitor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5333c5e129c637848f710d5378a924251d0ae5719166a682b0c7f6ae04f17222->leave($__internal_5333c5e129c637848f710d5378a924251d0ae5719166a682b0c7f6ae04f17222_prof);

    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        $__internal_ae467ed8608cc0de5b4d006b032230b03dd82dd513d4a497bbedb5fb2bc1a064 = $this->env->getExtension("native_profiler");
        $__internal_ae467ed8608cc0de5b4d006b032230b03dd82dd513d4a497bbedb5fb2bc1a064->enter($__internal_ae467ed8608cc0de5b4d006b032230b03dd82dd513d4a497bbedb5fb2bc1a064_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 4
        echo "
    .table-responsive{
    }
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
    .content {
    padding: 15px;
    margin-top: 100px;
    }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
    /* clean up some of the default Bootstrap styles for panels in the menu */

    #menu-bar .panel {
    margin-bottom: 0;    margin-bottom: 0;

    border: none;
    border-radius: 0;
    -webkit-box-shadow: none;
    -box-shadow: none;
    }

    #navbar {
    float: left;
    width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
    height: 100%;
    }

    .content {
    margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
    min-height: 100%;
    }

    .container {
    margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
    }
    }
";
        
        $__internal_ae467ed8608cc0de5b4d006b032230b03dd82dd513d4a497bbedb5fb2bc1a064->leave($__internal_ae467ed8608cc0de5b4d006b032230b03dd82dd513d4a497bbedb5fb2bc1a064_prof);

    }

    // line 45
    public function block_body($context, array $blocks = array())
    {
        $__internal_8fa1e97957910b80d53054cec0504675313a5cc39d7fb282af8ed23fe83d0218 = $this->env->getExtension("native_profiler");
        $__internal_8fa1e97957910b80d53054cec0504675313a5cc39d7fb282af8ed23fe83d0218->enter($__internal_8fa1e97957910b80d53054cec0504675313a5cc39d7fb282af8ed23fe83d0218_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 46
        echo "    <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Monitor</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllMonitors\">Llistar tots els monitors</a>
                </li>
                <li>
                    <a href=\"/insertMonitor\">Insertar nou monitor</a></li>
                <li>
                    <a href=\"/selectMonitor\">Seleccionar un monitor</a>
                </li>
                <li><a href=\"/updateMonitor\">Modificar un monitor</a></li>
                <li>
                    <a href=\"/removeMonitor\">Eliminar un monitor</a>
                </li>
    <div class=\"clearfix\"></div>
    <div class=\"content\">
            <div class=\"row\">
                <div class=\"col-lg-8\">
                    ";
        // line 75
        $this->displayBlock('bodychild', $context, $blocks);
        
        $__internal_8fa1e97957910b80d53054cec0504675313a5cc39d7fb282af8ed23fe83d0218->leave($__internal_8fa1e97957910b80d53054cec0504675313a5cc39d7fb282af8ed23fe83d0218_prof);

    }

    public function block_bodychild($context, array $blocks = array())
    {
        $__internal_4a3b82f8060b1533d700fccb6a94a6de65f567521b5bf6626ff63b98b406efbf = $this->env->getExtension("native_profiler");
        $__internal_4a3b82f8060b1533d700fccb6a94a6de65f567521b5bf6626ff63b98b406efbf->enter($__internal_4a3b82f8060b1533d700fccb6a94a6de65f567521b5bf6626ff63b98b406efbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bodychild"));

        // line 76
        echo "                </div>
            </div>
        </div>
    </div>

    ";
        
        $__internal_4a3b82f8060b1533d700fccb6a94a6de65f567521b5bf6626ff63b98b406efbf->leave($__internal_4a3b82f8060b1533d700fccb6a94a6de65f567521b5bf6626ff63b98b406efbf_prof);

    }

    public function getTemplateName()
    {
        return "principal/monitor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 76,  125 => 75,  94 => 46,  88 => 45,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/monitor.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block style%}*/
/* */
/*     .table-responsive{*/
/*     }*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*     .content {*/
/*     padding: 15px;*/
/*     margin-top: 100px;*/
/*     }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*     /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*     #menu-bar .panel {*/
/*     margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*     border: none;*/
/*     border-radius: 0;*/
/*     -webkit-box-shadow: none;*/
/*     -box-shadow: none;*/
/*     }*/
/* */
/*     #navbar {*/
/*     float: left;*/
/*     width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*     height: 100%;*/
/*     }*/
/* */
/*     .content {*/
/*     margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*     min-height: 100%;*/
/*     }*/
/* */
/*     .container {*/
/*     margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*     }*/
/*     }*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Monitor</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllMonitors">Llistar tots els monitors</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertMonitor">Insertar nou monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/selectMonitor">Seleccionar un monitor</a>*/
/*                 </li>*/
/*                 <li><a href="/updateMonitor">Modificar un monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/removeMonitor">Eliminar un monitor</a>*/
/*                 </li>*/
/*     <div class="clearfix"></div>*/
/*     <div class="content">*/
/*             <div class="row">*/
/*                 <div class="col-lg-8">*/
/*                     {% block bodychild %}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     {% endblock %}*/
/* {% endblock %}*/
