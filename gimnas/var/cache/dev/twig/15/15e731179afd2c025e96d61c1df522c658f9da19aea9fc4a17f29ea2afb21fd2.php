<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_76919e3412e852d8d15960ca9b1f308dfa0cb845eea79914c759d0a44834fddb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9213ee2cf6a447611bb045d76aed7d752dd9737f72196a383ee0477e0cd4171 = $this->env->getExtension("native_profiler");
        $__internal_e9213ee2cf6a447611bb045d76aed7d752dd9737f72196a383ee0477e0cd4171->enter($__internal_e9213ee2cf6a447611bb045d76aed7d752dd9737f72196a383ee0477e0cd4171_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e9213ee2cf6a447611bb045d76aed7d752dd9737f72196a383ee0477e0cd4171->leave($__internal_e9213ee2cf6a447611bb045d76aed7d752dd9737f72196a383ee0477e0cd4171_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_776f4f09c9effce5a47d420bcce5fe2d9d7088eac905b5339ec46a9babaeca80 = $this->env->getExtension("native_profiler");
        $__internal_776f4f09c9effce5a47d420bcce5fe2d9d7088eac905b5339ec46a9babaeca80->enter($__internal_776f4f09c9effce5a47d420bcce5fe2d9d7088eac905b5339ec46a9babaeca80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_776f4f09c9effce5a47d420bcce5fe2d9d7088eac905b5339ec46a9babaeca80->leave($__internal_776f4f09c9effce5a47d420bcce5fe2d9d7088eac905b5339ec46a9babaeca80_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_29165f30b2e063c21c1180e305c19c6b1d5e1cf8cc5731be48decb7367410968 = $this->env->getExtension("native_profiler");
        $__internal_29165f30b2e063c21c1180e305c19c6b1d5e1cf8cc5731be48decb7367410968->enter($__internal_29165f30b2e063c21c1180e305c19c6b1d5e1cf8cc5731be48decb7367410968_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_29165f30b2e063c21c1180e305c19c6b1d5e1cf8cc5731be48decb7367410968->leave($__internal_29165f30b2e063c21c1180e305c19c6b1d5e1cf8cc5731be48decb7367410968_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_b72de63dbced84a779391e71c2bc11dbd7235a05057ee28f64efcca01163d2e0 = $this->env->getExtension("native_profiler");
        $__internal_b72de63dbced84a779391e71c2bc11dbd7235a05057ee28f64efcca01163d2e0->enter($__internal_b72de63dbced84a779391e71c2bc11dbd7235a05057ee28f64efcca01163d2e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_b72de63dbced84a779391e71c2bc11dbd7235a05057ee28f64efcca01163d2e0->leave($__internal_b72de63dbced84a779391e71c2bc11dbd7235a05057ee28f64efcca01163d2e0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
