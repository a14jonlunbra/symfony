<?php

/* reserva/form.html.twig */
class __TwigTemplate_f88c8656096d6e48ad224bd8641e54ceccbfbdd3d11fc255c0d84cdc25ec1cad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "reserva/form.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_496b83a3c915618be4fff1a3a8278ac4fa99811d36915d3956477441d0198342 = $this->env->getExtension("native_profiler");
        $__internal_496b83a3c915618be4fff1a3a8278ac4fa99811d36915d3956477441d0198342->enter($__internal_496b83a3c915618be4fff1a3a8278ac4fa99811d36915d3956477441d0198342_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reserva/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_496b83a3c915618be4fff1a3a8278ac4fa99811d36915d3956477441d0198342->leave($__internal_496b83a3c915618be4fff1a3a8278ac4fa99811d36915d3956477441d0198342_prof);

    }

    // line 5
    public function block_style($context, array $blocks = array())
    {
        $__internal_7afd7350d4c0b5769dae7a4f5b3ac8031d1897dedce67f01f803d1628331c4bd = $this->env->getExtension("native_profiler");
        $__internal_7afd7350d4c0b5769dae7a4f5b3ac8031d1897dedce67f01f803d1628331c4bd->enter($__internal_7afd7350d4c0b5769dae7a4f5b3ac8031d1897dedce67f01f803d1628331c4bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 6
        echo "\t.col-sm-4{
\t\t\t\tmargin-right: 1%;
\t\t\t}
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_7afd7350d4c0b5769dae7a4f5b3ac8031d1897dedce67f01f803d1628331c4bd->leave($__internal_7afd7350d4c0b5769dae7a4f5b3ac8031d1897dedce67f01f803d1628331c4bd_prof);

    }

    // line 46
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_99798dc08c6d21dd5ae50fe6de19db7d3ddafe439b086292cbd20661174af2b0 = $this->env->getExtension("native_profiler");
        $__internal_99798dc08c6d21dd5ae50fe6de19db7d3ddafe439b086292cbd20661174af2b0->enter($__internal_99798dc08c6d21dd5ae50fe6de19db7d3ddafe439b086292cbd20661174af2b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 47
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Reserva</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllReserves\">Llistar totes les reserves</a>
                </li>
                <li>
                    <a href=\"/insertReserva\">Insertar nova reserva</a></li>
                <li>
                    <a href=\"/selectReserva\">Seleccionar una reserva</a>
                </li>
                <li><a href=\"/updateReserva\">Modificar una reserva</a></li>
                <li>
                    <a href=\"/removeReserva\">Eliminar una reserva</a>
                </li>

";
        
        $__internal_99798dc08c6d21dd5ae50fe6de19db7d3ddafe439b086292cbd20661174af2b0->leave($__internal_99798dc08c6d21dd5ae50fe6de19db7d3ddafe439b086292cbd20661174af2b0_prof);

    }

    // line 75
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_deb0d059886b148c4faaff1a6629b6020691ce03a0c4a73f5f8e861a1ef870ea = $this->env->getExtension("native_profiler");
        $__internal_deb0d059886b148c4faaff1a6629b6020691ce03a0c4a73f5f8e861a1ef870ea->enter($__internal_deb0d059886b148c4faaff1a6629b6020691ce03a0c4a73f5f8e861a1ef870ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 76
        echo "    <h3> ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 77
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 78
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 79
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

\t
\t<div class=\"container\">
\t  <h2>Estat de les pistes</h2>
\t  <button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#demo\">Veure estat</button>
\t  <div id=\"demo\" class=\"collapse\">
\t    
\t\t  <div class=\"container\">
\t\t  \t<div class=\"row\">
\t\t  \t \t";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pistes"]) ? $context["pistes"] : $this->getContext($context, "pistes")));
        foreach ($context['_seq'] as $context["_key"] => $context["pista"]) {
            // line 90
            echo "\t\t    \t\t<div class=\"col-sm-4\" style=\"background-color:lavender;\">
\t\t    \t\t\t<h5>Numero de Pista: ";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["pista"], "getId", array(), "method"), "html", null, true);
            echo "</h5>
\t\t    \t\t\t";
            // line 92
            if (($this->getAttribute($context["pista"], "estat", array()) == 0)) {
                // line 93
                echo "\t\t    \t\t\t\t<p>Estat: Lliure</p>
\t\t    \t\t\t";
            } else {
                // line 95
                echo "              \t\t\t\t<p>Estat: Ocupat</p>
        \t\t\t\t";
            }
            // line 97
            echo "\t\t    \t\t</div>
\t    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pista'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 99
        echo "\t  \t\t</div>
\t  \t  </div>

\t  </div>
\t</div>

";
        
        $__internal_deb0d059886b148c4faaff1a6629b6020691ce03a0c4a73f5f8e861a1ef870ea->leave($__internal_deb0d059886b148c4faaff1a6629b6020691ce03a0c4a73f5f8e861a1ef870ea_prof);

    }

    public function getTemplateName()
    {
        return "reserva/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 99,  180 => 97,  176 => 95,  172 => 93,  170 => 92,  166 => 91,  163 => 90,  159 => 89,  146 => 79,  142 => 78,  138 => 77,  133 => 76,  127 => 75,  94 => 47,  88 => 46,  42 => 6,  36 => 5,  11 => 2,);
    }
}
/* {# app/Resources/views/reserva/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block style%}*/
/* 	.col-sm-4{*/
/* 				margin-right: 1%;*/
/* 			}*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Reserva</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllReserves">Llistar totes les reserves</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertReserva">Insertar nova reserva</a></li>*/
/*                 <li>*/
/*                     <a href="/selectReserva">Seleccionar una reserva</a>*/
/*                 </li>*/
/*                 <li><a href="/updateReserva">Modificar una reserva</a></li>*/
/*                 <li>*/
/*                     <a href="/removeReserva">Eliminar una reserva</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/*     <h3> {{title}} </h3>*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* */
/* 	*/
/* 	<div class="container">*/
/* 	  <h2>Estat de les pistes</h2>*/
/* 	  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Veure estat</button>*/
/* 	  <div id="demo" class="collapse">*/
/* 	    */
/* 		  <div class="container">*/
/* 		  	<div class="row">*/
/* 		  	 	{% for pista in pistes %}*/
/* 		    		<div class="col-sm-4" style="background-color:lavender;">*/
/* 		    			<h5>Numero de Pista: {{pista.getId()}}</h5>*/
/* 		    			{% if pista.estat==0 %}*/
/* 		    				<p>Estat: Lliure</p>*/
/* 		    			{% else %}*/
/*               				<p>Estat: Ocupat</p>*/
/*         				{% endif %}*/
/* 		    		</div>*/
/* 	    		{% endfor %}*/
/* 	  		</div>*/
/* 	  	  </div>*/
/* */
/* 	  </div>*/
/* 	</div>*/
/* */
/* {% endblock %}*/
