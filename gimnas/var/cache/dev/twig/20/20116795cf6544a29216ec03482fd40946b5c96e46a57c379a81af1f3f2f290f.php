<?php

/* principal/reserva.html.twig */
class __TwigTemplate_eec777ec1215e0622c50b81dec60082618c2e7adbce04c88e6ab46bc3337a859 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/reserva.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffe4a21c2e5dac83e15312ff3651a3a46d58a40cc1658beff1889f72c3608df2 = $this->env->getExtension("native_profiler");
        $__internal_ffe4a21c2e5dac83e15312ff3651a3a46d58a40cc1658beff1889f72c3608df2->enter($__internal_ffe4a21c2e5dac83e15312ff3651a3a46d58a40cc1658beff1889f72c3608df2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/reserva.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ffe4a21c2e5dac83e15312ff3651a3a46d58a40cc1658beff1889f72c3608df2->leave($__internal_ffe4a21c2e5dac83e15312ff3651a3a46d58a40cc1658beff1889f72c3608df2_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_425c45d6ddc7629ca22066d65e12276921de3c5bf51e675c640e1a705f8cacaa = $this->env->getExtension("native_profiler");
        $__internal_425c45d6ddc7629ca22066d65e12276921de3c5bf51e675c640e1a705f8cacaa->enter($__internal_425c45d6ddc7629ca22066d65e12276921de3c5bf51e675c640e1a705f8cacaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Reserva</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 13
        echo "        </div>
";
        
        $__internal_425c45d6ddc7629ca22066d65e12276921de3c5bf51e675c640e1a705f8cacaa->leave($__internal_425c45d6ddc7629ca22066d65e12276921de3c5bf51e675c640e1a705f8cacaa_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_73e5f988858854e8c1d9cabfd7d98e37bc366d27937e684127786305862e56bb = $this->env->getExtension("native_profiler");
        $__internal_73e5f988858854e8c1d9cabfd7d98e37bc366d27937e684127786305862e56bb->enter($__internal_73e5f988858854e8c1d9cabfd7d98e37bc366d27937e684127786305862e56bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllReserves\">Llistar totes les reserves</a><br>
                <a href=\"/insertReserva\">Insertar una nova reserva</a><br>
                <a href=\"/selectReserva\">Seleccionar una reserva</a><br>
                <a href=\"/removeReserva\">Eliminar una reserva</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_73e5f988858854e8c1d9cabfd7d98e37bc366d27937e684127786305862e56bb->leave($__internal_73e5f988858854e8c1d9cabfd7d98e37bc366d27937e684127786305862e56bb_prof);

    }

    public function getTemplateName()
    {
        return "principal/reserva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 13,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/reserva.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Reserva</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllReserves">Llistar totes les reserves</a><br>*/
/*                 <a href="/insertReserva">Insertar una nova reserva</a><br>*/
/*                 <a href="/selectReserva">Seleccionar una reserva</a><br>*/
/*                 <a href="/removeReserva">Eliminar una reserva</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
