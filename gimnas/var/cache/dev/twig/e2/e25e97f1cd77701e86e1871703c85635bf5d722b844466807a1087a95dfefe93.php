<?php

/* @Security/Collector/icon.svg */
class __TwigTemplate_c131496d3b21a48134f87ddcdb3a975a0069d09905d95022b11f4fcf83dc96c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3dfeba6354a7f6010a8917cc1087a60ad267e0c52286098369db27bda43dff49 = $this->env->getExtension("native_profiler");
        $__internal_3dfeba6354a7f6010a8917cc1087a60ad267e0c52286098369db27bda43dff49->enter($__internal_3dfeba6354a7f6010a8917cc1087a60ad267e0c52286098369db27bda43dff49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Security/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z\"/>
</svg>
";
        
        $__internal_3dfeba6354a7f6010a8917cc1087a60ad267e0c52286098369db27bda43dff49->leave($__internal_3dfeba6354a7f6010a8917cc1087a60ad267e0c52286098369db27bda43dff49_prof);

    }

    public function getTemplateName()
    {
        return "@Security/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="24" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">*/
/*     <path fill="#AAAAAA" d="M21,20.4V22H3v-1.6c0-3.7,2.4-6.9,5.8-8c-1.7-1.1-2.9-3-2.9-5.2c0-3.4,2.7-6.1,6.1-6.1s6.1,2.7,6.1,6.1c0,2.2-1.2,4.1-2.9,5.2C18.6,13.5,21,16.7,21,20.4z"/>*/
/* </svg>*/
/* */
