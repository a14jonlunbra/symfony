<?php

/* default/form.html.twig */
class __TwigTemplate_b5474a13f12e87f5850318f7d8e3895013f31f6284d205c3d393370d942c141e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/form.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_386eeec4e84a52c8f5a0d0fee608ff8be7e47f54429a26d6befc084f877e7485 = $this->env->getExtension("native_profiler");
        $__internal_386eeec4e84a52c8f5a0d0fee608ff8be7e47f54429a26d6befc084f877e7485->enter($__internal_386eeec4e84a52c8f5a0d0fee608ff8be7e47f54429a26d6befc084f877e7485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_386eeec4e84a52c8f5a0d0fee608ff8be7e47f54429a26d6befc084f877e7485->leave($__internal_386eeec4e84a52c8f5a0d0fee608ff8be7e47f54429a26d6befc084f877e7485_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_282a9f00513325ae8dc83cc37678cfad403b17845e28219b46793b1479744c32 = $this->env->getExtension("native_profiler");
        $__internal_282a9f00513325ae8dc83cc37678cfad403b17845e28219b46793b1479744c32->enter($__internal_282a9f00513325ae8dc83cc37678cfad403b17845e28219b46793b1479744c32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h3> ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : $this->getContext($context, "title")), "html", null, true);
        echo " </h3>
    ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
    ";
        // line 6
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
    ";
        // line 7
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_282a9f00513325ae8dc83cc37678cfad403b17845e28219b46793b1479744c32->leave($__internal_282a9f00513325ae8dc83cc37678cfad403b17845e28219b46793b1479744c32_prof);

    }

    public function getTemplateName()
    {
        return "default/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  49 => 6,  45 => 5,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     <h3> {{title}} </h3>*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* {% endblock %}*/
