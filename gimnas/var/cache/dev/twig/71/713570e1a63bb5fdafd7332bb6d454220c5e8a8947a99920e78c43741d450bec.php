<?php

/* principal/clase.html.twig */
class __TwigTemplate_9b6a9467bfefe5c1cc4606ee45741ba9a12e5add215991c78727b89ce50afb0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/clase.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3505ee70ca7eb187af6a8d875d4eb92b638466531684d944df6b5825a3091b8 = $this->env->getExtension("native_profiler");
        $__internal_b3505ee70ca7eb187af6a8d875d4eb92b638466531684d944df6b5825a3091b8->enter($__internal_b3505ee70ca7eb187af6a8d875d4eb92b638466531684d944df6b5825a3091b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/clase.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3505ee70ca7eb187af6a8d875d4eb92b638466531684d944df6b5825a3091b8->leave($__internal_b3505ee70ca7eb187af6a8d875d4eb92b638466531684d944df6b5825a3091b8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2cf3017b96e758aade38f77fe28aa4be1b7de11115906381275e31df4c974a9b = $this->env->getExtension("native_profiler");
        $__internal_2cf3017b96e758aade38f77fe28aa4be1b7de11115906381275e31df4c974a9b->enter($__internal_2cf3017b96e758aade38f77fe28aa4be1b7de11115906381275e31df4c974a9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Clase</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 14
        echo "        </div>
";
        
        $__internal_2cf3017b96e758aade38f77fe28aa4be1b7de11115906381275e31df4c974a9b->leave($__internal_2cf3017b96e758aade38f77fe28aa4be1b7de11115906381275e31df4c974a9b_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_47682b59ce14f3e6b2f38c011bcae8aaa5f4d51eebdaa002295c3ff044f8d4a5 = $this->env->getExtension("native_profiler");
        $__internal_47682b59ce14f3e6b2f38c011bcae8aaa5f4d51eebdaa002295c3ff044f8d4a5->enter($__internal_47682b59ce14f3e6b2f38c011bcae8aaa5f4d51eebdaa002295c3ff044f8d4a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllClases\">Llistar totes les clases</a><br>
                <a href=\"/insertClase\">Insertar una nova clase</a><br>
                <a href=\"/selectClase\">Seleccionar una clase</a><br>
                <a href=\"/updateClase\">Modificar una clase</a><br>
                <a href=\"/removeClase\">Eliminar una clase</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_47682b59ce14f3e6b2f38c011bcae8aaa5f4d51eebdaa002295c3ff044f8d4a5->leave($__internal_47682b59ce14f3e6b2f38c011bcae8aaa5f4d51eebdaa002295c3ff044f8d4a5_prof);

    }

    public function getTemplateName()
    {
        return "principal/clase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 14,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/clase.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Clase</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllClases">Llistar totes les clases</a><br>*/
/*                 <a href="/insertClase">Insertar una nova clase</a><br>*/
/*                 <a href="/selectClase">Seleccionar una clase</a><br>*/
/*                 <a href="/updateClase">Modificar una clase</a><br>*/
/*                 <a href="/removeClase">Eliminar una clase</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
