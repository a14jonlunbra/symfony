<?php

/* clase/content.html.twig */
class __TwigTemplate_876630f3165ad59385cf3bfbd4c824fc882958e7f5521923f00d9a431a472565 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "clase/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_74983b084d8ef3d00d4042c291067b8f9cdbb45cc04a01c28aca907f5959b86a = $this->env->getExtension("native_profiler");
        $__internal_74983b084d8ef3d00d4042c291067b8f9cdbb45cc04a01c28aca907f5959b86a->enter($__internal_74983b084d8ef3d00d4042c291067b8f9cdbb45cc04a01c28aca907f5959b86a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clase/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_74983b084d8ef3d00d4042c291067b8f9cdbb45cc04a01c28aca907f5959b86a->leave($__internal_74983b084d8ef3d00d4042c291067b8f9cdbb45cc04a01c28aca907f5959b86a_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_0733d67aa01a63b4165763177ac3d1fb52679c156ffd3019266be3629055167a = $this->env->getExtension("native_profiler");
        $__internal_0733d67aa01a63b4165763177ac3d1fb52679c156ffd3019266be3629055167a->enter($__internal_0733d67aa01a63b4165763177ac3d1fb52679c156ffd3019266be3629055167a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_0733d67aa01a63b4165763177ac3d1fb52679c156ffd3019266be3629055167a->leave($__internal_0733d67aa01a63b4165763177ac3d1fb52679c156ffd3019266be3629055167a_prof);

    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_59909f3b1be4902c9cbb4fe882589060cbae3894ea904b8c5c43b591bdddc0f8 = $this->env->getExtension("native_profiler");
        $__internal_59909f3b1be4902c9cbb4fe882589060cbae3894ea904b8c5c43b591bdddc0f8->enter($__internal_59909f3b1be4902c9cbb4fe882589060cbae3894ea904b8c5c43b591bdddc0f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Clases</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllClases\">Llistar totes les clases</a>
                </li>
                <li>
                    <a href=\"/selectClase\">Seleccionar una clase</a>
                </li>
                <li>
                    <a href=\"/insertClase\">Insertar una nova clase</a></li>
                <li><a href=\"/updateClase\">Modificar una clase</a></li>
                <li>
                    <a href=\"/removeClase\">Eliminar una clase</a>
                </li>
";
        
        $__internal_59909f3b1be4902c9cbb4fe882589060cbae3894ea904b8c5c43b591bdddc0f8->leave($__internal_59909f3b1be4902c9cbb4fe882589060cbae3894ea904b8c5c43b591bdddc0f8_prof);

    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_af9107b67e22ee6c5612ab5ef4dbf152abd9a51ba1eb654e192ed63ce97c39d5 = $this->env->getExtension("native_profiler");
        $__internal_af9107b67e22ee6c5612ab5ef4dbf152abd9a51ba1eb654e192ed63ce97c39d5->enter($__internal_af9107b67e22ee6c5612ab5ef4dbf152abd9a51ba1eb654e192ed63ce97c39d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 72
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>monitor</th>
                <th>nom</th>
                <th>descripcio</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clase"]) ? $context["clase"] : $this->getContext($context, "clase")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 88
            echo "                <tr>
                  <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "monitor", array()), "getNom", array(), "method"), "html", null, true);
            echo "</td>
                  <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nom", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "descripcio", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_af9107b67e22ee6c5612ab5ef4dbf152abd9a51ba1eb654e192ed63ce97c39d5->leave($__internal_af9107b67e22ee6c5612ab5ef4dbf152abd9a51ba1eb654e192ed63ce97c39d5_prof);

    }

    public function getTemplateName()
    {
        return "clase/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 94,  162 => 91,  158 => 90,  154 => 89,  151 => 88,  147 => 87,  130 => 72,  124 => 71,  92 => 44,  86 => 43,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/clase/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Clases</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllClases">Llistar totes les clases</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectClase">Seleccionar una clase</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertClase">Insertar una nova clase</a></li>*/
/*                 <li><a href="/updateClase">Modificar una clase</a></li>*/
/*                 <li>*/
/*                     <a href="/removeClase">Eliminar una clase</a>*/
/*                 </li>*/
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>monitor</th>*/
/*                 <th>nom</th>*/
/*                 <th>descripcio</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for c in clase %}*/
/*                 <tr>*/
/*                   <td>{{ c.monitor.getNom() }}</td>*/
/*                   <td>{{ c.nom }}</td>*/
/*                   <td>{{ c.descripcio }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
