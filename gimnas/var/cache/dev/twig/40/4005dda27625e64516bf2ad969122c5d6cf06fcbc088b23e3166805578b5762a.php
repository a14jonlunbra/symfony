<?php

/* default/message.html.twig */
class __TwigTemplate_66e47f2b4ede40e73bde5a7388fc34cee53ecefdacb3dd191ee4f50ef505bbba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/message.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_907a33d9a9ff0accd839159edde00abeb8593313238d23a565a922fd72c639c6 = $this->env->getExtension("native_profiler");
        $__internal_907a33d9a9ff0accd839159edde00abeb8593313238d23a565a922fd72c639c6->enter($__internal_907a33d9a9ff0accd839159edde00abeb8593313238d23a565a922fd72c639c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_907a33d9a9ff0accd839159edde00abeb8593313238d23a565a922fd72c639c6->leave($__internal_907a33d9a9ff0accd839159edde00abeb8593313238d23a565a922fd72c639c6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_18f350f4c57228bf682912da455b64b83e7ad6bcb2759d1d9c48eb3455e28771 = $this->env->getExtension("native_profiler");
        $__internal_18f350f4c57228bf682912da455b64b83e7ad6bcb2759d1d9c48eb3455e28771->enter($__internal_18f350f4c57228bf682912da455b64b83e7ad6bcb2759d1d9c48eb3455e28771_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
";
        
        $__internal_18f350f4c57228bf682912da455b64b83e7ad6bcb2759d1d9c48eb3455e28771->leave($__internal_18f350f4c57228bf682912da455b64b83e7ad6bcb2759d1d9c48eb3455e28771_prof);

    }

    public function getTemplateName()
    {
        return "default/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/message.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/*     {{ message }}*/
/* {% endblock %}*/
