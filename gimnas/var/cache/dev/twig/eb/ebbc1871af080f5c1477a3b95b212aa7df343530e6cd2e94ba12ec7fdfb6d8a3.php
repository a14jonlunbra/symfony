<?php

/* principal/usuari.html.twig */
class __TwigTemplate_d49a69893b04130a34cf5a5e332cd9d35a5afc2e3786e616693ea50423f008e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/usuari.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_61de7459780dceb19beb8f1fa2056157db602b04870f001d67d5e462db5a0412 = $this->env->getExtension("native_profiler");
        $__internal_61de7459780dceb19beb8f1fa2056157db602b04870f001d67d5e462db5a0412->enter($__internal_61de7459780dceb19beb8f1fa2056157db602b04870f001d67d5e462db5a0412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/usuari.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_61de7459780dceb19beb8f1fa2056157db602b04870f001d67d5e462db5a0412->leave($__internal_61de7459780dceb19beb8f1fa2056157db602b04870f001d67d5e462db5a0412_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d7cccb057b76123bbc9d5f0801aa865ac5a6d7a0d7ae002c0d0e66b2679475c8 = $this->env->getExtension("native_profiler");
        $__internal_d7cccb057b76123bbc9d5f0801aa865ac5a6d7a0d7ae002c0d0e66b2679475c8->enter($__internal_d7cccb057b76123bbc9d5f0801aa865ac5a6d7a0d7ae002c0d0e66b2679475c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Usuari</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>
";
        
        $__internal_d7cccb057b76123bbc9d5f0801aa865ac5a6d7a0d7ae002c0d0e66b2679475c8->leave($__internal_d7cccb057b76123bbc9d5f0801aa865ac5a6d7a0d7ae002c0d0e66b2679475c8_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_a2c64f960020f15f6af12d55501d24847602b16f7ca33f9c44232b7594ead9d5 = $this->env->getExtension("native_profiler");
        $__internal_a2c64f960020f15f6af12d55501d24847602b16f7ca33f9c44232b7594ead9d5->enter($__internal_a2c64f960020f15f6af12d55501d24847602b16f7ca33f9c44232b7594ead9d5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllUsers\">Llistar tots els usuaris</a><br>
                <a href=\"/selectAllDate\">Llistar tots els usuaris per data d'inscripcio</a><br>
                <a href=\"/insertUsuari\">Insertar nou usuari</a><br>
                <a href=\"/selectUsuari\">Seleccionar un usuari</a><br>
                <a href=\"/updateUsuari\">Modificar un usuari</a><br>
                <a href=\"/removeUsuari\">Eliminar un usuari</a>
                <br>
            ";
        
        $__internal_a2c64f960020f15f6af12d55501d24847602b16f7ca33f9c44232b7594ead9d5->leave($__internal_a2c64f960020f15f6af12d55501d24847602b16f7ca33f9c44232b7594ead9d5_prof);

    }

    public function getTemplateName()
    {
        return "principal/usuari.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/usuari.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Usuari</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllUsers">Llistar tots els usuaris</a><br>*/
/*                 <a href="/selectAllDate">Llistar tots els usuaris per data d'inscripcio</a><br>*/
/*                 <a href="/insertUsuari">Insertar nou usuari</a><br>*/
/*                 <a href="/selectUsuari">Seleccionar un usuari</a><br>*/
/*                 <a href="/updateUsuari">Modificar un usuari</a><br>*/
/*                 <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
