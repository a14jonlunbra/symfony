<?php

/* default/message.html.twig */
class __TwigTemplate_fdfc6f843229d8ffb6b1931827ff93f7985b8fcfebf33020960e7d185531db35 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/message.html.twig", 2);
        $this->blocks = array(
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_52fa33d69da885659ad88e5f99e435993237be1938e478b1a6229d77e536a305 = $this->env->getExtension("native_profiler");
        $__internal_52fa33d69da885659ad88e5f99e435993237be1938e478b1a6229d77e536a305->enter($__internal_52fa33d69da885659ad88e5f99e435993237be1938e478b1a6229d77e536a305_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_52fa33d69da885659ad88e5f99e435993237be1938e478b1a6229d77e536a305->leave($__internal_52fa33d69da885659ad88e5f99e435993237be1938e478b1a6229d77e536a305_prof);

    }

    // line 3
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_83937b3c88f5f0a9992079fcb63f90d10b40df7504a665115107c96683286cd5 = $this->env->getExtension("native_profiler");
        $__internal_83937b3c88f5f0a9992079fcb63f90d10b40df7504a665115107c96683286cd5->enter($__internal_83937b3c88f5f0a9992079fcb63f90d10b40df7504a665115107c96683286cd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
";
        
        $__internal_83937b3c88f5f0a9992079fcb63f90d10b40df7504a665115107c96683286cd5->leave($__internal_83937b3c88f5f0a9992079fcb63f90d10b40df7504a665115107c96683286cd5_prof);

    }

    public function getTemplateName()
    {
        return "default/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/message.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block mainContent %}*/
/*     {{ message }}*/
/* {% endblock %}*/
