<?php

/* principal/usuari.html.twig */
class __TwigTemplate_ab74e51a59c0797171376146be3824c4dad1696e83b8de0f4f2251ff3941856e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/usuari.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_32d606235af4f9fb9e8c732e889724ec7a241be6432666e0f7f8195108c2c67f = $this->env->getExtension("native_profiler");
        $__internal_32d606235af4f9fb9e8c732e889724ec7a241be6432666e0f7f8195108c2c67f->enter($__internal_32d606235af4f9fb9e8c732e889724ec7a241be6432666e0f7f8195108c2c67f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/usuari.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_32d606235af4f9fb9e8c732e889724ec7a241be6432666e0f7f8195108c2c67f->leave($__internal_32d606235af4f9fb9e8c732e889724ec7a241be6432666e0f7f8195108c2c67f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6c5cb1e41b2359268f21b5ef9bf20a3372aaf3984caafcb198f349068419856e = $this->env->getExtension("native_profiler");
        $__internal_6c5cb1e41b2359268f21b5ef9bf20a3372aaf3984caafcb198f349068419856e->enter($__internal_6c5cb1e41b2359268f21b5ef9bf20a3372aaf3984caafcb198f349068419856e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Usuari</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>
";
        
        $__internal_6c5cb1e41b2359268f21b5ef9bf20a3372aaf3984caafcb198f349068419856e->leave($__internal_6c5cb1e41b2359268f21b5ef9bf20a3372aaf3984caafcb198f349068419856e_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_7b110931504fcb2b87602b516cd3cbd72e89bc74703c38b23e598f5e0a9c96e1 = $this->env->getExtension("native_profiler");
        $__internal_7b110931504fcb2b87602b516cd3cbd72e89bc74703c38b23e598f5e0a9c96e1->enter($__internal_7b110931504fcb2b87602b516cd3cbd72e89bc74703c38b23e598f5e0a9c96e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllUsers\">Llistar tots els usuaris</a><br>
                <a href=\"/selectAllDate\">Llistar tots els usuaris per data d'inscripcio</a><br>
                <a href=\"/insertUsuari\">Insertar nou usuari</a><br>
                <a href=\"/selectUsuari\">Seleccionar un usuari</a><br>
                <a href=\"/updateUsuari\">Modificar un usuari</a><br>
                <a href=\"/removeUsuari\">Eliminar un usuari</a>
                <br>
            ";
        
        $__internal_7b110931504fcb2b87602b516cd3cbd72e89bc74703c38b23e598f5e0a9c96e1->leave($__internal_7b110931504fcb2b87602b516cd3cbd72e89bc74703c38b23e598f5e0a9c96e1_prof);

    }

    public function getTemplateName()
    {
        return "principal/usuari.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/usuari.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Usuari</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllUsers">Llistar tots els usuaris</a><br>*/
/*                 <a href="/selectAllDate">Llistar tots els usuaris per data d'inscripcio</a><br>*/
/*                 <a href="/insertUsuari">Insertar nou usuari</a><br>*/
/*                 <a href="/selectUsuari">Seleccionar un usuari</a><br>*/
/*                 <a href="/updateUsuari">Modificar un usuari</a><br>*/
/*                 <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
