<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_d564ec7a2dc6066a83310897114406649c591ae2fee4af65dc952987b044c05f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3fd29023366f7c29968dd06ad5c3ff42dac9649c18f3d56ef05cdd94d49767b = $this->env->getExtension("native_profiler");
        $__internal_e3fd29023366f7c29968dd06ad5c3ff42dac9649c18f3d56ef05cdd94d49767b->enter($__internal_e3fd29023366f7c29968dd06ad5c3ff42dac9649c18f3d56ef05cdd94d49767b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e3fd29023366f7c29968dd06ad5c3ff42dac9649c18f3d56ef05cdd94d49767b->leave($__internal_e3fd29023366f7c29968dd06ad5c3ff42dac9649c18f3d56ef05cdd94d49767b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5af3428680fa291ec229b93ba75ceb62c778f48b74c92696512f5f9e62a69f06 = $this->env->getExtension("native_profiler");
        $__internal_5af3428680fa291ec229b93ba75ceb62c778f48b74c92696512f5f9e62a69f06->enter($__internal_5af3428680fa291ec229b93ba75ceb62c778f48b74c92696512f5f9e62a69f06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_5af3428680fa291ec229b93ba75ceb62c778f48b74c92696512f5f9e62a69f06->leave($__internal_5af3428680fa291ec229b93ba75ceb62c778f48b74c92696512f5f9e62a69f06_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1957bb3e7f2a2accd954ee8d0be80965aeead7e82debd8abc548399f77545b62 = $this->env->getExtension("native_profiler");
        $__internal_1957bb3e7f2a2accd954ee8d0be80965aeead7e82debd8abc548399f77545b62->enter($__internal_1957bb3e7f2a2accd954ee8d0be80965aeead7e82debd8abc548399f77545b62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_1957bb3e7f2a2accd954ee8d0be80965aeead7e82debd8abc548399f77545b62->leave($__internal_1957bb3e7f2a2accd954ee8d0be80965aeead7e82debd8abc548399f77545b62_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_f17066a35a4e18fac694dfad4ebfdd54f2e79f8193294dc2b3acf5de36dd8bb0 = $this->env->getExtension("native_profiler");
        $__internal_f17066a35a4e18fac694dfad4ebfdd54f2e79f8193294dc2b3acf5de36dd8bb0->enter($__internal_f17066a35a4e18fac694dfad4ebfdd54f2e79f8193294dc2b3acf5de36dd8bb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_f17066a35a4e18fac694dfad4ebfdd54f2e79f8193294dc2b3acf5de36dd8bb0->leave($__internal_f17066a35a4e18fac694dfad4ebfdd54f2e79f8193294dc2b3acf5de36dd8bb0_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
