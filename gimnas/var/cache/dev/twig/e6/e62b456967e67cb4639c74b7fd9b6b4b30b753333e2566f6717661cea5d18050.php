<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_6b7ea59800c376bfa2a6fb03d1b8adf9e54797dd2c8ffc9ffd1990698cd8592b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8246cac3343e83b169635f6b4d9f37f7fb6ec81c67460055c998e34d4997205 = $this->env->getExtension("native_profiler");
        $__internal_c8246cac3343e83b169635f6b4d9f37f7fb6ec81c67460055c998e34d4997205->enter($__internal_c8246cac3343e83b169635f6b4d9f37f7fb6ec81c67460055c998e34d4997205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c8246cac3343e83b169635f6b4d9f37f7fb6ec81c67460055c998e34d4997205->leave($__internal_c8246cac3343e83b169635f6b4d9f37f7fb6ec81c67460055c998e34d4997205_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_927dd478a380c0a07bd5b5dda8b64535e60452c38a15412f60550d22dab28a8c = $this->env->getExtension("native_profiler");
        $__internal_927dd478a380c0a07bd5b5dda8b64535e60452c38a15412f60550d22dab28a8c->enter($__internal_927dd478a380c0a07bd5b5dda8b64535e60452c38a15412f60550d22dab28a8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_927dd478a380c0a07bd5b5dda8b64535e60452c38a15412f60550d22dab28a8c->leave($__internal_927dd478a380c0a07bd5b5dda8b64535e60452c38a15412f60550d22dab28a8c_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_8925de6174a4366e7130e099babce840e70332dee3a19a67fbc412efe71182ff = $this->env->getExtension("native_profiler");
        $__internal_8925de6174a4366e7130e099babce840e70332dee3a19a67fbc412efe71182ff->enter($__internal_8925de6174a4366e7130e099babce840e70332dee3a19a67fbc412efe71182ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_8925de6174a4366e7130e099babce840e70332dee3a19a67fbc412efe71182ff->leave($__internal_8925de6174a4366e7130e099babce840e70332dee3a19a67fbc412efe71182ff_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_e623a74bf051780328fc13530b6a5db05ca5af2498765032e05a4b073c8827dc = $this->env->getExtension("native_profiler");
        $__internal_e623a74bf051780328fc13530b6a5db05ca5af2498765032e05a4b073c8827dc->enter($__internal_e623a74bf051780328fc13530b6a5db05ca5af2498765032e05a4b073c8827dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_e623a74bf051780328fc13530b6a5db05ca5af2498765032e05a4b073c8827dc->leave($__internal_e623a74bf051780328fc13530b6a5db05ca5af2498765032e05a4b073c8827dc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
