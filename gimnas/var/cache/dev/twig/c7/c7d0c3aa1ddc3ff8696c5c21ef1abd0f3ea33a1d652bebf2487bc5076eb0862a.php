<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_4162d9f4e30e34a12847b4b1d05ba08625aeeb3c4b246f9b9937d4fd382cc32c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_031b91414f42fe683f5968e3db5c11285c2c43249996f4a38e49a94686d3abff = $this->env->getExtension("native_profiler");
        $__internal_031b91414f42fe683f5968e3db5c11285c2c43249996f4a38e49a94686d3abff->enter($__internal_031b91414f42fe683f5968e3db5c11285c2c43249996f4a38e49a94686d3abff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_031b91414f42fe683f5968e3db5c11285c2c43249996f4a38e49a94686d3abff->leave($__internal_031b91414f42fe683f5968e3db5c11285c2c43249996f4a38e49a94686d3abff_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_dd4410ff1e37021ca659c28663b5cfca125fb71fea33e7373b6bfd60fc827a94 = $this->env->getExtension("native_profiler");
        $__internal_dd4410ff1e37021ca659c28663b5cfca125fb71fea33e7373b6bfd60fc827a94->enter($__internal_dd4410ff1e37021ca659c28663b5cfca125fb71fea33e7373b6bfd60fc827a94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_dd4410ff1e37021ca659c28663b5cfca125fb71fea33e7373b6bfd60fc827a94->leave($__internal_dd4410ff1e37021ca659c28663b5cfca125fb71fea33e7373b6bfd60fc827a94_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_34dfccb6fb40d039eb13e7e1ab2e742136bd1717b7d0941178bc09779b357d57 = $this->env->getExtension("native_profiler");
        $__internal_34dfccb6fb40d039eb13e7e1ab2e742136bd1717b7d0941178bc09779b357d57->enter($__internal_34dfccb6fb40d039eb13e7e1ab2e742136bd1717b7d0941178bc09779b357d57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_34dfccb6fb40d039eb13e7e1ab2e742136bd1717b7d0941178bc09779b357d57->leave($__internal_34dfccb6fb40d039eb13e7e1ab2e742136bd1717b7d0941178bc09779b357d57_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_81c6a639790e17118323ae0f096e0331afa3e02ea38ee298a699b8a3c395424e = $this->env->getExtension("native_profiler");
        $__internal_81c6a639790e17118323ae0f096e0331afa3e02ea38ee298a699b8a3c395424e->enter($__internal_81c6a639790e17118323ae0f096e0331afa3e02ea38ee298a699b8a3c395424e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_81c6a639790e17118323ae0f096e0331afa3e02ea38ee298a699b8a3c395424e->leave($__internal_81c6a639790e17118323ae0f096e0331afa3e02ea38ee298a699b8a3c395424e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
