<?php

/* principal/monitor.html.twig */
class __TwigTemplate_74419da1625f4a256f72a23937cf6f9473d21a5568ce3b46380cc9fb55ae76a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/monitor.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0527a21eea016b1284565296d1dc650f90d387a0b8fd9fc280ab65bf5886d1bc = $this->env->getExtension("native_profiler");
        $__internal_0527a21eea016b1284565296d1dc650f90d387a0b8fd9fc280ab65bf5886d1bc->enter($__internal_0527a21eea016b1284565296d1dc650f90d387a0b8fd9fc280ab65bf5886d1bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/monitor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0527a21eea016b1284565296d1dc650f90d387a0b8fd9fc280ab65bf5886d1bc->leave($__internal_0527a21eea016b1284565296d1dc650f90d387a0b8fd9fc280ab65bf5886d1bc_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2d2ab23a5915978701a5d8b6b41aee5ee84658f3312081ea1c1efffa8ed5faee = $this->env->getExtension("native_profiler");
        $__internal_2d2ab23a5915978701a5d8b6b41aee5ee84658f3312081ea1c1efffa8ed5faee->enter($__internal_2d2ab23a5915978701a5d8b6b41aee5ee84658f3312081ea1c1efffa8ed5faee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Monitor</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>

";
        
        $__internal_2d2ab23a5915978701a5d8b6b41aee5ee84658f3312081ea1c1efffa8ed5faee->leave($__internal_2d2ab23a5915978701a5d8b6b41aee5ee84658f3312081ea1c1efffa8ed5faee_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_7932bdfd0311e1855c0839c646cc12964a5438c6aaf809e7695d24e08595006e = $this->env->getExtension("native_profiler");
        $__internal_7932bdfd0311e1855c0839c646cc12964a5438c6aaf809e7695d24e08595006e->enter($__internal_7932bdfd0311e1855c0839c646cc12964a5438c6aaf809e7695d24e08595006e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllMonitors\">Llistar tots els monitors</a><br>
                <a href=\"/selectAllCognom1\">Llistar tots els monitors per el primer cognom</a><br>
                <a href=\"/insertMonitor\">Insertar nou monitor</a><br>
                <a href=\"/selectMonitor\">Seleccionar un monitor</a><br>
                <a href=\"/updateMonitor\">Modificar un monitor</a><br>
                <a href=\"/removeMonitor\">Eliminar un monitor</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_7932bdfd0311e1855c0839c646cc12964a5438c6aaf809e7695d24e08595006e->leave($__internal_7932bdfd0311e1855c0839c646cc12964a5438c6aaf809e7695d24e08595006e_prof);

    }

    public function getTemplateName()
    {
        return "principal/monitor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 7,  56 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/monitor.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Monitor</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllMonitors">Llistar tots els monitors</a><br>*/
/*                 <a href="/selectAllCognom1">Llistar tots els monitors per el primer cognom</a><br>*/
/*                 <a href="/insertMonitor">Insertar nou monitor</a><br>*/
/*                 <a href="/selectMonitor">Seleccionar un monitor</a><br>*/
/*                 <a href="/updateMonitor">Modificar un monitor</a><br>*/
/*                 <a href="/removeMonitor">Eliminar un monitor</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* */
/* {% endblock %}*/
