<?php

/* base.html.twig */
class __TwigTemplate_e565fd9521e836f18cb767cee047a2faf582f15a46e07bf88248b5a3b6b2b011 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b09681abba78fa03b0812c7ba6403d7ea39a7ecfafa13b8973fef5de77de2b7e = $this->env->getExtension("native_profiler");
        $__internal_b09681abba78fa03b0812c7ba6403d7ea39a7ecfafa13b8973fef5de77de2b7e->enter($__internal_b09681abba78fa03b0812c7ba6403d7ea39a7ecfafa13b8973fef5de77de2b7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "        <style>
            .mainContent{
                margin-left: 20%;
            }
            .table-responsive{
                margin-right: 19%;
            }
          ";
        // line 24
        $this->displayBlock('style', $context, $blocks);
        // line 25
        echo "        </style>
    </head>
    
    <body>

         <nav class=\"navbar navbar-inverse navbar-static-top custom-navbar\" role=\"navigation\">
            <div class=\"container\">
             <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
             </button>
             <div class=\"navbar-header\">
                 <a class=\"navbar-brand\" rel=\"home\" href=\"/\" title=\"Help\"> Projecte Symfony</a>
             </div>
             <!-- Non-collapsing right-side icons -->
             <ul class=\"nav navbar-nav navbar-right\">
              <li>
                   <a href=\"#\" class=\"fa fa-cog\"></a>
              </li>
              <li>
                   <a href=\"#\" class=\"fa fa-home\"></a>
              </li>
             </ul>
             <!-- the collapsing menu -->
             <div class=\"collapse navbar-collapse navbar-left\" id=\"navbar-collapse-1\">
               <ul class=\"nav navbar-nav\">
                   <li><a href=\"/selectAllUsers\">Usuaris</a></li>
                   <li><a href=\"/selectAllMonitors\">Monitors</a></li>
                   <li><a href=\"/selectAllClases\">Clases</a></li>
                   <li><a href=\"/selectAllPistes\">Pistes</a></li>
                   <li><a href=\"/selectAllReserves\">Reserves</a></li>
               </ul>
             </div>
            <!--/.nav-collapse -->
            </div>
            <!--/.container -->
         </nav>

        <div id=\"content\">
            <nav>
                ";
        // line 67
        $this->displayBlock('menu_aside', $context, $blocks);
        // line 68
        echo "            </nav>
            <main>
                <div class=\"mainContent\">
                    ";
        // line 71
        $this->displayBlock('mainContent', $context, $blocks);
        // line 72
        echo "                </div>
            </main>

        </div>

    </body>

</html>";
        
        $__internal_b09681abba78fa03b0812c7ba6403d7ea39a7ecfafa13b8973fef5de77de2b7e->leave($__internal_b09681abba78fa03b0812c7ba6403d7ea39a7ecfafa13b8973fef5de77de2b7e_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_664a5105e0342d315cf038563344fab77b87d0cd3ae824892852352476b844d4 = $this->env->getExtension("native_profiler");
        $__internal_664a5105e0342d315cf038563344fab77b87d0cd3ae824892852352476b844d4->enter($__internal_664a5105e0342d315cf038563344fab77b87d0cd3ae824892852352476b844d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Product Application";
        
        $__internal_664a5105e0342d315cf038563344fab77b87d0cd3ae824892852352476b844d4->leave($__internal_664a5105e0342d315cf038563344fab77b87d0cd3ae824892852352476b844d4_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a3c6dbe574a9d14f031b2e0e05f97ba7a4847583b6de5bd007fcc1169b83df73 = $this->env->getExtension("native_profiler");
        $__internal_a3c6dbe574a9d14f031b2e0e05f97ba7a4847583b6de5bd007fcc1169b83df73->enter($__internal_a3c6dbe574a9d14f031b2e0e05f97ba7a4847583b6de5bd007fcc1169b83df73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "         <!-- Latest compiled and minified CSS -->
         <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">

         <!-- jQuery library -->
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>

         <!-- Latest compiled JavaScript -->
         <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        ";
        
        $__internal_a3c6dbe574a9d14f031b2e0e05f97ba7a4847583b6de5bd007fcc1169b83df73->leave($__internal_a3c6dbe574a9d14f031b2e0e05f97ba7a4847583b6de5bd007fcc1169b83df73_prof);

    }

    // line 24
    public function block_style($context, array $blocks = array())
    {
        $__internal_310b6a1dc18e00d4be018cb42d1805568c1b0be554cd1e8dd332065d0030455a = $this->env->getExtension("native_profiler");
        $__internal_310b6a1dc18e00d4be018cb42d1805568c1b0be554cd1e8dd332065d0030455a->enter($__internal_310b6a1dc18e00d4be018cb42d1805568c1b0be554cd1e8dd332065d0030455a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        
        $__internal_310b6a1dc18e00d4be018cb42d1805568c1b0be554cd1e8dd332065d0030455a->leave($__internal_310b6a1dc18e00d4be018cb42d1805568c1b0be554cd1e8dd332065d0030455a_prof);

    }

    // line 67
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_fee160714548126d359384e95978693f0a25c46f7a2a39823843ef76c80136cf = $this->env->getExtension("native_profiler");
        $__internal_fee160714548126d359384e95978693f0a25c46f7a2a39823843ef76c80136cf->enter($__internal_fee160714548126d359384e95978693f0a25c46f7a2a39823843ef76c80136cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        echo " ";
        
        $__internal_fee160714548126d359384e95978693f0a25c46f7a2a39823843ef76c80136cf->leave($__internal_fee160714548126d359384e95978693f0a25c46f7a2a39823843ef76c80136cf_prof);

    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_6e01a91fb491617497bd85f8f664188332d0bdd3aa9f399a826b1189cf4aa443 = $this->env->getExtension("native_profiler");
        $__internal_6e01a91fb491617497bd85f8f664188332d0bdd3aa9f399a826b1189cf4aa443->enter($__internal_6e01a91fb491617497bd85f8f664188332d0bdd3aa9f399a826b1189cf4aa443_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        
        $__internal_6e01a91fb491617497bd85f8f664188332d0bdd3aa9f399a826b1189cf4aa443->leave($__internal_6e01a91fb491617497bd85f8f664188332d0bdd3aa9f399a826b1189cf4aa443_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  173 => 71,  161 => 67,  150 => 24,  135 => 8,  129 => 7,  117 => 6,  103 => 72,  101 => 71,  96 => 68,  94 => 67,  50 => 25,  48 => 24,  39 => 17,  37 => 7,  33 => 6,  27 => 2,);
    }
}
/* {# app/Resources/views/base.html.twig #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{% block title %}Product Application{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*          <!-- Latest compiled and minified CSS -->*/
/*          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/* */
/*          <!-- jQuery library -->*/
/*          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/* */
/*          <!-- Latest compiled JavaScript -->*/
/*          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>*/
/*         {% endblock %}*/
/*         <style>*/
/*             .mainContent{*/
/*                 margin-left: 20%;*/
/*             }*/
/*             .table-responsive{*/
/*                 margin-right: 19%;*/
/*             }*/
/*           {% block style%}{% endblock %}*/
/*         </style>*/
/*     </head>*/
/*     */
/*     <body>*/
/* */
/*          <nav class="navbar navbar-inverse navbar-static-top custom-navbar" role="navigation">*/
/*             <div class="container">*/
/*              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*              </button>*/
/*              <div class="navbar-header">*/
/*                  <a class="navbar-brand" rel="home" href="/" title="Help"> Projecte Symfony</a>*/
/*              </div>*/
/*              <!-- Non-collapsing right-side icons -->*/
/*              <ul class="nav navbar-nav navbar-right">*/
/*               <li>*/
/*                    <a href="#" class="fa fa-cog"></a>*/
/*               </li>*/
/*               <li>*/
/*                    <a href="#" class="fa fa-home"></a>*/
/*               </li>*/
/*              </ul>*/
/*              <!-- the collapsing menu -->*/
/*              <div class="collapse navbar-collapse navbar-left" id="navbar-collapse-1">*/
/*                <ul class="nav navbar-nav">*/
/*                    <li><a href="/selectAllUsers">Usuaris</a></li>*/
/*                    <li><a href="/selectAllMonitors">Monitors</a></li>*/
/*                    <li><a href="/selectAllClases">Clases</a></li>*/
/*                    <li><a href="/selectAllPistes">Pistes</a></li>*/
/*                    <li><a href="/selectAllReserves">Reserves</a></li>*/
/*                </ul>*/
/*              </div>*/
/*             <!--/.nav-collapse -->*/
/*             </div>*/
/*             <!--/.container -->*/
/*          </nav>*/
/* */
/*         <div id="content">*/
/*             <nav>*/
/*                 {% block menu_aside %} {% endblock %}*/
/*             </nav>*/
/*             <main>*/
/*                 <div class="mainContent">*/
/*                     {% block mainContent %}{% endblock %}*/
/*                 </div>*/
/*             </main>*/
/* */
/*         </div>*/
/* */
/*     </body>*/
/* */
/* </html>*/
