<?php

/* principal/clase.html.twig */
class __TwigTemplate_c6e81cf26999068f19eaac05253b562c77f2eb406b7dadbc6cb57fd5a2659d8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/clase.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_428cb8e2b4e96c806c11b69ae0bec0b7c6d3f74422cd7d6f54b8c6e65477b39f = $this->env->getExtension("native_profiler");
        $__internal_428cb8e2b4e96c806c11b69ae0bec0b7c6d3f74422cd7d6f54b8c6e65477b39f->enter($__internal_428cb8e2b4e96c806c11b69ae0bec0b7c6d3f74422cd7d6f54b8c6e65477b39f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/clase.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_428cb8e2b4e96c806c11b69ae0bec0b7c6d3f74422cd7d6f54b8c6e65477b39f->leave($__internal_428cb8e2b4e96c806c11b69ae0bec0b7c6d3f74422cd7d6f54b8c6e65477b39f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_207fe36cd58aa4fdf7c6f86489739c2816bef66662005d29dfa6dc18edeef41b = $this->env->getExtension("native_profiler");
        $__internal_207fe36cd58aa4fdf7c6f86489739c2816bef66662005d29dfa6dc18edeef41b->enter($__internal_207fe36cd58aa4fdf7c6f86489739c2816bef66662005d29dfa6dc18edeef41b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Clase</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 14
        echo "        </div>
";
        
        $__internal_207fe36cd58aa4fdf7c6f86489739c2816bef66662005d29dfa6dc18edeef41b->leave($__internal_207fe36cd58aa4fdf7c6f86489739c2816bef66662005d29dfa6dc18edeef41b_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_40f194e2788b509c2ea4bd2d51069d8bb8e8ac26b5f6fc119ce0a98097b0eb6b = $this->env->getExtension("native_profiler");
        $__internal_40f194e2788b509c2ea4bd2d51069d8bb8e8ac26b5f6fc119ce0a98097b0eb6b->enter($__internal_40f194e2788b509c2ea4bd2d51069d8bb8e8ac26b5f6fc119ce0a98097b0eb6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllClases\">Llistar totes les clases</a><br>
                <a href=\"/insertClase\">Insertar una nova clase</a><br>
                <a href=\"/selectClase\">Seleccionar una clase</a><br>
                <a href=\"/updateClase\">Modificar una clase</a><br>
                <a href=\"/removeClase\">Eliminar una clase</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_40f194e2788b509c2ea4bd2d51069d8bb8e8ac26b5f6fc119ce0a98097b0eb6b->leave($__internal_40f194e2788b509c2ea4bd2d51069d8bb8e8ac26b5f6fc119ce0a98097b0eb6b_prof);

    }

    public function getTemplateName()
    {
        return "principal/clase.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 14,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/clase.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Clase</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllClases">Llistar totes les clases</a><br>*/
/*                 <a href="/insertClase">Insertar una nova clase</a><br>*/
/*                 <a href="/selectClase">Seleccionar una clase</a><br>*/
/*                 <a href="/updateClase">Modificar una clase</a><br>*/
/*                 <a href="/removeClase">Eliminar una clase</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
