<?php

/* principal/reserva.html.twig */
class __TwigTemplate_68f1c81bae9f5c507657c68a8ee94e382ecccf4c3f616f49132e54e3513b20bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/reserva.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2005227ed06808689424c757e7b12aa4f06f3785fb2293b034f75512c09e5d6 = $this->env->getExtension("native_profiler");
        $__internal_a2005227ed06808689424c757e7b12aa4f06f3785fb2293b034f75512c09e5d6->enter($__internal_a2005227ed06808689424c757e7b12aa4f06f3785fb2293b034f75512c09e5d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/reserva.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2005227ed06808689424c757e7b12aa4f06f3785fb2293b034f75512c09e5d6->leave($__internal_a2005227ed06808689424c757e7b12aa4f06f3785fb2293b034f75512c09e5d6_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_107b0a2f3af000a7aa29df2dc54fbb01224870d27323fd63dcc5c0b3249aa71a = $this->env->getExtension("native_profiler");
        $__internal_107b0a2f3af000a7aa29df2dc54fbb01224870d27323fd63dcc5c0b3249aa71a->enter($__internal_107b0a2f3af000a7aa29df2dc54fbb01224870d27323fd63dcc5c0b3249aa71a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Reserva</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 13
        echo "        </div>
";
        
        $__internal_107b0a2f3af000a7aa29df2dc54fbb01224870d27323fd63dcc5c0b3249aa71a->leave($__internal_107b0a2f3af000a7aa29df2dc54fbb01224870d27323fd63dcc5c0b3249aa71a_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_72cf3d8e4676aa54e24c62b3233a49cea2d8806c2604994a8cbfaf23c4b2a6f8 = $this->env->getExtension("native_profiler");
        $__internal_72cf3d8e4676aa54e24c62b3233a49cea2d8806c2604994a8cbfaf23c4b2a6f8->enter($__internal_72cf3d8e4676aa54e24c62b3233a49cea2d8806c2604994a8cbfaf23c4b2a6f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllReserves\">Llistar totes les reserves</a><br>
                <a href=\"/insertReserva\">Insertar una nova reserva</a><br>
                <a href=\"/selectReserva\">Seleccionar una reserva</a><br>
                <a href=\"/removeReserva\">Eliminar una reserva</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_72cf3d8e4676aa54e24c62b3233a49cea2d8806c2604994a8cbfaf23c4b2a6f8->leave($__internal_72cf3d8e4676aa54e24c62b3233a49cea2d8806c2604994a8cbfaf23c4b2a6f8_prof);

    }

    public function getTemplateName()
    {
        return "principal/reserva.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 13,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/reserva.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Reserva</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllReserves">Llistar totes les reserves</a><br>*/
/*                 <a href="/insertReserva">Insertar una nova reserva</a><br>*/
/*                 <a href="/selectReserva">Seleccionar una reserva</a><br>*/
/*                 <a href="/removeReserva">Eliminar una reserva</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
