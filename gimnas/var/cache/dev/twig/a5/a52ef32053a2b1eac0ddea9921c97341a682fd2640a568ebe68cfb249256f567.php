<?php

/* pista/content.html.twig */
class __TwigTemplate_4daea2c7b03777c1200537453d694e8dc7502ff8b9b4681e910a24a78913fc21 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "pista/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7e4b3056e1efeabcbf044f6c44174645605a2732f8b4e6c7f9d8293b1dd009d = $this->env->getExtension("native_profiler");
        $__internal_f7e4b3056e1efeabcbf044f6c44174645605a2732f8b4e6c7f9d8293b1dd009d->enter($__internal_f7e4b3056e1efeabcbf044f6c44174645605a2732f8b4e6c7f9d8293b1dd009d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "pista/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7e4b3056e1efeabcbf044f6c44174645605a2732f8b4e6c7f9d8293b1dd009d->leave($__internal_f7e4b3056e1efeabcbf044f6c44174645605a2732f8b4e6c7f9d8293b1dd009d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_ab507ac38268a3579f05aae91ea0034d623ff9c88e51b1699aca5c6f197a5189 = $this->env->getExtension("native_profiler");
        $__internal_ab507ac38268a3579f05aae91ea0034d623ff9c88e51b1699aca5c6f197a5189->enter($__internal_ab507ac38268a3579f05aae91ea0034d623ff9c88e51b1699aca5c6f197a5189_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>                              

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>numero de pista</th>
                <th>estat</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pista"]) ? $context["pista"] : $this->getContext($context, "pista")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 20
            echo "                <tr>
                    <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "numeroPista", array()), "html", null, true);
            echo "</td>
                    ";
            // line 23
            if (($this->getAttribute($context["p"], "estat", array()) == 0)) {
                // line 24
                echo "                        <td> Pista ocupada </td>
                    ";
            } else {
                // line 26
                echo "                        <td> Pista buida </td>
                    ";
            }
            // line 28
            echo "                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_ab507ac38268a3579f05aae91ea0034d623ff9c88e51b1699aca5c6f197a5189->leave($__internal_ab507ac38268a3579f05aae91ea0034d623ff9c88e51b1699aca5c6f197a5189_prof);

    }

    public function getTemplateName()
    {
        return "pista/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 30,  82 => 28,  78 => 26,  74 => 24,  72 => 23,  68 => 22,  64 => 21,  61 => 20,  57 => 19,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/pista/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>                              */
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>numero de pista</th>*/
/*                 <th>estat</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for p in pista %}*/
/*                 <tr>*/
/*                     <td>{{ p.id }}</td>*/
/*                     <td>{{ p.numeroPista }}</td>*/
/*                     {% if(p.estat==0) %}*/
/*                         <td> Pista ocupada </td>*/
/*                     {% else %}*/
/*                         <td> Pista buida </td>*/
/*                     {% endif %}*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
