<?php

/* base.html.twig */
class __TwigTemplate_0c6cf06ac1e2f1c81f29a0ee0c63d7e816959dd91e6d35f288c72fe6507ca9b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4b0625fdf36687e638d2ba875ccb85b25cd38043765a4bc82210740e590b861 = $this->env->getExtension("native_profiler");
        $__internal_c4b0625fdf36687e638d2ba875ccb85b25cd38043765a4bc82210740e590b861->enter($__internal_c4b0625fdf36687e638d2ba875ccb85b25cd38043765a4bc82210740e590b861_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "    </head>
    <body>
         <nav class=\"navbar navbar-inverse navbar-static-top custom-navbar\" role=\"navigation\">
            <div class=\"container\">
             <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
             </button>
             <div class=\"navbar-header\">
                 <a class=\"navbar-brand\" rel=\"home\" href=\"/home\" title=\"Help\"> Projecte Symfony</a>
             </div>
             <!-- Non-collapsing right-side icons -->
             <ul class=\"nav navbar-nav navbar-right\">
              <li>
                   <a href=\"#\" class=\"fa fa-cog\"></a>
              </li>
              <li>
                   <a href=\"#\" class=\"fa fa-home\"></a>
              </li>
             </ul>
             <!-- the collapsing menu -->
             <div class=\"collapse navbar-collapse navbar-left\" id=\"navbar-collapse-1\">
               <ul class=\"nav navbar-nav\">
                   <li class=\"active\"><a href=\"/usuari\">Usuaris</a></li>
                   <li><a href=\"/monitor\">Monitors</a></li>
                   <li><a href=\"/clase\">Clases</a></li>
                   <li><a href=\"/pista\">Pistes</a></li>
                   <li><a href=\"/reserva\">Reserves</a></li>
               </ul>
             </div>
            <!--/.nav-collapse -->
            </div>
            <!--/.container -->
         </nav>

        <div id=\"content\">
            ";
        // line 55
        $this->displayBlock('body', $context, $blocks);
        // line 56
        echo "        </div>
    </body>
</html>";
        
        $__internal_c4b0625fdf36687e638d2ba875ccb85b25cd38043765a4bc82210740e590b861->leave($__internal_c4b0625fdf36687e638d2ba875ccb85b25cd38043765a4bc82210740e590b861_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_0bab7a5821a63fcc149f21327f73df659980576409601ff70f9299fa0d238916 = $this->env->getExtension("native_profiler");
        $__internal_0bab7a5821a63fcc149f21327f73df659980576409601ff70f9299fa0d238916->enter($__internal_0bab7a5821a63fcc149f21327f73df659980576409601ff70f9299fa0d238916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Product Application";
        
        $__internal_0bab7a5821a63fcc149f21327f73df659980576409601ff70f9299fa0d238916->leave($__internal_0bab7a5821a63fcc149f21327f73df659980576409601ff70f9299fa0d238916_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_da9a304d5d2adba6fa03def01fa204a96d65166d09f63e0dd15baf4ab9019b0d = $this->env->getExtension("native_profiler");
        $__internal_da9a304d5d2adba6fa03def01fa204a96d65166d09f63e0dd15baf4ab9019b0d->enter($__internal_da9a304d5d2adba6fa03def01fa204a96d65166d09f63e0dd15baf4ab9019b0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "         <!-- Latest compiled and minified CSS -->
         <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">

         <!-- jQuery library -->
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>

         <!-- Latest compiled JavaScript -->
         <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        ";
        
        $__internal_da9a304d5d2adba6fa03def01fa204a96d65166d09f63e0dd15baf4ab9019b0d->leave($__internal_da9a304d5d2adba6fa03def01fa204a96d65166d09f63e0dd15baf4ab9019b0d_prof);

    }

    // line 55
    public function block_body($context, array $blocks = array())
    {
        $__internal_7f451787fe2a0d7dd109fe12bc3dfa16f3ce9185004c3454e9766eab07e053e7 = $this->env->getExtension("native_profiler");
        $__internal_7f451787fe2a0d7dd109fe12bc3dfa16f3ce9185004c3454e9766eab07e053e7->enter($__internal_7f451787fe2a0d7dd109fe12bc3dfa16f3ce9185004c3454e9766eab07e053e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_7f451787fe2a0d7dd109fe12bc3dfa16f3ce9185004c3454e9766eab07e053e7->leave($__internal_7f451787fe2a0d7dd109fe12bc3dfa16f3ce9185004c3454e9766eab07e053e7_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  121 => 55,  106 => 8,  100 => 7,  88 => 6,  79 => 56,  77 => 55,  37 => 17,  35 => 7,  31 => 6,  25 => 2,);
    }
}
/* {# app/Resources/views/base.html.twig #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{% block title %}Product Application{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*          <!-- Latest compiled and minified CSS -->*/
/*          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/* */
/*          <!-- jQuery library -->*/
/*          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/* */
/*          <!-- Latest compiled JavaScript -->*/
/*          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>*/
/*         {% endblock %}*/
/*     </head>*/
/*     <body>*/
/*          <nav class="navbar navbar-inverse navbar-static-top custom-navbar" role="navigation">*/
/*             <div class="container">*/
/*              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*              </button>*/
/*              <div class="navbar-header">*/
/*                  <a class="navbar-brand" rel="home" href="/home" title="Help"> Projecte Symfony</a>*/
/*              </div>*/
/*              <!-- Non-collapsing right-side icons -->*/
/*              <ul class="nav navbar-nav navbar-right">*/
/*               <li>*/
/*                    <a href="#" class="fa fa-cog"></a>*/
/*               </li>*/
/*               <li>*/
/*                    <a href="#" class="fa fa-home"></a>*/
/*               </li>*/
/*              </ul>*/
/*              <!-- the collapsing menu -->*/
/*              <div class="collapse navbar-collapse navbar-left" id="navbar-collapse-1">*/
/*                <ul class="nav navbar-nav">*/
/*                    <li class="active"><a href="/usuari">Usuaris</a></li>*/
/*                    <li><a href="/monitor">Monitors</a></li>*/
/*                    <li><a href="/clase">Clases</a></li>*/
/*                    <li><a href="/pista">Pistes</a></li>*/
/*                    <li><a href="/reserva">Reserves</a></li>*/
/*                </ul>*/
/*              </div>*/
/*             <!--/.nav-collapse -->*/
/*             </div>*/
/*             <!--/.container -->*/
/*          </nav>*/
/* */
/*         <div id="content">*/
/*             {% block body %}{% endblock %}*/
/*         </div>*/
/*     </body>*/
/* </html>*/
