<?php

/* base.html.twig */
class __TwigTemplate_b99d9379eed8b0ecd5f0be1b816f95489800cd826cbe4da58d68930149210b25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f916bc8b57c5859f04b146664be44e90a1fa5e288cf1822f21fdff23a8d8848 = $this->env->getExtension("native_profiler");
        $__internal_3f916bc8b57c5859f04b146664be44e90a1fa5e288cf1822f21fdff23a8d8848->enter($__internal_3f916bc8b57c5859f04b146664be44e90a1fa5e288cf1822f21fdff23a8d8848_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "        <style>
            .mainContent{
                margin-left: 20%;
            }
            .table-responsive{
                margin-right: 19%;
            }
          ";
        // line 24
        $this->displayBlock('style', $context, $blocks);
        // line 25
        echo "        </style>
    </head>
    
    <body>

         <nav class=\"navbar navbar-inverse navbar-static-top custom-navbar\" role=\"navigation\">
            <div class=\"container\">
             <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
             </button>
             <div class=\"navbar-header\">
                 <a class=\"navbar-brand\" rel=\"home\" href=\"/\" title=\"Help\"> Projecte Symfony</a>
             </div>
             <!-- Non-collapsing right-side icons -->
             <ul class=\"nav navbar-nav navbar-right\">
              <li>
                   <a href=\"#\" class=\"fa fa-cog\"></a>
              </li>
              <li>
                   <a href=\"#\" class=\"fa fa-home\"></a>
              </li>
             </ul>
             <!-- the collapsing menu -->
             <div class=\"collapse navbar-collapse navbar-left\" id=\"navbar-collapse-1\">
               <ul class=\"nav navbar-nav\">
                   <li><a href=\"/selectAllUsers\">Usuaris</a></li>
                   <li><a href=\"/selectAllMonitors\">Monitors</a></li>
                   <li><a href=\"/selectAllClases\">Clases</a></li>
                   <li><a href=\"/selectAllPistes\">Pistes</a></li>
                   <li><a href=\"/selectAllReserves\">Reserves</a></li>
               </ul>
             </div>
            <!--/.nav-collapse -->
            </div>
            <!--/.container -->
         </nav>

        <div id=\"content\">
            <nav>
                ";
        // line 67
        $this->displayBlock('menu_aside', $context, $blocks);
        // line 68
        echo "            </nav>
            <main>
                <div class=\"mainContent\">
                    ";
        // line 71
        $this->displayBlock('mainContent', $context, $blocks);
        // line 72
        echo "                </div>
            </main>

        </div>

    </body>

</html>";
        
        $__internal_3f916bc8b57c5859f04b146664be44e90a1fa5e288cf1822f21fdff23a8d8848->leave($__internal_3f916bc8b57c5859f04b146664be44e90a1fa5e288cf1822f21fdff23a8d8848_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_7ee407d82f8bfca2eca999262df4e76b02f47429bcf9b3bd61fbc6ed3585eaa3 = $this->env->getExtension("native_profiler");
        $__internal_7ee407d82f8bfca2eca999262df4e76b02f47429bcf9b3bd61fbc6ed3585eaa3->enter($__internal_7ee407d82f8bfca2eca999262df4e76b02f47429bcf9b3bd61fbc6ed3585eaa3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Product Application";
        
        $__internal_7ee407d82f8bfca2eca999262df4e76b02f47429bcf9b3bd61fbc6ed3585eaa3->leave($__internal_7ee407d82f8bfca2eca999262df4e76b02f47429bcf9b3bd61fbc6ed3585eaa3_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d91583c78051f217e28ac627691f0981f8fb6a380cc8918c74296e6325bdf2ff = $this->env->getExtension("native_profiler");
        $__internal_d91583c78051f217e28ac627691f0981f8fb6a380cc8918c74296e6325bdf2ff->enter($__internal_d91583c78051f217e28ac627691f0981f8fb6a380cc8918c74296e6325bdf2ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "         <!-- Latest compiled and minified CSS -->
         <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">

         <!-- jQuery library -->
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>

         <!-- Latest compiled JavaScript -->
         <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        ";
        
        $__internal_d91583c78051f217e28ac627691f0981f8fb6a380cc8918c74296e6325bdf2ff->leave($__internal_d91583c78051f217e28ac627691f0981f8fb6a380cc8918c74296e6325bdf2ff_prof);

    }

    // line 24
    public function block_style($context, array $blocks = array())
    {
        $__internal_022431d2dba61a472ed0e9c9f9b68b51910f88cda9b2a7e4e84ca0782f67a304 = $this->env->getExtension("native_profiler");
        $__internal_022431d2dba61a472ed0e9c9f9b68b51910f88cda9b2a7e4e84ca0782f67a304->enter($__internal_022431d2dba61a472ed0e9c9f9b68b51910f88cda9b2a7e4e84ca0782f67a304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        
        $__internal_022431d2dba61a472ed0e9c9f9b68b51910f88cda9b2a7e4e84ca0782f67a304->leave($__internal_022431d2dba61a472ed0e9c9f9b68b51910f88cda9b2a7e4e84ca0782f67a304_prof);

    }

    // line 67
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_75c0123b018af3e75b4ede5a1171092b16a93909ba0aeb1e8c93f1e0f1fd24ed = $this->env->getExtension("native_profiler");
        $__internal_75c0123b018af3e75b4ede5a1171092b16a93909ba0aeb1e8c93f1e0f1fd24ed->enter($__internal_75c0123b018af3e75b4ede5a1171092b16a93909ba0aeb1e8c93f1e0f1fd24ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        echo " ";
        
        $__internal_75c0123b018af3e75b4ede5a1171092b16a93909ba0aeb1e8c93f1e0f1fd24ed->leave($__internal_75c0123b018af3e75b4ede5a1171092b16a93909ba0aeb1e8c93f1e0f1fd24ed_prof);

    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_e7e7abcaeee7487e4be8d1ce76d2b13420211918a5189ef21c9c445aaad9dec4 = $this->env->getExtension("native_profiler");
        $__internal_e7e7abcaeee7487e4be8d1ce76d2b13420211918a5189ef21c9c445aaad9dec4->enter($__internal_e7e7abcaeee7487e4be8d1ce76d2b13420211918a5189ef21c9c445aaad9dec4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        
        $__internal_e7e7abcaeee7487e4be8d1ce76d2b13420211918a5189ef21c9c445aaad9dec4->leave($__internal_e7e7abcaeee7487e4be8d1ce76d2b13420211918a5189ef21c9c445aaad9dec4_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  173 => 71,  161 => 67,  150 => 24,  135 => 8,  129 => 7,  117 => 6,  103 => 72,  101 => 71,  96 => 68,  94 => 67,  50 => 25,  48 => 24,  39 => 17,  37 => 7,  33 => 6,  27 => 2,);
    }
}
/* {# app/Resources/views/base.html.twig #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{% block title %}Product Application{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*          <!-- Latest compiled and minified CSS -->*/
/*          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/* */
/*          <!-- jQuery library -->*/
/*          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/* */
/*          <!-- Latest compiled JavaScript -->*/
/*          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>*/
/*         {% endblock %}*/
/*         <style>*/
/*             .mainContent{*/
/*                 margin-left: 20%;*/
/*             }*/
/*             .table-responsive{*/
/*                 margin-right: 19%;*/
/*             }*/
/*           {% block style%}{% endblock %}*/
/*         </style>*/
/*     </head>*/
/*     */
/*     <body>*/
/* */
/*          <nav class="navbar navbar-inverse navbar-static-top custom-navbar" role="navigation">*/
/*             <div class="container">*/
/*              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*              </button>*/
/*              <div class="navbar-header">*/
/*                  <a class="navbar-brand" rel="home" href="/" title="Help"> Projecte Symfony</a>*/
/*              </div>*/
/*              <!-- Non-collapsing right-side icons -->*/
/*              <ul class="nav navbar-nav navbar-right">*/
/*               <li>*/
/*                    <a href="#" class="fa fa-cog"></a>*/
/*               </li>*/
/*               <li>*/
/*                    <a href="#" class="fa fa-home"></a>*/
/*               </li>*/
/*              </ul>*/
/*              <!-- the collapsing menu -->*/
/*              <div class="collapse navbar-collapse navbar-left" id="navbar-collapse-1">*/
/*                <ul class="nav navbar-nav">*/
/*                    <li><a href="/selectAllUsers">Usuaris</a></li>*/
/*                    <li><a href="/selectAllMonitors">Monitors</a></li>*/
/*                    <li><a href="/selectAllClases">Clases</a></li>*/
/*                    <li><a href="/selectAllPistes">Pistes</a></li>*/
/*                    <li><a href="/selectAllReserves">Reserves</a></li>*/
/*                </ul>*/
/*              </div>*/
/*             <!--/.nav-collapse -->*/
/*             </div>*/
/*             <!--/.container -->*/
/*          </nav>*/
/* */
/*         <div id="content">*/
/*             <nav>*/
/*                 {% block menu_aside %} {% endblock %}*/
/*             </nav>*/
/*             <main>*/
/*                 <div class="mainContent">*/
/*                     {% block mainContent %}{% endblock %}*/
/*                 </div>*/
/*             </main>*/
/* */
/*         </div>*/
/* */
/*     </body>*/
/* */
/* </html>*/
