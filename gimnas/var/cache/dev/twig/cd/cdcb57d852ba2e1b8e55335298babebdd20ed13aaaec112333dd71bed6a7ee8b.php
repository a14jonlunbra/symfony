<?php

/* clase/content.html.twig */
class __TwigTemplate_27442a9a48a78ed50cd57cd54c45908a97f5a5b96fee7831a2a2798dfcae4938 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "clase/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_83468bf5a09b4d9564c903386598bbd9afeb0ea8c4fe0df9d1422ac0dd67c8f1 = $this->env->getExtension("native_profiler");
        $__internal_83468bf5a09b4d9564c903386598bbd9afeb0ea8c4fe0df9d1422ac0dd67c8f1->enter($__internal_83468bf5a09b4d9564c903386598bbd9afeb0ea8c4fe0df9d1422ac0dd67c8f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clase/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_83468bf5a09b4d9564c903386598bbd9afeb0ea8c4fe0df9d1422ac0dd67c8f1->leave($__internal_83468bf5a09b4d9564c903386598bbd9afeb0ea8c4fe0df9d1422ac0dd67c8f1_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_ca8676cf9ddc35d377cab48d9f62b3afd37c6f9e6bab5226d2b8457ee51987d7 = $this->env->getExtension("native_profiler");
        $__internal_ca8676cf9ddc35d377cab48d9f62b3afd37c6f9e6bab5226d2b8457ee51987d7->enter($__internal_ca8676cf9ddc35d377cab48d9f62b3afd37c6f9e6bab5226d2b8457ee51987d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_ca8676cf9ddc35d377cab48d9f62b3afd37c6f9e6bab5226d2b8457ee51987d7->leave($__internal_ca8676cf9ddc35d377cab48d9f62b3afd37c6f9e6bab5226d2b8457ee51987d7_prof);

    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_6e3ee248b21c71b8e62064f435f4c928c31296ae8124d91dd3e5afe878ddd24d = $this->env->getExtension("native_profiler");
        $__internal_6e3ee248b21c71b8e62064f435f4c928c31296ae8124d91dd3e5afe878ddd24d->enter($__internal_6e3ee248b21c71b8e62064f435f4c928c31296ae8124d91dd3e5afe878ddd24d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Clases</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllClases\">Llistar totes les clases</a>
                </li>
                <li>
                    <a href=\"/selectClase\">Seleccionar una clase</a>
                </li>
                <li>
                    <a href=\"/insertClase\">Insertar una nova clase</a></li>
                <li><a href=\"/updateClase\">Modificar una clase</a></li>
                <li>
                    <a href=\"/removeClase\">Eliminar una clase</a>
                </li>
";
        
        $__internal_6e3ee248b21c71b8e62064f435f4c928c31296ae8124d91dd3e5afe878ddd24d->leave($__internal_6e3ee248b21c71b8e62064f435f4c928c31296ae8124d91dd3e5afe878ddd24d_prof);

    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_9cc6dad3adc017138eae5e02e492de5f989fae47dc2fe4e6c651b3d744305b1c = $this->env->getExtension("native_profiler");
        $__internal_9cc6dad3adc017138eae5e02e492de5f989fae47dc2fe4e6c651b3d744305b1c->enter($__internal_9cc6dad3adc017138eae5e02e492de5f989fae47dc2fe4e6c651b3d744305b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 72
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>monitor</th>
                <th>nom</th>
                <th>descripcio</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clase"]) ? $context["clase"] : $this->getContext($context, "clase")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 88
            echo "                <tr>
                  <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "monitor", array()), "getNom", array(), "method"), "html", null, true);
            echo "</td>
                  <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nom", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "descripcio", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 94
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_9cc6dad3adc017138eae5e02e492de5f989fae47dc2fe4e6c651b3d744305b1c->leave($__internal_9cc6dad3adc017138eae5e02e492de5f989fae47dc2fe4e6c651b3d744305b1c_prof);

    }

    public function getTemplateName()
    {
        return "clase/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 94,  162 => 91,  158 => 90,  154 => 89,  151 => 88,  147 => 87,  130 => 72,  124 => 71,  92 => 44,  86 => 43,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/clase/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Clases</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllClases">Llistar totes les clases</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectClase">Seleccionar una clase</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertClase">Insertar una nova clase</a></li>*/
/*                 <li><a href="/updateClase">Modificar una clase</a></li>*/
/*                 <li>*/
/*                     <a href="/removeClase">Eliminar una clase</a>*/
/*                 </li>*/
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>monitor</th>*/
/*                 <th>nom</th>*/
/*                 <th>descripcio</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for c in clase %}*/
/*                 <tr>*/
/*                   <td>{{ c.monitor.getNom() }}</td>*/
/*                   <td>{{ c.nom }}</td>*/
/*                   <td>{{ c.descripcio }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
