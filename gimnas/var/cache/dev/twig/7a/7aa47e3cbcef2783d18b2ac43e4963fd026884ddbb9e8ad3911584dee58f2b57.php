<?php

/* monitor/content.html.twig */
class __TwigTemplate_fbd4169a66a73dbd7c7db97caf6b45a0675a668e7edf81294a7dbebb833ff0bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "monitor/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_86d88e6dc3c629236a88458d53b9d094607c5460412e5c708683e091c2c9599f = $this->env->getExtension("native_profiler");
        $__internal_86d88e6dc3c629236a88458d53b9d094607c5460412e5c708683e091c2c9599f->enter($__internal_86d88e6dc3c629236a88458d53b9d094607c5460412e5c708683e091c2c9599f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "monitor/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_86d88e6dc3c629236a88458d53b9d094607c5460412e5c708683e091c2c9599f->leave($__internal_86d88e6dc3c629236a88458d53b9d094607c5460412e5c708683e091c2c9599f_prof);

    }

    // line 3
    public function block_style($context, array $blocks = array())
    {
        $__internal_6526f4fc061174e015499de7bc48ae54857130664804c611132b3166fda10ec8 = $this->env->getExtension("native_profiler");
        $__internal_6526f4fc061174e015499de7bc48ae54857130664804c611132b3166fda10ec8->enter($__internal_6526f4fc061174e015499de7bc48ae54857130664804c611132b3166fda10ec8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 4
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_6526f4fc061174e015499de7bc48ae54857130664804c611132b3166fda10ec8->leave($__internal_6526f4fc061174e015499de7bc48ae54857130664804c611132b3166fda10ec8_prof);

    }

    // line 42
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_7ebd2a260cea9687ff16e1889610a168c323954ca66a64fdf4bef6a51b87766f = $this->env->getExtension("native_profiler");
        $__internal_7ebd2a260cea9687ff16e1889610a168c323954ca66a64fdf4bef6a51b87766f->enter($__internal_7ebd2a260cea9687ff16e1889610a168c323954ca66a64fdf4bef6a51b87766f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 43
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Monitors</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllMonitors\">Llistar tots els monitors</a>
                </li>
                <li>
                    <a href=\"/selectMonitor\">Seleccionar un monitor</a>
                </li>
                <li>
                    <a href=\"/insertMonitor\">Insertar nou monitor</a></li>

                <li><a href=\"/updateMonitor\">Modificar un monitor</a></li>
                <li>
                    <a href=\"/removeMonitor\">Eliminar un monitor</a>
                </li>

";
        
        $__internal_7ebd2a260cea9687ff16e1889610a168c323954ca66a64fdf4bef6a51b87766f->leave($__internal_7ebd2a260cea9687ff16e1889610a168c323954ca66a64fdf4bef6a51b87766f_prof);

    }

    // line 72
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_4d2597fd965575bb6d55306a105d507046291a325b376d3fe80eadd35ae00011 = $this->env->getExtension("native_profiler");
        $__internal_4d2597fd965575bb6d55306a105d507046291a325b376d3fe80eadd35ae00011->enter($__internal_4d2597fd965575bb6d55306a105d507046291a325b376d3fe80eadd35ae00011_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 73
        echo "
  <h2>Llista d'usuaris inscrits</h2>
    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
               <th><a href=\"/selectAllMonitors/1\">id</a></th>
                <th><a href=\"/selectAllMonitors/2\">nom</a></th>
                <th><a href=\"/selectAllMonitors/3\">cognom1</a></th>
                <th><a href=\"/selectAllMonitors/4\">cognom2</a></th>
                <th><a href=\"/selectAllMonitors/5\">sou</a></th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 87
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["monitor"]) ? $context["monitor"] : $this->getContext($context, "monitor")));
        foreach ($context['_seq'] as $context["_key"] => $context["moni"]) {
            // line 88
            echo "                <tr>
                    <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["moni"], "sou", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['moni'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "            </tbody>
        </table>
</div>

";
        
        $__internal_4d2597fd965575bb6d55306a105d507046291a325b376d3fe80eadd35ae00011->leave($__internal_4d2597fd965575bb6d55306a105d507046291a325b376d3fe80eadd35ae00011_prof);

    }

    public function getTemplateName()
    {
        return "monitor/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 96,  171 => 93,  167 => 92,  163 => 91,  159 => 90,  155 => 89,  152 => 88,  148 => 87,  132 => 73,  126 => 72,  92 => 43,  86 => 42,  42 => 4,  36 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/monitor/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Monitors</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllMonitors">Llistar tots els monitors</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectMonitor">Seleccionar un monitor</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertMonitor">Insertar nou monitor</a></li>*/
/* */
/*                 <li><a href="/updateMonitor">Modificar un monitor</a></li>*/
/*                 <li>*/
/*                     <a href="/removeMonitor">Eliminar un monitor</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                <th><a href="/selectAllMonitors/1">id</a></th>*/
/*                 <th><a href="/selectAllMonitors/2">nom</a></th>*/
/*                 <th><a href="/selectAllMonitors/3">cognom1</a></th>*/
/*                 <th><a href="/selectAllMonitors/4">cognom2</a></th>*/
/*                 <th><a href="/selectAllMonitors/5">sou</a></th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for moni in monitor %}*/
/*                 <tr>*/
/*                     <td>{{ moni.id }}</td>*/
/*                     <td>{{ moni.nom }}</td>*/
/*                     <td>{{ moni.cognom1 }}</td>*/
/*                     <td>{{ moni.cognom2 }}</td>*/
/*                     <td>{{ moni.sou }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/* </div>*/
/* */
/* {% endblock %}*/
