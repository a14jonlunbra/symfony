<?php

/* reserva/content.html.twig */
class __TwigTemplate_c5fe9ea7676518f8f2a24108b2ff54c0ca1ce48d905f007df59aa1f31c8c61f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "reserva/content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e2f171a2a597924492cd29a9e402e18cf8748213d397dd5fe0da4fd886517ac = $this->env->getExtension("native_profiler");
        $__internal_7e2f171a2a597924492cd29a9e402e18cf8748213d397dd5fe0da4fd886517ac->enter($__internal_7e2f171a2a597924492cd29a9e402e18cf8748213d397dd5fe0da4fd886517ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reserva/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7e2f171a2a597924492cd29a9e402e18cf8748213d397dd5fe0da4fd886517ac->leave($__internal_7e2f171a2a597924492cd29a9e402e18cf8748213d397dd5fe0da4fd886517ac_prof);

    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        $__internal_966b947d74a64e270c628b1715c0cdef144691d9d6f6c8ecaae75298c6652bca = $this->env->getExtension("native_profiler");
        $__internal_966b947d74a64e270c628b1715c0cdef144691d9d6f6c8ecaae75298c6652bca->enter($__internal_966b947d74a64e270c628b1715c0cdef144691d9d6f6c8ecaae75298c6652bca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "style"));

        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
        
        $__internal_966b947d74a64e270c628b1715c0cdef144691d9d6f6c8ecaae75298c6652bca->leave($__internal_966b947d74a64e270c628b1715c0cdef144691d9d6f6c8ecaae75298c6652bca_prof);

    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        $__internal_59b44996001351da4a11e0ba5cfe76d2eb464dc106156e3934a9f81e185af366 = $this->env->getExtension("native_profiler");
        $__internal_59b44996001351da4a11e0ba5cfe76d2eb464dc106156e3934a9f81e185af366->enter($__internal_59b44996001351da4a11e0ba5cfe76d2eb464dc106156e3934a9f81e185af366_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu_aside"));

        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Reserves</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllReserves\">Llistar totes les reserves</a>
                </li>
                <li>
                    <a href=\"/selectReserva\">Buscar una reserva</a>
                <li>
                    <a href=\"/insertReserva\">Fer una nova reserva</a>
                </li>
                <li><a href=\"/removeReserva\">Cancel·lar reserva</a></li>

";
        
        $__internal_59b44996001351da4a11e0ba5cfe76d2eb464dc106156e3934a9f81e185af366->leave($__internal_59b44996001351da4a11e0ba5cfe76d2eb464dc106156e3934a9f81e185af366_prof);

    }

    // line 69
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_8ca07e77e8031eb668912b5c54044aed36d1753a9d042c7d25d3862e958dff50 = $this->env->getExtension("native_profiler");
        $__internal_8ca07e77e8031eb668912b5c54044aed36d1753a9d042c7d25d3862e958dff50->enter($__internal_8ca07e77e8031eb668912b5c54044aed36d1753a9d042c7d25d3862e958dff50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 70
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>pista_id</th>
                <th>usuari_id</th>
                <th>data</th>
                <th>hora</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 86
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reserva"]) ? $context["reserva"] : $this->getContext($context, "reserva")));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 87
            echo "                 <tr>
                    <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "pista", array()), "getNumeroPista", array(), "method"), "html", null, true);
            echo "</td>
                    <td>";
            // line 90
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "usuari", array()), "getCognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["res"], "data", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["res"], "hora", array()), "H:i:s"), "html", null, true);
            echo "</td>
                 </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_8ca07e77e8031eb668912b5c54044aed36d1753a9d042c7d25d3862e958dff50->leave($__internal_8ca07e77e8031eb668912b5c54044aed36d1753a9d042c7d25d3862e958dff50_prof);

    }

    public function getTemplateName()
    {
        return "reserva/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 95,  169 => 92,  165 => 91,  161 => 90,  157 => 89,  153 => 88,  150 => 87,  146 => 86,  128 => 70,  122 => 69,  92 => 44,  86 => 43,  42 => 5,  36 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Reserves</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllReserves">Llistar totes les reserves</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectReserva">Buscar una reserva</a>*/
/*                 <li>*/
/*                     <a href="/insertReserva">Fer una nova reserva</a>*/
/*                 </li>*/
/*                 <li><a href="/removeReserva">Cancel·lar reserva</a></li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>pista_id</th>*/
/*                 <th>usuari_id</th>*/
/*                 <th>data</th>*/
/*                 <th>hora</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for res in reserva %}*/
/*                  <tr>*/
/*                     <td>{{ res.id }}</td>*/
/*                     <td>{{ res.pista.getNumeroPista() }}</td>*/
/*                     <td>{{ res.usuari.getCognom1 }}</td>*/
/*                     <td>{{ res.data|date("m/d/Y")}}</td>*/
/*                     <td>{{ res.hora |date("H:i:s")}}</td>*/
/*                  </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
