<?php

/* principal/monitor.html.twig */
class __TwigTemplate_3494ec42836eb40ecec1ed4394b9ab4fc0dc7914ae092bd99e13650d7eb0497d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/monitor.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbd524a9157a5b43d36caf94389619ee44944bcf3bc9ee11674222ad7f5b8313 = $this->env->getExtension("native_profiler");
        $__internal_fbd524a9157a5b43d36caf94389619ee44944bcf3bc9ee11674222ad7f5b8313->enter($__internal_fbd524a9157a5b43d36caf94389619ee44944bcf3bc9ee11674222ad7f5b8313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/monitor.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fbd524a9157a5b43d36caf94389619ee44944bcf3bc9ee11674222ad7f5b8313->leave($__internal_fbd524a9157a5b43d36caf94389619ee44944bcf3bc9ee11674222ad7f5b8313_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1172a62ccb5abb18d73cc21b6a69ec3b211649c33a4ce2696199a4a1bc084f53 = $this->env->getExtension("native_profiler");
        $__internal_1172a62ccb5abb18d73cc21b6a69ec3b211649c33a4ce2696199a4a1bc084f53->enter($__internal_1172a62ccb5abb18d73cc21b6a69ec3b211649c33a4ce2696199a4a1bc084f53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Monitor</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>

";
        
        $__internal_1172a62ccb5abb18d73cc21b6a69ec3b211649c33a4ce2696199a4a1bc084f53->leave($__internal_1172a62ccb5abb18d73cc21b6a69ec3b211649c33a4ce2696199a4a1bc084f53_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_585e71b91c1aabec55f7216459099e6cfaaf9bc6a54f5209db9999310a9a518a = $this->env->getExtension("native_profiler");
        $__internal_585e71b91c1aabec55f7216459099e6cfaaf9bc6a54f5209db9999310a9a518a->enter($__internal_585e71b91c1aabec55f7216459099e6cfaaf9bc6a54f5209db9999310a9a518a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllMonitors\">Llistar tots els monitors</a><br>
                <a href=\"/selectAllCognom1\">Llistar tots els monitors per el primer cognom</a><br>
                <a href=\"/insertMonitor\">Insertar nou monitor</a><br>
                <a href=\"/selectMonitor\">Seleccionar un monitor</a><br>
                <a href=\"/updateMonitor\">Modificar un monitor</a><br>
                <a href=\"/removeMonitor\">Eliminar un monitor</a><br>
                <br> <hr> <br>
            ";
        
        $__internal_585e71b91c1aabec55f7216459099e6cfaaf9bc6a54f5209db9999310a9a518a->leave($__internal_585e71b91c1aabec55f7216459099e6cfaaf9bc6a54f5209db9999310a9a518a_prof);

    }

    public function getTemplateName()
    {
        return "principal/monitor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 7,  56 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/monitor.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Monitor</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllMonitors">Llistar tots els monitors</a><br>*/
/*                 <a href="/selectAllCognom1">Llistar tots els monitors per el primer cognom</a><br>*/
/*                 <a href="/insertMonitor">Insertar nou monitor</a><br>*/
/*                 <a href="/selectMonitor">Seleccionar un monitor</a><br>*/
/*                 <a href="/updateMonitor">Modificar un monitor</a><br>*/
/*                 <a href="/removeMonitor">Eliminar un monitor</a><br>*/
/*                 <br> <hr> <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* */
/* {% endblock %}*/
