<?php

/* usuari/content.html.twig */
class __TwigTemplate_28a1d0064cfdf9693af186d1c61d10246efb0b6124201d7ab87ce560662b06fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "usuari/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46b8d1629eae552210da97741475abde3f101d17154ff9ebc895c9cfa305d835 = $this->env->getExtension("native_profiler");
        $__internal_46b8d1629eae552210da97741475abde3f101d17154ff9ebc895c9cfa305d835->enter($__internal_46b8d1629eae552210da97741475abde3f101d17154ff9ebc895c9cfa305d835_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "usuari/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_46b8d1629eae552210da97741475abde3f101d17154ff9ebc895c9cfa305d835->leave($__internal_46b8d1629eae552210da97741475abde3f101d17154ff9ebc895c9cfa305d835_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_58ffb149342beb773a834fc185dd5f4d696e53ae389acef38d70458b5261c97e = $this->env->getExtension("native_profiler");
        $__internal_58ffb149342beb773a834fc185dd5f4d696e53ae389acef38d70458b5261c97e->enter($__internal_58ffb149342beb773a834fc185dd5f4d696e53ae389acef38d70458b5261c97e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>                              

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>nom</th>
                <th>cognom1</th>
                <th>cognom2</th>
                <th>edat</th>
                <th>inscripcio</th>
                <th>quota</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuari"]) ? $context["usuari"] : $this->getContext($context, "usuari")));
        foreach ($context['_seq'] as $context["_key"] => $context["usu"]) {
            // line 24
            echo "                <tr>
                    <td>";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "edat", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usu"], "inscripcio", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "quota", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_58ffb149342beb773a834fc185dd5f4d696e53ae389acef38d70458b5261c97e->leave($__internal_58ffb149342beb773a834fc185dd5f4d696e53ae389acef38d70458b5261c97e_prof);

    }

    public function getTemplateName()
    {
        return "usuari/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 34,  92 => 31,  88 => 30,  84 => 29,  80 => 28,  76 => 27,  72 => 26,  68 => 25,  65 => 24,  61 => 23,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>                              */
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>nom</th>*/
/*                 <th>cognom1</th>*/
/*                 <th>cognom2</th>*/
/*                 <th>edat</th>*/
/*                 <th>inscripcio</th>*/
/*                 <th>quota</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for usu in usuari %}*/
/*                 <tr>*/
/*                     <td>{{ usu.id }}</td>*/
/*                     <td>{{ usu.nom }}</td>*/
/*                     <td>{{ usu.cognom1 }}</td>*/
/*                     <td>{{ usu.cognom2 }}</td>*/
/*                     <td>{{ usu.edat }}</td>*/
/*                     <td>{{ usu.inscripcio|date("m/d/Y")}}</td>*/
/*                     <td>{{ usu.quota }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
