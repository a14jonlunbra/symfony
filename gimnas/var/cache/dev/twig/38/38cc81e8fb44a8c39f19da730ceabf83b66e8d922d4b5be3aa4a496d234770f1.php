<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_1961850f4c27a68827dbf788ed226e1f2406b15743a114c0e77e1eb20a1987db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54cf4027db217c098e88eaa86d07077e08d85528eb9cd1f164f8bee4e7f694b4 = $this->env->getExtension("native_profiler");
        $__internal_54cf4027db217c098e88eaa86d07077e08d85528eb9cd1f164f8bee4e7f694b4->enter($__internal_54cf4027db217c098e88eaa86d07077e08d85528eb9cd1f164f8bee4e7f694b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_54cf4027db217c098e88eaa86d07077e08d85528eb9cd1f164f8bee4e7f694b4->leave($__internal_54cf4027db217c098e88eaa86d07077e08d85528eb9cd1f164f8bee4e7f694b4_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6033e00a648c3a06fca0406e4bc80211e8e042881488880de6343813a12955dd = $this->env->getExtension("native_profiler");
        $__internal_6033e00a648c3a06fca0406e4bc80211e8e042881488880de6343813a12955dd->enter($__internal_6033e00a648c3a06fca0406e4bc80211e8e042881488880de6343813a12955dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_6033e00a648c3a06fca0406e4bc80211e8e042881488880de6343813a12955dd->leave($__internal_6033e00a648c3a06fca0406e4bc80211e8e042881488880de6343813a12955dd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_82ad8a9b2be2e419cd06a3a7280411948b1c206f237072741d7da8dfd59c2cc4 = $this->env->getExtension("native_profiler");
        $__internal_82ad8a9b2be2e419cd06a3a7280411948b1c206f237072741d7da8dfd59c2cc4->enter($__internal_82ad8a9b2be2e419cd06a3a7280411948b1c206f237072741d7da8dfd59c2cc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_82ad8a9b2be2e419cd06a3a7280411948b1c206f237072741d7da8dfd59c2cc4->leave($__internal_82ad8a9b2be2e419cd06a3a7280411948b1c206f237072741d7da8dfd59c2cc4_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_dce20e3252ec5db1d5e8a0d61e8e1e228d90f2a66edb87df2a471d2a5b1f4119 = $this->env->getExtension("native_profiler");
        $__internal_dce20e3252ec5db1d5e8a0d61e8e1e228d90f2a66edb87df2a471d2a5b1f4119->enter($__internal_dce20e3252ec5db1d5e8a0d61e8e1e228d90f2a66edb87df2a471d2a5b1f4119_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_dce20e3252ec5db1d5e8a0d61e8e1e228d90f2a66edb87df2a471d2a5b1f4119->leave($__internal_dce20e3252ec5db1d5e8a0d61e8e1e228d90f2a66edb87df2a471d2a5b1f4119_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
