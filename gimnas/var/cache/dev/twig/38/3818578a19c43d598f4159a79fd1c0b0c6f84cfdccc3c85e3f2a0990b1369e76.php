<?php

/* principal/usuari.html.twig */
class __TwigTemplate_32e0e0b2ec85e547b1dffe2e893ca66939bfc51ff469a2681bb4ac594a1d933f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "principal/usuari.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'upperbar1' => array($this, 'block_upperbar1'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b888210edc7b84119cca564904faf3e4e84436d8208c2d0cd965962fd1e615b = $this->env->getExtension("native_profiler");
        $__internal_8b888210edc7b84119cca564904faf3e4e84436d8208c2d0cd965962fd1e615b->enter($__internal_8b888210edc7b84119cca564904faf3e4e84436d8208c2d0cd965962fd1e615b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "principal/usuari.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8b888210edc7b84119cca564904faf3e4e84436d8208c2d0cd965962fd1e615b->leave($__internal_8b888210edc7b84119cca564904faf3e4e84436d8208c2d0cd965962fd1e615b_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_cbeead6d3d53781d345265dc7a1abd0a71f613a6bea2292358082c659c633f5a = $this->env->getExtension("native_profiler");
        $__internal_cbeead6d3d53781d345265dc7a1abd0a71f613a6bea2292358082c659c633f5a->enter($__internal_cbeead6d3d53781d345265dc7a1abd0a71f613a6bea2292358082c659c633f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<h3>Usuari</h3>
        <div id=\"upperbar1\">
            ";
        // line 6
        $this->displayBlock('upperbar1', $context, $blocks);
        // line 15
        echo "        </div>
";
        
        $__internal_cbeead6d3d53781d345265dc7a1abd0a71f613a6bea2292358082c659c633f5a->leave($__internal_cbeead6d3d53781d345265dc7a1abd0a71f613a6bea2292358082c659c633f5a_prof);

    }

    // line 6
    public function block_upperbar1($context, array $blocks = array())
    {
        $__internal_d30a64b69e8d53311483ef5f7097cb60aa46286a0dfb9095e1b2281020304bd6 = $this->env->getExtension("native_profiler");
        $__internal_d30a64b69e8d53311483ef5f7097cb60aa46286a0dfb9095e1b2281020304bd6->enter($__internal_d30a64b69e8d53311483ef5f7097cb60aa46286a0dfb9095e1b2281020304bd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "upperbar1"));

        // line 7
        echo "                <a href=\"/selectAllUsers\">Llistar tots els usuaris</a><br>
                <a href=\"/selectAllDate\">Llistar tots els usuaris per data d'inscripcio</a><br>
                <a href=\"/insertUsuari\">Insertar nou usuari</a><br>
                <a href=\"/selectUsuari\">Seleccionar un usuari</a><br>
                <a href=\"/updateUsuari\">Modificar un usuari</a><br>
                <a href=\"/removeUsuari\">Eliminar un usuari</a>
                <br>
            ";
        
        $__internal_d30a64b69e8d53311483ef5f7097cb60aa46286a0dfb9095e1b2281020304bd6->leave($__internal_d30a64b69e8d53311483ef5f7097cb60aa46286a0dfb9095e1b2281020304bd6_prof);

    }

    public function getTemplateName()
    {
        return "principal/usuari.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  55 => 6,  47 => 15,  45 => 6,  41 => 4,  35 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/principal/usuari.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* <h3>Usuari</h3>*/
/*         <div id="upperbar1">*/
/*             {% block upperbar1 %}*/
/*                 <a href="/selectAllUsers">Llistar tots els usuaris</a><br>*/
/*                 <a href="/selectAllDate">Llistar tots els usuaris per data d'inscripcio</a><br>*/
/*                 <a href="/insertUsuari">Insertar nou usuari</a><br>*/
/*                 <a href="/selectUsuari">Seleccionar un usuari</a><br>*/
/*                 <a href="/updateUsuari">Modificar un usuari</a><br>*/
/*                 <a href="/removeUsuari">Eliminar un usuari</a>*/
/*                 <br>*/
/*             {% endblock %}*/
/*         </div>*/
/* {% endblock %}*/
