<?php

/* clase/content.html.twig */
class __TwigTemplate_061c1a049d87cb65f37afc1284212f74e0f9baf4361a3610b795cdd224dbcb8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "clase/content.html.twig", 2);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d66f3234e8a8e0f0c6f7dc692536071781bdb73e965b498746aa9a42125d67d = $this->env->getExtension("native_profiler");
        $__internal_6d66f3234e8a8e0f0c6f7dc692536071781bdb73e965b498746aa9a42125d67d->enter($__internal_6d66f3234e8a8e0f0c6f7dc692536071781bdb73e965b498746aa9a42125d67d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "clase/content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d66f3234e8a8e0f0c6f7dc692536071781bdb73e965b498746aa9a42125d67d->leave($__internal_6d66f3234e8a8e0f0c6f7dc692536071781bdb73e965b498746aa9a42125d67d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e860a929f50baa10a6221b541042039b954c985964ca2416654a948e30bc7a92 = $this->env->getExtension("native_profiler");
        $__internal_e860a929f50baa10a6221b541042039b954c985964ca2416654a948e30bc7a92->enter($__internal_e860a929f50baa10a6221b541042039b954c985964ca2416654a948e30bc7a92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>
  <p>Usuaris</p>                              

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>monitor</th>
                <th>nom</th>
                <th>descripcio</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clase"]) ? $context["clase"] : $this->getContext($context, "clase")));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 20
            echo "                <tr>
                  <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "monitor", array()), "getNom", array(), "method"), "html", null, true);
            echo "</td>
                  <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nom", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "descripcio", array()), "html", null, true);
            echo "</td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "            </tbody>
        </table>
    </div> 
</div>

";
        
        $__internal_e860a929f50baa10a6221b541042039b954c985964ca2416654a948e30bc7a92->leave($__internal_e860a929f50baa10a6221b541042039b954c985964ca2416654a948e30bc7a92_prof);

    }

    public function getTemplateName()
    {
        return "clase/content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 26,  72 => 23,  68 => 22,  64 => 21,  61 => 20,  57 => 19,  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/clase/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block body %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/*   <p>Usuaris</p>                              */
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>monitor</th>*/
/*                 <th>nom</th>*/
/*                 <th>descripcio</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for c in clase %}*/
/*                 <tr>*/
/*                   <td>{{ c.monitor.getNom() }}</td>*/
/*                   <td>{{ c.nom }}</td>*/
/*                   <td>{{ c.descripcio }}</td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
