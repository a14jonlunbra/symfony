<?php

/* default/message.html.twig */
class __TwigTemplate_9fe629f4178ba986b37f8eff95e142141028811ccf1335e3a938e79cb90f4a4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", "default/message.html.twig", 2);
        $this->blocks = array(
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7637fb1ae84be00657ff326e3f113204729aeff47cd4020580e18f8f2e27cbf = $this->env->getExtension("native_profiler");
        $__internal_c7637fb1ae84be00657ff326e3f113204729aeff47cd4020580e18f8f2e27cbf->enter($__internal_c7637fb1ae84be00657ff326e3f113204729aeff47cd4020580e18f8f2e27cbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/message.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c7637fb1ae84be00657ff326e3f113204729aeff47cd4020580e18f8f2e27cbf->leave($__internal_c7637fb1ae84be00657ff326e3f113204729aeff47cd4020580e18f8f2e27cbf_prof);

    }

    // line 3
    public function block_mainContent($context, array $blocks = array())
    {
        $__internal_ba5e3b8dc4093e1e9fe57d283dd6aa30022c49bf319f8eb2ed336ada50608f52 = $this->env->getExtension("native_profiler");
        $__internal_ba5e3b8dc4093e1e9fe57d283dd6aa30022c49bf319f8eb2ed336ada50608f52->enter($__internal_ba5e3b8dc4093e1e9fe57d283dd6aa30022c49bf319f8eb2ed336ada50608f52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "mainContent"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
        echo "
";
        
        $__internal_ba5e3b8dc4093e1e9fe57d283dd6aa30022c49bf319f8eb2ed336ada50608f52->leave($__internal_ba5e3b8dc4093e1e9fe57d283dd6aa30022c49bf319f8eb2ed336ada50608f52_prof);

    }

    public function getTemplateName()
    {
        return "default/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 2,);
    }
}
/* {# app/Resources/views/default/message.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* {% block mainContent %}*/
/*     {{ message }}*/
/* {% endblock %}*/
