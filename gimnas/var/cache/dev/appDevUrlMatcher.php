<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // insertClase
        if ($pathinfo === '/insertClase') {
            return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::insertClaseAction',  '_route' => 'insertClase',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectClase
            if ($pathinfo === '/selectClase') {
                return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::selectClaseAction',  '_route' => 'selectClase',);
            }

            // selectAllClases
            if ($pathinfo === '/selectAllClases') {
                return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::selectAllClasesAction',  '_route' => 'selectAllClases',);
            }

        }

        // updateClase
        if ($pathinfo === '/updateClase') {
            return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::updateClaseAction',  '_route' => 'updateClase',);
        }

        // editarClase
        if (0 === strpos($pathinfo, '/editarClase') && preg_match('#^/editarClase/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarClase')), array (  '_controller' => 'AppBundle\\Controller\\ClaseController::editarUsuariAction',));
        }

        // removeClase
        if ($pathinfo === '/removeClase') {
            return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::removeClaseAction',  '_route' => 'removeClase',);
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // insertMonitor
        if ($pathinfo === '/insertMonitor') {
            return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::insertMonitorAction',  '_route' => 'insertMonitor',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectAllCognom1
            if ($pathinfo === '/selectAllCognom1') {
                return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::selectAllCognom1Action',  '_route' => 'selectAllCognom1',);
            }

            // selectMonitor
            if ($pathinfo === '/selectMonitor') {
                return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::selectMonitorAction',  '_route' => 'selectMonitor',);
            }

            // selectAllMonitors
            if (0 === strpos($pathinfo, '/selectAllMonitors') && preg_match('#^/selectAllMonitors(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'selectAllMonitors')), array (  'id' => 0,  '_controller' => 'AppBundle\\Controller\\MonitorController::selectAllMonitorsAction',));
            }

        }

        // updateMonitor
        if ($pathinfo === '/updateMonitor') {
            return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::updateMonitorAction',  '_route' => 'updateMonitor',);
        }

        // editarMonitor
        if (0 === strpos($pathinfo, '/editarMonitor') && preg_match('#^/editarMonitor/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarMonitor')), array (  '_controller' => 'AppBundle\\Controller\\MonitorController::editarMonitorAction',));
        }

        // removeMonitor
        if ($pathinfo === '/removeMonitor') {
            return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::removeProductAction',  '_route' => 'removeMonitor',);
        }

        // insertReserva
        if ($pathinfo === '/insertReserva') {
            return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::insertReservaAction',  '_route' => 'insertReserva',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectReserva
            if ($pathinfo === '/selectReserva') {
                return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::selectReservaAction',  '_route' => 'selectReserva',);
            }

            // selectAllReserves
            if ($pathinfo === '/selectAllReserves') {
                return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::selectAllReservesAction',  '_route' => 'selectAllReserves',);
            }

        }

        // removeReserva
        if ($pathinfo === '/removeReserva') {
            return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::removeProductAction',  '_route' => 'removeReserva',);
        }

        // insertPista
        if ($pathinfo === '/insertPista') {
            return array (  '_controller' => 'AppBundle\\Controller\\SquashController::insertPistaAction',  '_route' => 'insertPista',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectPista
            if ($pathinfo === '/selectPista') {
                return array (  '_controller' => 'AppBundle\\Controller\\SquashController::selectPistaAction',  '_route' => 'selectPista',);
            }

            // selectAllPistes
            if ($pathinfo === '/selectAllPistes') {
                return array (  '_controller' => 'AppBundle\\Controller\\SquashController::selectAllPistesAction',  '_route' => 'selectAllPistes',);
            }

        }

        // insertUsuari
        if ($pathinfo === '/insertUsuari') {
            return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::insertUsuariAction',  '_route' => 'insertUsuari',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectUsuari
            if ($pathinfo === '/selectUsuari') {
                return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::selectUsuariAction',  '_route' => 'selectUsuari',);
            }

            // selectAllUsers
            if (0 === strpos($pathinfo, '/selectAllUsers') && preg_match('#^/selectAllUsers(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'selectAllUsers')), array (  'id' => 0,  '_controller' => 'AppBundle\\Controller\\UsuariController::selectAllUsersAction',));
            }

        }

        // updateUsuari
        if ($pathinfo === '/updateUsuari') {
            return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::updateUsuariAction',  '_route' => 'updateUsuari',);
        }

        // editarUsuari
        if (0 === strpos($pathinfo, '/editarUsuari') && preg_match('#^/editarUsuari/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarUsuari')), array (  '_controller' => 'AppBundle\\Controller\\UsuariController::editarUsuariAction',));
        }

        // removeUsuari
        if ($pathinfo === '/removeUsuari') {
            return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::removeProductAction',  '_route' => 'removeUsuari',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
