<?php

/* :reserva:form.html.twig */
class __TwigTemplate_f104a99295266e70254ac46fb1d7bfc1daf579cbbae57534acc3a02e53656876 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":reserva:form.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_style($context, array $blocks = array())
    {
        // line 6
        echo "\t.col-sm-4{
\t\t\t\tmargin-right: 1%;
\t\t\t}
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
    .col-sm-4{
    margin-bottom: 1%;
    }

";
    }

    // line 50
    public function block_menu_aside($context, array $blocks = array())
    {
        // line 51
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Reserva</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllReserves\">Llistar totes les reserves</a>
                </li>
                <li>
                    <a href=\"/selectReserva\">Buscar una reserva</a>
                </li>
                <li>
                    <a href=\"/insertReserva\">Fer una nova reserva</a>
                </li>

";
    }

    // line 76
    public function block_mainContent($context, array $blocks = array())
    {
        // line 77
        echo "    <h3> ";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo " </h3>
    ";
        // line 78
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
    ";
        // line 79
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'widget');
        echo "
    ";
        // line 80
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "

\t
\t<div class=\"container\">
\t  <h2>Estat de les pistes</h2>
\t  <button type=\"button\" class=\"btn btn-info\" data-toggle=\"collapse\" data-target=\"#demo\">Veure estat</button>
\t  <div id=\"demo\" class=\"collapse\">
\t    
\t\t  <div class=\"container\">
\t\t  \t<div class=\"row\">
\t\t  \t \t";
        // line 90
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pistes"]) ? $context["pistes"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["pista"]) {
            // line 91
            echo "\t\t    \t\t<div class=\"col-sm-4\" style=\"background-color:lavender;\">
\t\t    \t\t\t<h5>Numero de Pista: ";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["pista"], "getId", array(), "method"), "html", null, true);
            echo "</h5>
\t\t    \t\t\t";
            // line 93
            if (($this->getAttribute($context["pista"], "estat", array()) == 0)) {
                // line 94
                echo "\t\t    \t\t\t\t<p>Estat: Lliure</p>
\t\t    \t\t\t";
            } else {
                // line 96
                echo "              \t\t\t\t<p>Estat: Ocupat</p>
        \t\t\t\t";
            }
            // line 98
            echo "\t\t    \t\t</div>
\t    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pista'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "\t  \t\t</div>
\t  \t  </div>

\t  </div>
\t</div>

";
    }

    public function getTemplateName()
    {
        return ":reserva:form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 100,  160 => 98,  156 => 96,  152 => 94,  150 => 93,  146 => 92,  143 => 91,  139 => 90,  126 => 80,  122 => 79,  118 => 78,  113 => 77,  110 => 76,  83 => 51,  80 => 50,  33 => 6,  30 => 5,  11 => 2,);
    }
}
/* {# app/Resources/views/reserva/form.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* */
/* {% block style%}*/
/* 	.col-sm-4{*/
/* 				margin-right: 1%;*/
/* 			}*/
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/*     .col-sm-4{*/
/*     margin-bottom: 1%;*/
/*     }*/
/* */
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Reserva</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllReserves">Llistar totes les reserves</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectReserva">Buscar una reserva</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertReserva">Fer una nova reserva</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/*     <h3> {{title}} </h3>*/
/*     {{ form_start(form) }}*/
/*     {{ form_widget(form) }}*/
/*     {{ form_end(form) }}*/
/* */
/* 	*/
/* 	<div class="container">*/
/* 	  <h2>Estat de les pistes</h2>*/
/* 	  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Veure estat</button>*/
/* 	  <div id="demo" class="collapse">*/
/* 	    */
/* 		  <div class="container">*/
/* 		  	<div class="row">*/
/* 		  	 	{% for pista in pistes %}*/
/* 		    		<div class="col-sm-4" style="background-color:lavender;">*/
/* 		    			<h5>Numero de Pista: {{pista.getId()}}</h5>*/
/* 		    			{% if pista.estat==0 %}*/
/* 		    				<p>Estat: Lliure</p>*/
/* 		    			{% else %}*/
/*               				<p>Estat: Ocupat</p>*/
/*         				{% endif %}*/
/* 		    		</div>*/
/* 	    		{% endfor %}*/
/* 	  		</div>*/
/* 	  	  </div>*/
/* */
/* 	  </div>*/
/* 	</div>*/
/* */
/* {% endblock %}*/
