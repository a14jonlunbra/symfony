<?php

/* base.html.twig */
class __TwigTemplate_3d5592ecd34ce7f5fc6b24dd6d8b68756e64a778b1421497c1a00e978d16ef7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 17
        echo "        <style>
            .mainContent{
                margin-left: 20%;
            }
            .table-responsive{
                margin-right: 19%;
            }
          ";
        // line 24
        $this->displayBlock('style', $context, $blocks);
        // line 25
        echo "        </style>
    </head>
    
    <body>

         <nav class=\"navbar navbar-inverse navbar-static-top custom-navbar\" role=\"navigation\">
            <div class=\"container\">
             <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar-collapse-1\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
             </button>
             <div class=\"navbar-header\">
                 <a class=\"navbar-brand\" rel=\"home\" href=\"/\" title=\"Help\"> Projecte Symfony</a>
             </div>
             <!-- Non-collapsing right-side icons -->
             <ul class=\"nav navbar-nav navbar-right\">
              <li>
                   <a href=\"#\" class=\"fa fa-cog\"></a>
              </li>
              <li>
                   <a href=\"#\" class=\"fa fa-home\"></a>
              </li>
             </ul>
             <!-- the collapsing menu -->
             <div class=\"collapse navbar-collapse navbar-left\" id=\"navbar-collapse-1\">
               <ul class=\"nav navbar-nav\">
                   <li><a href=\"/selectAllUsers\">Usuaris</a></li>
                   <li><a href=\"/selectAllMonitors\">Monitors</a></li>
                   <li><a href=\"/selectAllClases\">Clases</a></li>
                   <li><a href=\"/selectAllPistes\">Pistes</a></li>
                   <li><a href=\"/selectAllReserves\">Reserves</a></li>
               </ul>
             </div>
            <!--/.nav-collapse -->
            </div>
            <!--/.container -->
         </nav>

        <div id=\"content\">
            <nav>
                ";
        // line 67
        $this->displayBlock('menu_aside', $context, $blocks);
        // line 68
        echo "            </nav>
            <main>
                <div class=\"mainContent\">
                    ";
        // line 71
        $this->displayBlock('mainContent', $context, $blocks);
        // line 72
        echo "                </div>
            </main>

        </div>

    </body>

</html>";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Product Application";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "         <!-- Latest compiled and minified CSS -->
         <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\">

         <!-- jQuery library -->
         <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js\"></script>

         <!-- Latest compiled JavaScript -->
         <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
        ";
    }

    // line 24
    public function block_style($context, array $blocks = array())
    {
    }

    // line 67
    public function block_menu_aside($context, array $blocks = array())
    {
        echo " ";
    }

    // line 71
    public function block_mainContent($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  143 => 71,  137 => 67,  132 => 24,  120 => 8,  117 => 7,  111 => 6,  100 => 72,  98 => 71,  93 => 68,  91 => 67,  47 => 25,  45 => 24,  36 => 17,  34 => 7,  30 => 6,  24 => 2,);
    }
}
/* {# app/Resources/views/base.html.twig #}*/
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="UTF-8">*/
/*         <title>{% block title %}Product Application{% endblock %}</title>*/
/*         {% block stylesheets %}*/
/*          <!-- Latest compiled and minified CSS -->*/
/*          <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">*/
/* */
/*          <!-- jQuery library -->*/
/*          <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>*/
/* */
/*          <!-- Latest compiled JavaScript -->*/
/*          <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>*/
/*         {% endblock %}*/
/*         <style>*/
/*             .mainContent{*/
/*                 margin-left: 20%;*/
/*             }*/
/*             .table-responsive{*/
/*                 margin-right: 19%;*/
/*             }*/
/*           {% block style%}{% endblock %}*/
/*         </style>*/
/*     </head>*/
/*     */
/*     <body>*/
/* */
/*          <nav class="navbar navbar-inverse navbar-static-top custom-navbar" role="navigation">*/
/*             <div class="container">*/
/*              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">*/
/*                 <span class="sr-only">Toggle navigation</span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*              </button>*/
/*              <div class="navbar-header">*/
/*                  <a class="navbar-brand" rel="home" href="/" title="Help"> Projecte Symfony</a>*/
/*              </div>*/
/*              <!-- Non-collapsing right-side icons -->*/
/*              <ul class="nav navbar-nav navbar-right">*/
/*               <li>*/
/*                    <a href="#" class="fa fa-cog"></a>*/
/*               </li>*/
/*               <li>*/
/*                    <a href="#" class="fa fa-home"></a>*/
/*               </li>*/
/*              </ul>*/
/*              <!-- the collapsing menu -->*/
/*              <div class="collapse navbar-collapse navbar-left" id="navbar-collapse-1">*/
/*                <ul class="nav navbar-nav">*/
/*                    <li><a href="/selectAllUsers">Usuaris</a></li>*/
/*                    <li><a href="/selectAllMonitors">Monitors</a></li>*/
/*                    <li><a href="/selectAllClases">Clases</a></li>*/
/*                    <li><a href="/selectAllPistes">Pistes</a></li>*/
/*                    <li><a href="/selectAllReserves">Reserves</a></li>*/
/*                </ul>*/
/*              </div>*/
/*             <!--/.nav-collapse -->*/
/*             </div>*/
/*             <!--/.container -->*/
/*          </nav>*/
/* */
/*         <div id="content">*/
/*             <nav>*/
/*                 {% block menu_aside %} {% endblock %}*/
/*             </nav>*/
/*             <main>*/
/*                 <div class="mainContent">*/
/*                     {% block mainContent %}{% endblock %}*/
/*                 </div>*/
/*             </main>*/
/* */
/*         </div>*/
/* */
/*     </body>*/
/* */
/* </html>*/
