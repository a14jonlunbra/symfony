<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_ba8247285f4eb2e2b816c895a54f5adfdd34b37dd4b7e67d6ca2224026004f3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
