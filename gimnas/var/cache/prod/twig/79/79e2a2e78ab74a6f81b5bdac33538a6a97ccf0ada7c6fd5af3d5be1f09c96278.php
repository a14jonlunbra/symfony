<?php

/* :reserva:content.html.twig */
class __TwigTemplate_8e638591fe130b02d4f8ed1ed23fa0ff654de397e839b79a2ff1e43e95d2529f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":reserva:content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Reserves</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllReserves\">Llistar totes les reserves</a>
                </li>
                <li>
                    <a href=\"/selectReserva\">Buscar una reserva</a>
                <li>
                    <a href=\"/insertReserva\">Fer una nova reserva</a>
                </li>

";
    }

    // line 68
    public function block_mainContent($context, array $blocks = array())
    {
        // line 69
        echo "
<div class=\"container\">
  <h2>Llista reserves inscrites</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>pista</th>
                <th>usuari</th>
                <th>data</th>
                <th>hora</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 85
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reserva"]) ? $context["reserva"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            // line 86
            echo "                 <tr>
                    <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "pista", array()), "getNumeroPista", array(), "method"), "html", null, true);
            echo "</td>
                    <td>";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["res"], "usuari", array()), "getCognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 90
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["res"], "data", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["res"], "hora", array()), "H:i:s"), "html", null, true);
            echo "</td>
                    <td><a href=\"/removeReserva/";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["res"], "id", array()), "html", null, true);
            echo "\">X</a></td>
                 </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 95
        echo "            </tbody>
        </table>
    </div> 
</div>

";
    }

    public function getTemplateName()
    {
        return ":reserva:content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 95,  151 => 92,  147 => 91,  143 => 90,  139 => 89,  135 => 88,  131 => 87,  128 => 86,  124 => 85,  106 => 69,  103 => 68,  77 => 44,  74 => 43,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Reserves</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllReserves">Llistar totes les reserves</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectReserva">Buscar una reserva</a>*/
/*                 <li>*/
/*                     <a href="/insertReserva">Fer una nova reserva</a>*/
/*                 </li>*/
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista reserves inscrites</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>pista</th>*/
/*                 <th>usuari</th>*/
/*                 <th>data</th>*/
/*                 <th>hora</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for res in reserva %}*/
/*                  <tr>*/
/*                     <td>{{ res.id }}</td>*/
/*                     <td>{{ res.pista.getNumeroPista() }}</td>*/
/*                     <td>{{ res.usuari.getCognom1 }}</td>*/
/*                     <td>{{ res.data|date("m/d/Y")}}</td>*/
/*                     <td>{{ res.hora |date("H:i:s")}}</td>*/
/*                     <td><a href="/removeReserva/{{res.id}}">X</a></td>*/
/*                  </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
