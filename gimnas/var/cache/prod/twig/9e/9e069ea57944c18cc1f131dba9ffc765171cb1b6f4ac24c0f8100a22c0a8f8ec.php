<?php

/* :usuari:content.html.twig */
class __TwigTemplate_7ffe35cfc4b00d59a86ef695a866aab05975d6d5aceb2ade6be7e3f7f91636d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":usuari:content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
    }

    // line 44
    public function block_menu_aside($context, array $blocks = array())
    {
        // line 45
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Usuaris</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllUsers\">Llistar tots els usuaris</a>
                </li>
                <li>
                    <a href=\"/selectUsuari\">Seleccionar un usuari</a></li>
                <li>
                    <a href=\"/insertUsuari\">Insertar nou usuari</a></li>
                <li>
                    <a href=\"/updateUsuari\">Modificar un usuari</a>
                </li>

               

";
    }

    // line 73
    public function block_mainContent($context, array $blocks = array())
    {
        // line 74
        echo "
<div class=\"container\">
  <h2>Llista d'usuaris inscrits</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th><a href=\"/selectAllUsers/2\">nom</a></th>
                <th><a href=\"/selectAllUsers/3\">cognom1</a></th>
                <th><a href=\"/selectAllUsers/4\">cognom2</a></th>
                <th>edat</th>
                <th><a href=\"/selectAllUsers/6\">dni</a></th>
                <th>email</th>
                <th>telefon</th>
                <th><a href=\"/selectAllUsers/9\">inscripcio</a></th>
                <th>quota</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 95
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usuari"]) ? $context["usuari"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["usu"]) {
            // line 96
            echo "                <tr>
                    <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "nom", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom1", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "cognom2", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 101
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "edat", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 102
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "dni", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 103
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "email", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 104
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "telefon", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 105
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["usu"], "inscripcio", array()), "m/d/Y"), "html", null, true);
            echo "</td>
                    <td>";
            // line 106
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "quota", array()), "html", null, true);
            echo "</td>
                    <td><a href=\"/removeUsuari/";
            // line 107
            echo twig_escape_filter($this->env, $this->getAttribute($context["usu"], "id", array()), "html", null, true);
            echo "\">X</a></td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['usu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "            </tbody>
        </table>
    </div> 
</div>

";
    }

    public function getTemplateName()
    {
        return ":usuari:content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 110,  180 => 107,  176 => 106,  172 => 105,  168 => 104,  164 => 103,  160 => 102,  156 => 101,  152 => 100,  148 => 99,  144 => 98,  140 => 97,  137 => 96,  133 => 95,  110 => 74,  107 => 73,  77 => 45,  74 => 44,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/usuari/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* */
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Usuaris</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllUsers">Llistar tots els usuaris</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectUsuari">Seleccionar un usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/insertUsuari">Insertar nou usuari</a></li>*/
/*                 <li>*/
/*                     <a href="/updateUsuari">Modificar un usuari</a>*/
/*                 </li>*/
/* */
/*                */
/* */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista d'usuaris inscrits</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th><a href="/selectAllUsers/2">nom</a></th>*/
/*                 <th><a href="/selectAllUsers/3">cognom1</a></th>*/
/*                 <th><a href="/selectAllUsers/4">cognom2</a></th>*/
/*                 <th>edat</th>*/
/*                 <th><a href="/selectAllUsers/6">dni</a></th>*/
/*                 <th>email</th>*/
/*                 <th>telefon</th>*/
/*                 <th><a href="/selectAllUsers/9">inscripcio</a></th>*/
/*                 <th>quota</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for usu in usuari %}*/
/*                 <tr>*/
/*                     <td>{{ usu.id }}</td>*/
/*                     <td>{{ usu.nom }}</td>*/
/*                     <td>{{ usu.cognom1 }}</td>*/
/*                     <td>{{ usu.cognom2 }}</td>*/
/*                     <td>{{ usu.edat }}</td>*/
/*                     <td>{{ usu.dni }}</td>*/
/*                     <td>{{ usu.email }}</td>*/
/*                     <td>{{ usu.telefon }}</td>*/
/*                     <td>{{ usu.inscripcio|date("m/d/Y")}}</td>*/
/*                     <td>{{ usu.quota }}</td>*/
/*                     <td><a href="/removeUsuari/{{usu.id}}">X</a></td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
