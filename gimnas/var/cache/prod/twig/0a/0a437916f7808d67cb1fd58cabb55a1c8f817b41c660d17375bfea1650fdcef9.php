<?php

/* :clase:content.html.twig */
class __TwigTemplate_52bd3dcd537673038d0685d483cdc29bad61cde2d25a146e7a5991647c5da054 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":clase:content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }
    }
";
    }

    // line 43
    public function block_menu_aside($context, array $blocks = array())
    {
        // line 44
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Clase</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllClases\">Llistar totes les classes</a>
                </li>
                <li>
                    <a href=\"/selectClase\">Seleccionar una classe</a>
                </li>
                <li>
                    <a href=\"/insertClase\">Crear nova classe</a></li>
                <li><a href=\"/updateClase\">Modificar una classe</a></li>
                
";
    }

    // line 69
    public function block_mainContent($context, array $blocks = array())
    {
        // line 70
        echo "
<div class=\"container\">
  <h2>Llista de clases inscrites</h2>

    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>monitor</th>
                <th>nom</th>
                <th>descripcio</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 84
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clase"]) ? $context["clase"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 85
            echo "                <tr>
                  <td>";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["c"], "monitor", array()), "getNom", array(), "method"), "html", null, true);
            echo "</td>
                  <td>";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "nom", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "descripcio", array()), "html", null, true);
            echo "</td>
                  <td><a href=\"/removeClase/";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["c"], "id", array()), "html", null, true);
            echo "\">X</a></td>
                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "            </tbody>
        </table>
    </div> 
</div>

";
    }

    public function getTemplateName()
    {
        return ":clase:content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 92,  142 => 89,  138 => 88,  134 => 87,  130 => 86,  127 => 85,  123 => 84,  107 => 70,  104 => 69,  77 => 44,  74 => 43,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/clase/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Clase</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllClases">Llistar totes les classes</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/selectClase">Seleccionar una classe</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertClase">Crear nova classe</a></li>*/
/*                 <li><a href="/updateClase">Modificar una classe</a></li>*/
/*                 */
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* */
/* <div class="container">*/
/*   <h2>Llista de clases inscrites</h2>*/
/* */
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>monitor</th>*/
/*                 <th>nom</th>*/
/*                 <th>descripcio</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for c in clase %}*/
/*                 <tr>*/
/*                   <td>{{ c.monitor.getNom() }}</td>*/
/*                   <td>{{ c.nom }}</td>*/
/*                   <td>{{ c.descripcio }}</td>*/
/*                   <td><a href="/removeClase/{{c.id}}">X</a></td>*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
