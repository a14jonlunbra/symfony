<?php

/* :pista:content.html.twig */
class __TwigTemplate_cdac5b0c854bd7cc88c02f6d7741aecdef1ab3adfe26315f8eecdf97cebdf391 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("base.html.twig", ":pista:content.html.twig", 2);
        $this->blocks = array(
            'style' => array($this, 'block_style'),
            'menu_aside' => array($this, 'block_menu_aside'),
            'mainContent' => array($this, 'block_mainContent'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_style($context, array $blocks = array())
    {
        // line 5
        echo "
    /* we push the content down some and clean up the edges on mobile devices */
    @media (max-width: 767px) {
        .content {
            padding: 15px;
            margin-top: 100px;
        }
    }

    /* When we were on larger screen sizes we can show our vertical menu bar */
    @media (min-width: 768px) {
        /* clean up some of the default Bootstrap styles for panels in the menu */

        #menu-bar .panel {
            margin-bottom: 0;    margin-bottom: 0;

            border: none;
            border-radius: 0;
            -webkit-box-shadow: none;
            -box-shadow: none;
        }

        #navbar {
            float: left;
            width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly */
            height: 100%;
        }

        .content {
            margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well */
            min-height: 100%;
        }

        .container {
         margin-left: 25px; /* We will add a little cushion to our content between the menu bar */
        }

    }

    .buida{ 234
            background-color: green;
        }
    .ocupada{
        background-color: rgba(255, 0, 0, 0.7);
        color: rgba(255, 0, 0, 0.7);
    }
";
    }

    // line 52
    public function block_menu_aside($context, array $blocks = array())
    {
        // line 53
        echo " <div id=\"navbar\" class=\"navbar navbar-inverse \">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                <span class=\"sr-only\">Toggle Navigation</span> <span class=\"icon-bar\"></span> <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a href=\"navbar-brand\" href=\"#\" title=\"Home\" rel=\"home\">
                <h1 class=\"site-title\">Pista</h1>
            </a>
        </div>
        <<!-- Let's clear the float so the menu drops below the header -->
        <div class=\"clearfix\"></div> <div class=\"collapse navbar-collapse\"> <ul class=\"nav nav-stacked\" id=\"menu-bar\">
                <!-- Notice the \"nav-stacked\" class we added here -->
                <li>
                    <a href=\"/selectAllPistes\">Llistar totes les pistes</a>
                </li>
                <li>
                    <a href=\"/insertPista\">Insertar una nova pista</a></li>
                <li>
                    <a href=\"/selectPista\">Seleccionar una pista</a>
                </li>
";
    }

    // line 76
    public function block_mainContent($context, array $blocks = array())
    {
        // line 77
        echo "<h2>Llista de pistes inscrites</h2>
<div class=\"container\">
    <div class=\"table-responsive\">
        <table class=\"table table-hover\">
            <thead>
              <tr>
                <th>id</th>
                <th>numero de pista</th>
                <th>estat</th>
              </tr>
            </thead>
            <tbody>
              ";
        // line 89
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pista"]) ? $context["pista"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 90
            echo "                <tr>
                    <td>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "id", array()), "html", null, true);
            echo "</td>
                    <td>";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["p"], "numeroPista", array()), "html", null, true);
            echo "</td>
                    ";
            // line 93
            if (($this->getAttribute($context["p"], "estat", array()) == 1)) {
                // line 94
                echo "                        <td class=\"ocupada\"> Pista ocupada </td>
                    ";
            } else {
                // line 96
                echo "                        <td class=\"buida\"> Pista buida </td>
                    ";
            }
            // line 98
            echo "                </tr>
              ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "            </tbody>
        </table>

    </div> 
</div>

";
    }

    public function getTemplateName()
    {
        return ":pista:content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 100,  153 => 98,  149 => 96,  145 => 94,  143 => 93,  139 => 92,  135 => 91,  132 => 90,  128 => 89,  114 => 77,  111 => 76,  86 => 53,  83 => 52,  33 => 5,  30 => 4,  11 => 2,);
    }
}
/* {# app/Resources/views/pista/content.html.twig #}*/
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block style%}*/
/* */
/*     /* we push the content down some and clean up the edges on mobile devices *//* */
/*     @media (max-width: 767px) {*/
/*         .content {*/
/*             padding: 15px;*/
/*             margin-top: 100px;*/
/*         }*/
/*     }*/
/* */
/*     /* When we were on larger screen sizes we can show our vertical menu bar *//* */
/*     @media (min-width: 768px) {*/
/*         /* clean up some of the default Bootstrap styles for panels in the menu *//* */
/* */
/*         #menu-bar .panel {*/
/*             margin-bottom: 0;    margin-bottom: 0;*/
/* */
/*             border: none;*/
/*             border-radius: 0;*/
/*             -webkit-box-shadow: none;*/
/*             -box-shadow: none;*/
/*         }*/
/* */
/*         #navbar {*/
/*             float: left;*/
/*             width: 230px; /* You can adjust the width to your liking, just remember to also adjust the content margin below accordingly *//* */
/*             height: 100%;*/
/*         }*/
/* */
/*         .content {*/
/*             margin-left: 300px; /* If you adjusted the width above you will need to adjust this as well *//* */
/*             min-height: 100%;*/
/*         }*/
/* */
/*         .container {*/
/*          margin-left: 25px; /* We will add a little cushion to our content between the menu bar *//* */
/*         }*/
/* */
/*     }*/
/* */
/*     .buida{ 234*/
/*             background-color: green;*/
/*         }*/
/*     .ocupada{*/
/*         background-color: rgba(255, 0, 0, 0.7);*/
/*         color: rgba(255, 0, 0, 0.7);*/
/*     }*/
/* {% endblock %}*/
/* {% block menu_aside %}*/
/*  <div id="navbar" class="navbar navbar-inverse ">*/
/*         <div class="navbar-header">*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*                 <span class="sr-only">Toggle Navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span>*/
/*                 <span class="icon-bar"></span>*/
/*             </button>*/
/*             <a href="navbar-brand" href="#" title="Home" rel="home">*/
/*                 <h1 class="site-title">Pista</h1>*/
/*             </a>*/
/*         </div>*/
/*         <<!-- Let's clear the float so the menu drops below the header -->*/
/*         <div class="clearfix"></div> <div class="collapse navbar-collapse"> <ul class="nav nav-stacked" id="menu-bar">*/
/*                 <!-- Notice the "nav-stacked" class we added here -->*/
/*                 <li>*/
/*                     <a href="/selectAllPistes">Llistar totes les pistes</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="/insertPista">Insertar una nova pista</a></li>*/
/*                 <li>*/
/*                     <a href="/selectPista">Seleccionar una pista</a>*/
/*                 </li>*/
/* {% endblock %}*/
/* */
/* {% block mainContent %}*/
/* <h2>Llista de pistes inscrites</h2>*/
/* <div class="container">*/
/*     <div class="table-responsive">*/
/*         <table class="table table-hover">*/
/*             <thead>*/
/*               <tr>*/
/*                 <th>id</th>*/
/*                 <th>numero de pista</th>*/
/*                 <th>estat</th>*/
/*               </tr>*/
/*             </thead>*/
/*             <tbody>*/
/*               {% for p in pista %}*/
/*                 <tr>*/
/*                     <td>{{ p.id }}</td>*/
/*                     <td>{{ p.numeroPista }}</td>*/
/*                     {% if(p.estat==1) %}*/
/*                         <td class="ocupada"> Pista ocupada </td>*/
/*                     {% else %}*/
/*                         <td class="buida"> Pista buida </td>*/
/*                     {% endif %}*/
/*                 </tr>*/
/*               {% endfor %}*/
/*             </tbody>*/
/*         </table>*/
/* */
/*     </div> */
/* </div>*/
/* */
/* {% endblock %}*/
