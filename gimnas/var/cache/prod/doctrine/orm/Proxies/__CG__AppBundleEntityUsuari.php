<?php

namespace Proxies\__CG__\AppBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Usuari extends \AppBundle\Entity\Usuari implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'id', 'reserves', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'nom', 'clases', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'cognom1', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'cognom2', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'edat', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'inscripcio', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'quota', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'telefon', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'dni', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'email'];
        }

        return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'id', 'reserves', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'nom', 'clases', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'cognom1', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'cognom2', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'edat', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'inscripcio', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'quota', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'telefon', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'dni', '' . "\0" . 'AppBundle\\Entity\\Usuari' . "\0" . 'email'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Usuari $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setNom($nom)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNom', [$nom]);

        return parent::setNom($nom);
    }

    /**
     * {@inheritDoc}
     */
    public function getNom()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNom', []);

        return parent::getNom();
    }

    /**
     * {@inheritDoc}
     */
    public function setCognom1($cognom1)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCognom1', [$cognom1]);

        return parent::setCognom1($cognom1);
    }

    /**
     * {@inheritDoc}
     */
    public function getCognom1()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCognom1', []);

        return parent::getCognom1();
    }

    /**
     * {@inheritDoc}
     */
    public function setCognom2($cognom2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCognom2', [$cognom2]);

        return parent::setCognom2($cognom2);
    }

    /**
     * {@inheritDoc}
     */
    public function getCognom2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCognom2', []);

        return parent::getCognom2();
    }

    /**
     * {@inheritDoc}
     */
    public function setEdat($edat)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEdat', [$edat]);

        return parent::setEdat($edat);
    }

    /**
     * {@inheritDoc}
     */
    public function getEdat()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEdat', []);

        return parent::getEdat();
    }

    /**
     * {@inheritDoc}
     */
    public function setInscripcio($inscripcio)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setInscripcio', [$inscripcio]);

        return parent::setInscripcio($inscripcio);
    }

    /**
     * {@inheritDoc}
     */
    public function getInscripcio()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getInscripcio', []);

        return parent::getInscripcio();
    }

    /**
     * {@inheritDoc}
     */
    public function setQuota($quota)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setQuota', [$quota]);

        return parent::setQuota($quota);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuota()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getQuota', []);

        return parent::getQuota();
    }

    /**
     * {@inheritDoc}
     */
    public function addReserf(\AppBundle\Entity\Reserva $reserf)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addReserf', [$reserf]);

        return parent::addReserf($reserf);
    }

    /**
     * {@inheritDoc}
     */
    public function removeReserf(\AppBundle\Entity\Reserva $reserf)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeReserf', [$reserf]);

        return parent::removeReserf($reserf);
    }

    /**
     * {@inheritDoc}
     */
    public function getReserves()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReserves', []);

        return parent::getReserves();
    }

    /**
     * {@inheritDoc}
     */
    public function addClase(\AppBundle\Entity\Clase $clase)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addClase', [$clase]);

        return parent::addClase($clase);
    }

    /**
     * {@inheritDoc}
     */
    public function removeClase(\AppBundle\Entity\Clase $clase)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeClase', [$clase]);

        return parent::removeClase($clase);
    }

    /**
     * {@inheritDoc}
     */
    public function getClases()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getClases', []);

        return parent::getClases();
    }

    /**
     * {@inheritDoc}
     */
    public function setTelefon($telefon)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTelefon', [$telefon]);

        return parent::setTelefon($telefon);
    }

    /**
     * {@inheritDoc}
     */
    public function getTelefon()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTelefon', []);

        return parent::getTelefon();
    }

    /**
     * {@inheritDoc}
     */
    public function setDni($dni)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDni', [$dni]);

        return parent::setDni($dni);
    }

    /**
     * {@inheritDoc}
     */
    public function getDni()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDni', []);

        return parent::getDni();
    }

    /**
     * {@inheritDoc}
     */
    public function setEmail($email)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setEmail', [$email]);

        return parent::setEmail($email);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getEmail', []);

        return parent::getEmail();
    }

}
