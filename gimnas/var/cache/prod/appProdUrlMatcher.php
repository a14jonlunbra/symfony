<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // insertClase
        if ($pathinfo === '/insertClase') {
            return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::insertClaseAction',  '_route' => 'insertClase',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectClase
            if ($pathinfo === '/selectClase') {
                return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::selectClaseAction',  '_route' => 'selectClase',);
            }

            // selectAllClases
            if ($pathinfo === '/selectAllClases') {
                return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::selectAllClasesAction',  '_route' => 'selectAllClases',);
            }

        }

        // updateClase
        if ($pathinfo === '/updateClase') {
            return array (  '_controller' => 'AppBundle\\Controller\\ClaseController::updateClaseAction',  '_route' => 'updateClase',);
        }

        // editarClase
        if (0 === strpos($pathinfo, '/editarClase') && preg_match('#^/editarClase/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarClase')), array (  '_controller' => 'AppBundle\\Controller\\ClaseController::editarUsuariAction',));
        }

        // matricular
        if (0 === strpos($pathinfo, '/matricula') && preg_match('#^/matricula/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'matricular')), array (  '_controller' => 'AppBundle\\Controller\\ClaseController::matricularAction',));
        }

        // removeClase
        if (0 === strpos($pathinfo, '/removeClase') && preg_match('#^/removeClase/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'removeClase')), array (  '_controller' => 'AppBundle\\Controller\\ClaseController::removeClaseAction',));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        // insertMonitor
        if ($pathinfo === '/insertMonitor') {
            return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::insertMonitorAction',  '_route' => 'insertMonitor',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectAllCognom1
            if ($pathinfo === '/selectAllCognom1') {
                return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::selectAllCognom1Action',  '_route' => 'selectAllCognom1',);
            }

            // selectMonitor
            if ($pathinfo === '/selectMonitor') {
                return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::selectMonitorAction',  '_route' => 'selectMonitor',);
            }

            // selectAllMonitors
            if (0 === strpos($pathinfo, '/selectAllMonitors') && preg_match('#^/selectAllMonitors(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'selectAllMonitors')), array (  'id' => 0,  '_controller' => 'AppBundle\\Controller\\MonitorController::selectAllMonitorsAction',));
            }

        }

        // updateMonitor
        if ($pathinfo === '/updateMonitor') {
            return array (  '_controller' => 'AppBundle\\Controller\\MonitorController::updateMonitorAction',  '_route' => 'updateMonitor',);
        }

        // editarMonitor
        if (0 === strpos($pathinfo, '/editarMonitor') && preg_match('#^/editarMonitor/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarMonitor')), array (  '_controller' => 'AppBundle\\Controller\\MonitorController::editarMonitorAction',));
        }

        // removeMonitor
        if (0 === strpos($pathinfo, '/removeMonitor') && preg_match('#^/removeMonitor/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'removeMonitor')), array (  '_controller' => 'AppBundle\\Controller\\MonitorController::removeProductAction',));
        }

        // insertReserva
        if ($pathinfo === '/insertReserva') {
            return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::insertReservaAction',  '_route' => 'insertReserva',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectReserva
            if ($pathinfo === '/selectReserva') {
                return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::selectReservaAction',  '_route' => 'selectReserva',);
            }

            // selectAllReserves
            if ($pathinfo === '/selectAllReserves') {
                return array (  '_controller' => 'AppBundle\\Controller\\ReservaController::selectAllReservesAction',  '_route' => 'selectAllReserves',);
            }

        }

        // removeReserva
        if (0 === strpos($pathinfo, '/removeReserva') && preg_match('#^/removeReserva/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'removeReserva')), array (  '_controller' => 'AppBundle\\Controller\\ReservaController::removeReservaAction',));
        }

        // insertPista
        if ($pathinfo === '/insertPista') {
            return array (  '_controller' => 'AppBundle\\Controller\\SquashController::insertPistaAction',  '_route' => 'insertPista',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectPista
            if ($pathinfo === '/selectPista') {
                return array (  '_controller' => 'AppBundle\\Controller\\SquashController::selectPistaAction',  '_route' => 'selectPista',);
            }

            // selectAllPistes
            if ($pathinfo === '/selectAllPistes') {
                return array (  '_controller' => 'AppBundle\\Controller\\SquashController::selectAllPistesAction',  '_route' => 'selectAllPistes',);
            }

        }

        // insertUsuari
        if ($pathinfo === '/insertUsuari') {
            return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::insertUsuariAction',  '_route' => 'insertUsuari',);
        }

        if (0 === strpos($pathinfo, '/select')) {
            // selectUsuari
            if ($pathinfo === '/selectUsuari') {
                return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::selectUsuariAction',  '_route' => 'selectUsuari',);
            }

            // selectAllUsers
            if (0 === strpos($pathinfo, '/selectAllUsers') && preg_match('#^/selectAllUsers(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'selectAllUsers')), array (  'id' => 0,  '_controller' => 'AppBundle\\Controller\\UsuariController::selectAllUsersAction',));
            }

        }

        // updateUsuari
        if ($pathinfo === '/updateUsuari') {
            return array (  '_controller' => 'AppBundle\\Controller\\UsuariController::updateUsuariAction',  '_route' => 'updateUsuari',);
        }

        // editarUsuari
        if (0 === strpos($pathinfo, '/editarUsuari') && preg_match('#^/editarUsuari/(?P<nom>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'editarUsuari')), array (  '_controller' => 'AppBundle\\Controller\\UsuariController::editarUsuariAction',));
        }

        // removeUsuari
        if (0 === strpos($pathinfo, '/removeUsuari') && preg_match('#^/removeUsuari/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'removeUsuari')), array (  '_controller' => 'AppBundle\\Controller\\UsuariController::removeProductAction',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
